# `libuv` Internals Deep Dive

Let's dive into the `libuv` source code!

Vist [https://libuv.org](https://libuv.org). `libuv` is part of Node, but it's
also meaningful **on its own**. NodeJS has **bindings** that allow it to
leverage `libuv`, but **other programming languages have `libuv` bindings as
well**.
[The libuv Github repo](https://github.com/libuv/libuv/blob/v1.x/LINKS.md) lists
all the languages that have `libuv` bindings. The bulk of the `libuv` code lives
in the `src` folder in this repo. We have two main folders: `unix` and `win`.

Open `unix/fs.c`. The `uv_fs_open` function lives here. The API bindings are
calling this function:

```c
int uv_fs_open(uv_loop_t* loop,
               uv_fs_t* req,
               const char* path,
               int flags,
               int mode,
               uv_fs_cb cb) {
  INIT(OPEN);
  PATH;
  req->flags = flags;
  req->mode = mode;
  if (cb != NULL)
    if (uv__iou_fs_open(loop, req))
      return 0;
  POST;
}
```

This doesn't seem to be opening any files, but the actual implementation is in
`uv__fs_open`:

```c
static ssize_t uv__fs_open(uv_fs_t* req) {
#ifdef O_CLOEXEC
  return open(req->path, req->flags | O_CLOEXEC, req->mode);
#else  /* O_CLOEXEC */
  int r;

  if (req->cb != NULL)
    uv_rwlock_rdlock(&req->loop->cloexec_lock);

  r = open(req->path, req->flags, req->mode);

  /* In case of failure `uv__cloexec` will leave error in `errno`,
   * so it is enough to just set `r` to `-1`.
   */
  if (r >= 0 && uv__cloexec(r, 1) != 0) {
    r = uv__close(r);
    if (r != 0)
      abort();
    r = -1;
  }

  if (req->cb != NULL)
    uv_rwlock_rdunlock(&req->loop->cloexec_lock);

  return r;
#endif  /* O_CLOEXEC */
}
```

**This is where the opening of the file**, and calling down into the (Unix) OS
is done!

Node allows us to not have to worry about coding low-level languages, but it's
beneficial to know how things are implemented in Node!
