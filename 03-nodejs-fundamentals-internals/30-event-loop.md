# The Event Loop

Let's take a closer look at the event loop. The event loop is responsible for
handling all the callback functions that allow Node to execute code
**asynchronously**, even though JS is a **single-threaded language**.

When a program executes in Node runtime, a piece of code called the event loop
starts executing in `libuv`. The event loop resembles a `while` loop that runs
continuously until a condition is true. For `libuv`, that condition is:

> "Should the process exit?"

If the code is done executing and the process should exit, `libuv` terminates
the process. If there are still callback functions to execute, the event loop
will execute them. If there are no callback functions, the process will simply
wait.

The event loop runs while Node is running, and processes callback functions
until Node is ready to exit.
