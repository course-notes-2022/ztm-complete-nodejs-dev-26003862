# Is NodeJS Multi-Threaded?

The ability to run JS code async without worrying about complicated
multi-threading logic is what made it so popular. How does node do this?

Node executes JS code, so it _has_ to follow JS' single-threaded architecture.
It gives us the ability to run **async code** using `libuv`'s **event loop**.

## The Event Loop

In Node, whenever we call an **async function**, it gets put on the **event
loop**. The event loop instructs the OS to execute functions and **notify the
loop when the work is done**.

There are two main **executors** of that work. Some tasks, such as network
requests, are done by the OS itself. Other tasks, such as reading a file, are
done in the **thread pool**.

In `libuv`, there is a collection of threads set up in advance that are
available to take up work as it comes in. Recall that `libuv` is written in C++,
which _is_ a multi-threaded language. The threads in the thread pool are just
like the ones in the `process` we learned about in the last lesson:

![libuv and threads](./screenshots/libuv-and-threads.png)

If we run out of available threads in the thread pool, we have to wait until
some tasks complete and a thread is available. When a task on a thread
completes, the thread pool notifies the event loop, which executes the
corresponding callback function.

Note that **only some operations use this thread pool**. Not all async functions
are executed in the thread pool! Instead, where possible, `libuv` uses the OS
kernel directly. The kernel is the core of the OS that talks to the system
hardware. It's really good at doing basic operations such as communicating with
other computers over the network. Whenever possible, `libuv` will leverage the
OS kernel **instead of the thread pool**. When an operation is finished on the
kernel, the event loop once again executes a callback.

As a Node developer, you never have to worry about managing threads! The code
behind node and your OS uses the threads internally to perform I/O tasks!
