# Phases of the Event Loop

Let's continue looking at how the event loop processes events. We've learned
that when we pass a callback to a function, that callback gets put onto a
callback queue to be executed at a later time. What does the event loop do with
the callback queue?

If you investigate the workings of the event loop, you'll see that there are
actually _several_ callback queues that handle different **phases** of the event
loop. There are 4 main phases of the event loop:

1. Timers
2. I/O Callbacks
3. `setImmediate`
4. Close Callbacks

Each of these phases has its own callback queue. For example, a `setTimeout`
callback goes on the Timers queue.

## Understanding Timers

Node has 3 types of timers:

- `setTimeout`
- `setInterval`
- `setImmediate`: This is a special timer that sets the callback it receives to
  be executed **as soon as possible** by the event loop.

When the event loop first executes, it begins by going through all the `Timers`
callbacks. Then, it moves on to the `I/O callbacks` (the majority of all your
callbacks, such as network and file operations), followed by the `setImmediate`
phase callbacks. Finally the event loop executes the `Close` callbacks, such as
callbacks for closing a file or a db connection. When all the phases callbacks
are complete, the event loop moves on to the next "tick", and begins again.

Node combines this event loop, along with `libuv` and V8, to bring Javascript to
the server side!
