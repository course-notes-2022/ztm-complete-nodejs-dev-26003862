# Async Callbacks

NodeJS is an **asynchronous event-driven JavaScript runtime**.

Consider the following code:

```js
console.log('Hare finishes!');
console.log('Tortoise finishes!');
```

This is an example of **synchronous code**; it executes **line-by-line**, each
step waiting until the previous step finishes before executing.

The output will be:

```
Hare finishes!
Tortoise finishes!
```

Let's look at an **async approach**:

```js
setTimeout(() => {
  console.log('Hare finishes!');
}, 3000);
console.log('Tortoise finishes!');
```

This is **async code**, accomplished by passing a **callback function** to
`setTimeout`. A callback function is a function that will **be executed at a
later time**.

Callback functions are **at the heart of NodeJS**. They allow us to build web
servers that can handle thousands of requests at the same time without waiting
for the previous request to complete.
