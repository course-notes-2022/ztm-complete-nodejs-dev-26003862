# What NodeJS Includes

What does the NodeJS runtime include beyond the V8 engine?

V8 lets node run Javascript, but there's more to node than the ability to run
pure JS. We can do many things that are outside the JS language specification.
We can get the arguments passed to our program, perform file input/output, and
much more!

## NodeJS APIs

Node provides a number of APIs that allow us to read/write files, make and
respond to HTTP requests, encrypt data, and more. V8 can leverage these APIs,
which are written in JS as well as **lower-level languages** such as C++.

Our JS code is able to leverage these lower-level languages via **NodeJS
bindings**. The Node APIs depend on these bindings for much of their core
functionality.

The **implementation** of this core functionality lives in `libuv`. `libuv` and
V8 are the two **most important components** of NodeJS. `libuv` is a
highly-optimized library of code written in C++ that deals with input/output
operations that it **delegates to other parts of your OS**.

For example, imagine you are downloading a web page. The download request is
made via JS using the `http` module from the NodeJS API, and through the NodeJS
bindings Node will pass the task on to `libuv`. `libuv` delegates the fetching
of the web page resources to the OS. Node then gets notified when the request is
complete. This is **asynchronous I/O** in action, and it's what node is BEST at!

![what node does](./screenshots/what-node-does.png)
