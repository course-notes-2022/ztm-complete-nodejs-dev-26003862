# Node Internals Deep Dive

We've talked about how the internals of node are structured. Let's look at the
code behind node.

The `nodejs` project is hosted on Github. You can browse it by going to
[github.com/nodejs/node](github.com/nodejs/node). Node is an **open-source**
project to which anyone can contribute.

The `lib` folder contains the JS side of the node APIs. Recall all the globals
and APIs that node provides for us. Each of these has a corresponding file in
the `lib` folder: `http`, `process`, `fs`, etc.

The other folder of interest is `src`, the C++ side with the low-level
**bindings**. Investigate `lib/fs.js`, and search for the `open` function:

```js
/**
 * Asynchronously opens a file.
 * @param {string | Buffer | URL} path
 * @param {string | number} [flags]
 * @param {string | number} [mode]
 * @param {(
 *   err?: Error,
 *   fd?: number
 *   ) => any} callback
 * @returns {void}
 */
function open(path, flags, mode, callback) {
  path = getValidatedPath(path);
  if (arguments.length < 3) {
    callback = flags;
    flags = 'r';
    mode = 0o666;
  } else if (typeof mode === 'function') {
    callback = mode;
    mode = 0o666;
  } else {
    mode = parseFileMode(mode, 'mode', 0o666);
  }
  const flagsNumber = stringToFlags(flags);
  callback = makeCallback(callback);

  const req = new FSReqCallback();
  req.oncomplete = callback;

  binding.open(pathModule.toNamespacedPath(path), flagsNumber, mode, req);
}
```

Note the `binding.open` call. This is where the **internal bindings** come in.
This function will convert the values between JS in the `fs` module and C++. We
can see the full implementation of this function in `src/node_file.cc`. The
following line:

```cpp
env->SetMethod(target, "open", Open);
```

is **associating the JS `open` function** with the `Open` function defined
inside this file:

```cpp
static void Open(const FunctionCallbackInfo<Value>& args) {
  Environment* env = Environment::GetCurrent(args);

  const int argc = args.Length();
  CHECK_GE(argc, 3);

  BufferValue path(env->isolate(), args[0]);
  CHECK_NOT_NULL(*path);

  CHECK(args[1]->IsInt32());
  const int flags = args[1].As<Int32>()->Value();

  CHECK(args[2]->IsInt32());
  const int mode = args[2].As<Int32>()->Value();

  if (CheckOpenPermissions(env, path, flags).IsNothing()) return;

  if (argc > 3) {  // open(path, flags, mode, req)
    FSReqBase* req_wrap_async = GetReqWrap(args, 3);
    CHECK_NOT_NULL(req_wrap_async);
    req_wrap_async->set_is_plain_open(true);
    FS_ASYNC_TRACE_BEGIN1(
        UV_FS_OPEN, req_wrap_async, "path", TRACE_STR_COPY(*path))
    AsyncCall(env, req_wrap_async, args, "open", UTF8, AfterInteger,
              uv_fs_open, *path, flags, mode);
  } else {  // open(path, flags, mode)
    FSReqWrapSync req_wrap_sync("open", *path);
    FS_SYNC_TRACE_BEGIN(open);
    int result = SyncCallAndThrowOnError(
        env, &req_wrap_sync, uv_fs_open, *path, flags, mode);
    FS_SYNC_TRACE_END(open);
    if (is_uv_error(result)) return;
    env->AddUnmanagedFd(result);
    args.GetReturnValue().Set(result);
  }
}
```

Note the `uv_fs_open` line. This references a **function** in `libuv`.
