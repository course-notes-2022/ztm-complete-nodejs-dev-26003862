# Is JavaScript Asynchronous?

Javascript is a **synchronous** language _out of the box_. However, we can also
write **asynchronous** code. When JS runs in environments such as a browser or
in a runtime like Node, we can leverage **globals** in those environments (such
as `setTimeout` from the `window` object) to write JS code **asynchronously**.
