# Synchronous vs. Asynchronous

Let's now look at the term "asynchronous", and what it means. "Async" is often
described as the difference between "synchronous". "Synchronous" describes a
process that happens **in sequence**, with **no step starting before another
completes**.

In contrast, an **async** process has steps that can happen **out of order**, or
at **unpredictable times**, or complete at **unpredictable times**.

In our programs, we often have **async code** that starts/stops/continues
executing while other pieces of code are also executing. Asynchronous functions
can run in the background while other tasks are still completing, handling
**many things simultaneously**. This is where **NodeJS excels**!
