# Observer Design Pattern

Let's learn about the **event-driven** properties of Node.

How do we as developers react to events from the event loop?

## The Observer Design Pattern

The Observer pattern involves an entity called a **Subject**, that periodically
does something of interest to some outside parties called **Observers**. The
Subject **notifies** the Observers when it has done something (an **event**) in
which the Observers have expressed interest, by **subscribing** to the event. As
long as the Observers are subscribed, they will be kept up to date on the events
of interest to them:

![observer design pattern](./screenshots/observer-design-pattern.png)
