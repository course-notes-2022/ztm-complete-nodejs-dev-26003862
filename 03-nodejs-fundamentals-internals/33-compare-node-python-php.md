# Comparing Node with PHP and Python

We've seen that Node is different from Java and C++ in that those languages
support **multi-threading**. What about high-level, single-threaded languages
like PHP and Python?

In the "old days" of the web, PHP and Python followed a model in which they
needed a web server like Apache to handle incoming requests. With Apache, any
request coming in would be handled by a **new thread**. Thus the server could
handle only as many requests as it had threads. Building sites that served
millions of users was difficult and **expensive**,

In 2009, Node became popular because of its non-blocking I/O model. Node takes
the requests coming in via the JS engine to be handled by `libuv`, skipping over
the thread pool whenever possible and leveraging the OS' threads. There's no
need for a web server like Apache, creating thousands of threads.

Python has integrated support for running an event loop, similarly to Node.
However, neither PHP nor Python make it as easy to perform async, non-blocking
I/O as efficiently.
