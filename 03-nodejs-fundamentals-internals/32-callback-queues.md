# Callback Queues

We now know that the event loop is a piece of code in `libuv` that processes
events in a loop. Node automatically enters this loop when it starts.

What happens when the event loop processes events?

When node executes an async function, that operation is sent to be executed in
the background (on our OS or the thread pool). When that operation **finishes**,
node places any callback functions for that operation on a **callback queue**,
to be executed asap.

The callback queue keeps track of **any callback functions that need to be
executed**. The callbacks are added in a **FIFO** manner. The first function to
be put **on** the queue is the first to be **executed**.
