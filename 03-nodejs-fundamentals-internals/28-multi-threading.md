# Multi-Threading, Processes, and Threads

Is NodeJS multi-threaded? What _is_ a thread? How do threads help us?

A **process** represents a program currently being executed. Observe the
following diagram:

![process-thread diagram](./screenshots/process-thread-diagram.png)

A **process** on our machine, such as Chrome or Node, as a **container** that
**contains the code** that lives in the memory of that process. We could have
some code that contains `firstFunction` and `secondFunction`, and creates two
new "threads" that run `firstFunction`. Java and C++ work in this way, running
code in multiple threads **in parallel**.

The process will create two "threads", each of which represents a **single
sequence of operations** in a **call stack**. Each thread runs
**asynchronously** in relation to one another; thread 2 may call **entirely
different functions** than thread 1. The threads share **memory and code**, but
_not call stacks_. This allows them to execute **in parallel**.

Sounds great, right? There's a catch...

## The Problem With Threads

Multi-threaded programming is **difficult to master**, even for experienced
programmers. It's difficult to reason about and understand, as well as debug, in
basically all multi-threaded software. The creator of Javascript intentionally
made it **single-threaded** to avoid the pitfalls of multi-threaded programs.

But how is node allowing us to do things like open files and make HTTP requests
**asynchronously**? Is it introducing multi-threading into Javascript?
