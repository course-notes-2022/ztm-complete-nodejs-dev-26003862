const EventEmitter = require('node:events');

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();
myEmitter.on('event', () => {
  console.log('an event occurred!');
});

// Any number of subscribers can subscribe to
// the same event emiter
myEmitter.on('event', () => {
  console.log('I saw an event!');
});

process.on('exit', (code) => {
  console.log('Process exit event with code: ', code);
});

myEmitter.emit('event');
