# Non-blocking I/O

What is a non-blocking function? Non-blocking code executes **synchronously**;
any commands after non-blocking functions must wait until the function
completes.

Non-blocking functions such as `setTimeout` execute in the background or in
parallel with other code.

We want to ensure that our long-running functions are **non-blocking**, allowing
Node to execute other code.
