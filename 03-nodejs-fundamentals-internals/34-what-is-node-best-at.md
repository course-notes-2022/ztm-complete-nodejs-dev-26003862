# What is Node Best At?

When is it best to use Node?

Node is _not_ very good at blocking, processor-heavy computations such as video
processing or machine learning. It's _possible_ to use node, but it won't give
you many advantages.

Node _is_ very good at **servers**. It works really well when the main problem
is I/O performance, such as video-streaming, web servers, or any activity
involving serving **data**.
