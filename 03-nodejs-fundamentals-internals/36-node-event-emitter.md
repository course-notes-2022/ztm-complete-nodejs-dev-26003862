# The Node Event Emitter

We learned about the Observer design pattern. In Node, the way we get notified
of events is through callback functions. Node has a built-in module that helps
us work with events, the [Events module](https://nodejs.org/api/events.html).
Much of the node API is built around async event-driven architecture in which
certain kinds of objects called "emitters" emit named events that cause Function
objects "listeners" to be called. Here's an example from the node docs:

```js
const EventEmitter = require('node:events');

class MyEmitter extends EventEmitter {}

// Subscribe to EventEmitter by calling the `on` function
const myEmitter = new MyEmitter();
myEmitter.on('event', () => {
  console.log('an event occurred!');
});

// Any number of subscribers can subscribe to
// the same event emiter
myEmitter.on('event', () => {
  console.log('I saw an event!');
});

myEmitter.emit('event');
```

Run this code in the Node REPL, and observe the following output:

```
an event occurred!
I saw an event!
```

This pattern is used in real life in the `Process` module in Node. `Process` is
an instance of `EventEmitter`. It has an `on` function that can react to
different events in the Node lifecycle:

```js
const EventEmitter = require('node:events');

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();
myEmitter.on('event', () => {
  console.log('an event occurred!');
});

myEmitter.on('event', () => {
  console.log('I saw an event!');
});

process.on('exit', (code) => {
  console.log('Process exit event with code: ', code);
});

myEmitter.emit('event');
```

Upon exiting the program, this code will print the exit message with the status
code (0 indicates success).
