# Resource: Security Cheat Sheet

To learn more about Node.js security best practices, I recommend this resource:

[OWASP Node.js Security Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/Nodejs_Security_Cheat_Sheet.html)

It provides a great summary of actions you can take to build secure Node.js
applications.

The OWASP Foundation is a community that works to improve the security of
software. Their resources are the go-to for companies large and small when it
comes to building secure web applications.

Happy coding!
