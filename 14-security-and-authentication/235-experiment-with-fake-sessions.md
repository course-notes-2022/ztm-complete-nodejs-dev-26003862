# Experiment with Fake Sessions

Imagine that we've just signed out. What happens if we log in and populate our
browser session again (by logging in via the Google flow)?

Log in again, and copy the value of the `session` cookie. Paste it into a **base
64 encoder/decoder** tool that you can find online. Recall that the session
value is a **base64 encoded string** or **whatever encoding `cookie-session` is
currently configured to use**.

Our session cookie value can be **decoded**.

Let's try an experiment. Take the decoded value, modify the user ID, and
**encode** it again. What happens if we take this encoded value and claim to our
server that **we're now a _different_ user, by putting it _inside our cookie_**?
What happens if we try to access the secret endpoint with a **tampered
session**?

Our server will **reject the request**, and tell us that we need to log in as a
valid user. Our tampered cookie is **not properly signed** with the **secrets**
our server uses to sign cookies, and `passport` tells it that **no user was
authenticated**. Things are working as we want them to!
