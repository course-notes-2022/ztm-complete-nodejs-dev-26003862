const https = require('https');
const fs = require('fs');
const path = require('path');
const helmet = require('helmet');
const passport = require('passport');
const { Strategy } = require('passport-google-oauth20');
const cookieSession = require('cookie-session');
const express = require('express');
const app = express();

require('dotenv').config();

const config = {
  CLIENT_ID: process.env.CLIENT_ID,
  CLIENT_SECRET: process.env.CLIENT_SECRET,
  COOKIE_KEY_1: process.env.COOKIE_KEY_1,
  COOKIE_KEY_2: process.env.COOKIE_KEY_2,
};

function verifyCallback(accessToken, refreshToken, profile, done) {
  console.log('Google profile:', profile);
  done(null, profile); // Call `done` if accessToken & refreshToken are VALID to supply passport with the authenticated user
}

// Set passport strategy
passport.use(
  new Strategy(
    {
      callbackURL: '/auth/google/callback',
      clientID: config.CLIENT_ID,
      clientSecret: config.CLIENT_SECRET,
    },
    // verify function: callback function that passport will invoke
    // when it authenticates a user
    verifyCallback
  )
);

// Save the session to the cookie
passport.serializeUser((user, done) => {
  done(null, user.id);
});

// Read the session from the cookie
passport.deserializeUser((id, done) => {
  done(null, id);
});

const PORT = 3000;

// `use()` helmet as early as possible in the middleware chain
// to ensure that ALL REQUESTS are subject to its security features
app.use(helmet());

app.use(
  cookieSession({
    name: 'session',
    maxAge: 24 * 60 * 60 * 1000,
    keys: [config.COOKIE_KEY_1, config.COOKIE_KEY_2], // list of secrets that cookieSession will use to sign cookie
  })
);

app.use(passport.initialize());
app.use(passport.session());

function checkLoggedIn(req, res, next) {
  //`passport.session` middleware is currently setting
  // `req.user`
  const isLoggedIn = req.isAuthenticated() && req.user ? true : false;
  if (!isLoggedIn) {
    return res.status(401).json({ err: 'You must log in!' });
  }
  next();
}

app.get('/', (req, res) => {
  res.status(200).sendFile(path.join(__dirname, 'public', 'index.html'));
});

// Define our login endpoint to let the user log in
app.get('/auth/google', passport.authenticate('google', { scope: ['email'] }));

// Callback URL to which Google will send our authorization code
app.get(
  '/auth/google/callback',
  passport.authenticate('google', {
    failureRedirect: '/failure',
    successRedirect: '/',
    session: true,
  }),
  (req, res) => {
    console.log('Google called us back!');
  }
);

app.get('/failure', (req, res) => {
  return res.send('Failed to log in');
});

// Logout route
app.get('/auth/logout', (req, res) => {
  req.logout(); // Removes req.user and clears any logged-in session
  return res.redirect('/'); // Redirect the user to the root route on logout
});

// Protect the `/secret` route with the `checkLoggedIn` middleware
app.get('/secret', checkLoggedIn, (req, res) => {
  return res.send('Your personal secret value is 42!');
});

https
  .createServer(
    {
      key: fs.readFileSync('key.pem'),
      cert: fs.readFileSync('cert.pem'),
    },
    app
  )
  .listen(PORT, () => {
    console.log(`server listening on port: ${PORT}`);
  });
