# Encrypted Connections with SSL and TLS

Before adding authentication, one thing we'll do often with our Node servers is
making sure they use **HTTPS**, the secure version of HTTP that **encrypts our
data as its being sent in transit**.

So far, we've used HTTP only for sending data to and from our server. This is
_okay_ for demo applications, but **best practice is to _always_ use HTTPS** to
ensure our data is being encrypted in tranist between the client and the server.
HTTPS prevents other users from **eavesdropping on/tampering with our data in
transit**.

## How Does HTTPS Work?

When we browse to a site using an HTTP connection, the data in the
request/response can be read by **anyone with access to the network**. If we're
on a WiFi connection, for example, **anyone else on the network can inspect the
data** that we're sending/receiving across the network using a tool such as
`Wireshark`. That's **really bad** if our data includes passwords or other
sensitive data.

On the other hand, when we use an HTTPS connection, we're using a protocol that
leverages `SSL/TLS`. TLS is actually the successor to SSL, and is the latest
version of this encryption protocol. HTTPS doesn't _change_ HTTP; it simply
wraps our HTTP requests and responses in a **strong outer shell of encryption**.
With HTTPS, only the **domain** you're browsing to and the **port** number are
unencrypted. All of the **data** in the request body and any specific **paths**
to which you're browsing are strongly encrypted by TLS.
