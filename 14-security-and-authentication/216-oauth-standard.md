# The OAuth Standard

One of the most widely-used approaches when implementing login is the **OAuth
standard**:

![oauth diagram](./screenshots/oauth-diagram.png)

## What Problem Does OAuth Solve?

OAuth solves the problem of a **client** who wants to sign in to a particular
(untrusted) site, **without entering credentials into the site itself**.

Consider the login flow to [medium.com](https://medium.com):

- Clicking "login" and "Sign In with Google" takes us to **Google's login
  page**, with Google's certificates.

- We always see Google's official login screen and enter our password in
  **OAuth's server** (in this case, Google's server).

- The authorization server will eventually return an **access token** that you
  can use in your app to access data on **Medium's server**. You never have to
  input your password to Medium!

OAuth allows users to keep their passwords **more secure**, while avoiding the
need to remember **multiple passwords**.
