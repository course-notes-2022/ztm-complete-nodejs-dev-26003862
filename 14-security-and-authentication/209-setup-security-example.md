# Setting Up Our Security Example

How do we get our node servers to use HTTPS, and respond to HTTPS requests?

Let's look at an example. Initialize a new node project, and add the following
to `index.html`:

```js
const path = require('path');
const express = require('express');
const app = express();

const PORT = 3000;

app.get('/', (req, res) => {
  res.status(200).sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.get('/secret', (req, res) => {
  return res.send('Your personal secret value is 42!');
});
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
```

We can start up our server and visit the `/secret` route, but because we're
currently communicating over HTTP, **our data is not encrypted**. Let's learn to
fix this in the next lesson.
