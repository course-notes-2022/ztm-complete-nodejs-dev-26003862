# Wrap-up and Next Steps

We've done it!

- Implemented social sign on with Google, implementing the OAuth 2.0
  authorization code flow

- Learned about tokens and cookies, and saved our login in a user session

- Discussed digital certificates, securing our cookies, and XSS

## Next Steps

- Implementing **refresh tokens** so that our users don't have to login all over
  again when their access token **expires**

- Manage sessions using **databases** (if we want to do server side sessions)

- Add more levels of security: giving different users different kinds of
  permission with **Role-Based Access Control** (RBAC)

Security is a **vast topic**. Hopefully this section has given you a practical
explanation of how authentication and authorization works in a **real-life**
production Node application, as well as the **general concepts** that help to
understand what's happening "under the hood".
