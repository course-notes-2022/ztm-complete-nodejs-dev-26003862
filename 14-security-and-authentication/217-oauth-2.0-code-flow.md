# OAuth 2.0 Authorization Code Flow

Let's understand the OAuth flow:

![oauth-diagram](./screenshots/oauth-diagram.png)

Let's start by looking at the different **roles** in the OAuth Flow:

1. The **Resource Owner**: the user of the application, e.g. the **end user** of
   `Medium.com` in the previous lesson

2. The **Client**: the **web application** that is attempting to access
   protected data on the **Resource Server** on behalf of the user

3. The **Resource Server**: usually the **backend** for the client. Restricts
   access to data to only the resource owner

4. The **Authorization Server**: the server that authenticates the resource
   owner, and issues access tokens that allow the user to access the resource
   server

For example, in the following diagram imagine that our **resource owner** or end
user is attempting to access **protected information** via the **client** at
`www.myapp.com`. This protected information lives on the **resource server** at
`api.myapp.com`.

The resource server at `api.myapp.com` requires the resource owner to
**authenticate himself** before allowing access to the requested resources. It
is the **authorization server** at `accounts.google.com` that will perform the
authentication, and issue the token credential that will allow the resource
owner to access the resource server's data via the client.

![oauth 2 roles](./screenshots/oauth_2_roles.png)
