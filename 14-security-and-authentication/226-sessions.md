# Sessions

**Sessions** is a topic that will definitely come up when building a large,
authenticated application. Sessions are a way of **storing data about the
current active user**.

What kinds of data should live in a session? Data that we don't want the user to
be able to **modify**, for example:

- A user's **bank balance**
- Permissions to access data in the server
- etc.

We want the client to be able to **access** this information, but not be able to
**modify** it directly.

Sessions are usually **short-lived**, and capture the state of an application
**during the time the user is interacting with it**, until they quit or log out.
