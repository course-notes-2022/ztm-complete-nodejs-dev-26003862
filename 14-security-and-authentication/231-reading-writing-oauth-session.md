# Reading and Writing to OAuth Session: Part 1

We've set up our `cookie-session` middleware. Let's now hook passport up with it
so that our **session is populated with the user's data** when Google calls our
`/auth/google/callback` endpoint:

```js
// Callback URL to which Google will send our authorization code
app.get(
  '/auth/google/callback',
  passport.authenticate('google', {
    failureRedirect: '/failure',
    successRedirect: '/',
    session: true,
  }),
  (req, res) => {
    console.log('Google called us back!');
  }
);
```

and our cookie with the login information is returned to the browser.

Let's return to our `passport` middleware for a moment. `passport.initialize()`
is middleware that sets up the passport session. The `passport` session is
populated by two functions:

- `passport.serializeUser`: serializes the user data to the session
- `passport.deserializeUser`: takes the cookie value and places it into the
  `req` object in all our middleware and request handler functions on the
  `req.user` property

We can define the `serializeUser` and `deserializeUser` functions ourselves,
directly on the `passport` object:

```js
// Save the session to the cookie
passport.serializeUser((user, done) => {
  done(
    null,
    user // set value of user
  );
});

// Read the session from the cookie
passport.deserializeUser((obj, done) => {
  done(
    null, // first param is an error; null if no errors
    obj // deserialization result; passing cookie for now
  );
});
```

## Saving the Session on Login And Serialize it in a Cookie

To save the session on user login and serialize it in a cookie, we need to set
the `session` property to `true` as follows:

```js
app.get(
  '/auth/google/callback',
  passport.authenticate('google', {
    failureRedirect: '/failure',
    successRedirect: '/',
    session: true, // Set `session` to `true`
  }),
  (req, res) => {
    console.log('Google called us back!');
  }
);
```

## Add `passport.session` Middleware

So passport understands our cookie-session, and the `request.user` object that
is set by the `cookie-session` middleware:

```js
app.use(
  cookieSession({
    name: 'session',
    maxAge: 24 * 60 * 60 * 1000,
    keys: [config.COOKIE_KEY_1, config.COOKIE_KEY_2],
  })
);

app.use(passport.initialize());
app.use(passport.session()); // Call app.use on passport.session
```

This authenticates the **session** that is being sent to our server, and that it
is **properly signed** with the `keys` we defined. It then sets the `req.user`
property to contain the user's data; i.e. it allows the `deserializeUser`
function to be called.

## Testing our Code

We can now test our code and observe our cookie being set with the user
information in the browser! Open a new incognito window and click the "Google
Login" link. Log in with your credentials, and observe in the Network and Cookie
Storage that we now have a `session` cookie set in our browser, with the Google
profile information set (base64 encoded, _not encrypted_).

## Full `index.js`

```js
const https = require('https');
const fs = require('fs');
const path = require('path');
const helmet = require('helmet');
const passport = require('passport');
const { Strategy } = require('passport-google-oauth20');
const cookieSession = require('cookie-session');
const express = require('express');
const app = express();

require('dotenv').config();

const config = {
  CLIENT_ID: process.env.CLIENT_ID,
  CLIENT_SECRET: process.env.CLIENT_SECRET,
  COOKIE_KEY_1: process.env.COOKIE_KEY_1,
  COOKIE_KEY_2: process.env.COOKIE_KEY_2,
};

function verifyCallback(accessToken, refreshToken, profile, done) {
  console.log('Google profile:', profile);
  done(null, profile);
}

passport.use(
  new Strategy(
    {
      callbackURL: '/auth/google/callback',
      clientID: config.CLIENT_ID,
      clientSecret: config.CLIENT_SECRET,
    },
    verifyCallback
  )
);

// Save the session to the cookie
passport.serializeUser((user, done) => {
  done(null, user);
});

// Read the session from the cookie
passport.deserializeUser((obj, done) => {
  done(null, obj);
});

const PORT = 3000;

app.use(helmet());

app.use(
  cookieSession({
    name: 'session',
    maxAge: 24 * 60 * 60 * 1000,
    keys: [config.COOKIE_KEY_1, config.COOKIE_KEY_2],
  })
);

app.use(passport.initialize());
app.use(passport.session());

function checkLoggedIn(req, res, next) {
  const isLoggedIn = true; //TODO
  if (!isLoggedIn) {
    return res.status(401).json({ err: 'You must log in!' });
  }
  next();
}

app.get('/', (req, res) => {
  res.status(200).sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.get('/auth/google', passport.authenticate('google', { scope: ['email'] }));

app.get(
  '/auth/google/callback',
  passport.authenticate('google', {
    failureRedirect: '/failure',
    successRedirect: '/',
    session: true,
  }),
  (req, res) => {
    console.log('Google called us back!');
  }
);

app.get('/failure', (req, res) => {
  return res.send('Failed to log in');
});

app.get('/auth/logout', (req, res) => {});

app.get('/secret', checkLoggedIn, (req, res) => {
  return res.send('Your personal secret value is 42!');
});

https
  .createServer(
    {
      key: fs.readFileSync('key.pem'),
      cert: fs.readFileSync('cert.pem'),
    },
    app
  )
  .listen(PORT, () => {
    console.log(`server listening on port: ${PORT}`);
  });
```
