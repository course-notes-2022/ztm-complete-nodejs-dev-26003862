# `dotenv` for Client Secrets

How are we going to connect our Node application with Google, so that Google
knows that our app is the one requesting users to log in? We use the **client
ID** and **client secret**.

We'll need to leverage these values in our server. Remember though that we
**need to keep our client secret...SECRET**. We don't want to expose our secret
in our source code that we might be publishing to source code management
solutions or elsewhere!

It's best practice to take these values from a **configuration file** or
**environment variable**. The most common way to accomplish this in Node is with
the [`dotenv` package](https://www.npmjs.com/package/dotenv). `dotenv` loads
environment variables that we define in a `.env` file into `process.env`. We can
also define multiple `.env` files for use in dev, test and prod environments.
Let's add it to our project!

> `npm install dotenv`

Refactor `server.js` as follows to use `dotenv`:

```js
const https = require('https');
const fs = require('fs');
const path = require('path');
const helmet = require('helmet');
const express = require('express');
const app = express();

require('dotenv').config();

const config = {
  CLIENT_ID: process.env.CLIENT_ID,
  CLIENT_SECRET: process.env.CLIENT_SECRET,
};

// OMITTED...
```

Create a new `.env` file with the values of your client ID and secret from
Google:

```
CLIENT_ID=your_client_id_here
CLIENT_SECRET=your_client_secret_here
```

Finally, add the path to your `.env` file to `.gitignore`, to **avoid checking
your secrets into SCM**.
