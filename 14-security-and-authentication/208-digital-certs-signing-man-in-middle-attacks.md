# Digital Certificates, Signing, and Man-In-The-Middle Attacks

We've learned that HTTPS uses SSL/TLS protocols to encrypt HTTP connections. For
data to be encrypted, we need a **digital certificate**. A digital certificate
is used to verify the server's ownership prior to sending encrypted data.

Just as with a physical certificate, if the **signature on the cert is valid**,
and we **trust the issuing authority of the cert**, we say that we're willing to
use the key the server sent to send encrypted data back and forth to that
server. This helps us verify that we're talking to the server we **expect**.

## Man In the Middle Attacks

It's not enough that the data is encrypted if we're sending data to a
**malicious hacker** who **has the key to decrypt the data**. This is what's
called a **man in the middle** attack. With digital certificates, we can verify
that the server we're sending our data to is the **right party**.

## Certificate Authorities

We mentioned that certificates are **signed**. They're signed by a **certificate
authority**, an organization that your browser **already trusts** to issue valid
certificates and **verify the real server's owner**.

In the early days, you had to buy these certificates from CAs that could charge
potentially hundreds of dollars. Thankfully, today there are CAs that allow us
to sign certificates for **free**. One of the most famous is `Let's Encrypt`.
However, `Let's Encrypt` will only issue you a certificate under certain
conditions: for example, you must have a **valid, registered domain name** and
not just an IP address.

This is why **self-signed-certificates** are useful. Self-signed certificates
enable HTTPS, but are _not trusted by others_. They're very useful for
**development** purposes. They still allow us to encrypt our traffic and use
HTTPS on our local machine. In **production**, we **always want to use CA-signed
certificates**.

All TLS certifcates can be divided into CA-signed or Self-Signed certificates.
