# Security and Authentication Overview

In this section, we'll learn about **security and authentication**. We'll learn:

- Social sign-on
- How to secure our node servers
- Security best practices

We _could_ talk about how to implement our own authentication with cookies and
tokens, but _in the real world, you'll pretty much **never do this**_ especially
on large projects where security is **important**. In fact, you _shouldn't_ be
implementing security from scratch, and instead use **proven solutions**.

In the real world you'll almost always be using an **authentication service**,
such as Amazon Cognito, Okta, social sign-on, etc. The thing that all these
services have in common is that the follow a **standard** such as **OAuth**, the
most widely-used standard for authentication on the internet. We'll learn to use
these techniques and topics in this chapter.
