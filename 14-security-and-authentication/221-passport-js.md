# `passport.js`

How are we going to log our user in with Google following the OAuth flow?

We'll use a popular middlware called
[`passport.js`](https://www.passportjs.org/). Passport is an `npm` package that
provides authentication for node applications. It's been downloaded millions of
times, and exposes a **common approach to authentication**, regardless of
provider, standard, or flow.

Passport uses the concept of **strategies**, which it uses to authenticate
requests:

> _"Strategies are responsible for authenticating requests, which they
> accomplish by implementing an authentication mechanism. Authentication
> mechanisms define how to encode a credential, such as a password or an
> assertion from an identity provider (IdP), in a request. They also specify the
> procedure necessary to verify that credential. If the credential is
> successfully verified, the request is authenticated."_

Let's install the `passport-google-oauth20` strategy with the following command:

> `npm install passport-google-oauth20`

As well as the main `passport` project:

> `npm install passport`
