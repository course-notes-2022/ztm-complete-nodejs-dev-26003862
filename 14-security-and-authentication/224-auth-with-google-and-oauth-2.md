# Authentication with Google and OAuth: Part 2

Let's implement the `callbackUrl` and the actual `/login` endpoint.

We've set up our `passport` strategy and the callback function for when the user
is authenticated. Let's look at the `callbackUrl` first.

The Google servers need to be able to call the `/auth/google/callback` endpoint
in order to exchange an **authorization code** for an **access token**:

```js
app.get('/auth/google/callback', (req, res) => {
  // ...
});
```

Passport gives us a **built-in** function that we can use as our route handler
callback: `passport.authenticate`. The first argument is a string that indicates
the **strategy** we're using. The second argument is an `options` object that
describes what to do if we **fail** or **succeed** to authenticate. The third
argument is the **request handler** callback. We can optionally check for
failure/success **here**, and handle them manually:

```js
// Callback URL to which Google will send our authorization code
app.get(
  '/auth/google/callback',
  passport.authenticate('google', {
    failureRedirect: '/failure',
    successRedirect: '/',
    session: false, // more on this later
  }),
  (req, res) => {
    console.log('Google called us back!');
  }
);
```

This `passport.authenticate` middleware will handle the entire flow from when
Google sends us the **authorization code**, to our request back to Google to get
the **access token**, and Google's **response with the token**.

## Implementing the Flow From the Browser

The process kicks off when our client sends a request to the `/auth/google`
endpoint of our server. We can use `passport.authenticate` here as well:

```js
app.get(
  '/auth/google',
  passport.authenticate(
    'google', // strategy
    { scope: ['email'] } // scope
  )
);
```

This time, instead of passing redirect URLs, we pass the `scope`. Recall that
`scope` specifies **which data we're requesting from Google** when
authentication succeeds.

That's it! That's using `passport` to help us with the Google login flow. Test
the flow in the browser by clicking the "Login with Google" link. You should be
taken through the login screen we set up in the Google API console earlier.
