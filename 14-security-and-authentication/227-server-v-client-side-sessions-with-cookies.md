# Server vs. Client-Side Sessions with Cookies

We use **sessions** to store temporary user data **as the user is using the
application**. Most often, sessions are used to store information about the user
in the user's browser.

## How do we Store Sessions?

1. On the **server side**: user data lives in the server, on a database. The
   data will get looked up for each request.

2. On the **client side**: session data is stored in the browser, using
   **cookies**.

We use either **stateful** or **stateless** cookies. A stateful cookie contains
a reference to some session information that lives in a database on the server.
Thus, we use stateful cookies with **server-side** sessions.

**Stateless cookies**, used with **client-side** sessions, store all the session
data in the **browser**. The server can trust that the cookies haven't been
tampered with by **signing the cookies** before it sends them to the browser.
For example, to store authentication information for a user that has
successfully authenticated to the server, the server can send back the user ID
and store it in a cookie that lives in the browser. To prevent tampering with
the cookie, the server can encrypt or sign the cookie value using a **secret
key** to which only the server has access.
