# Restricting Access to Endpoints

Let's now complete our `checkLoggedIn` middleware so that only authenticated
users have access to restricted endpoints.

Right now, `checkedLoggedIn` is passing `true` as a hardcoded value:

```js
function checkLoggedIn(req, res, next) {
  // req.user
  const isLoggedIn = true; //TODO
  if (!isLoggedIn) {
    return res.status(401).json({ err: 'You must log in!' });
  }
  next();
}
```

Let's fix that, so we're **actually securing our secrets**. Our
`passport.session` middleware is setting `req.user` on our Express `Request`
object. `req.user` will be the **deserialized user read from the incoming
browser cookie** by the `deserializeUser` function.

To check whether a user is logged in, we can simply check whether `req.user`
**exists**. If _yes_, it means that:

- `passport.session` has validated the session
- it has deserialized it and given us the `id`
- and the `id` is available to us

Make the following change to `checkLoggedIn`:

```js
function checkLoggedIn(req, res, next) {
  const isLoggedIn = req.user ? true : false; // The user is logged in if `req.user` is truthy
  if (!isLoggedIn) {
    return res.status(401).json({ err: 'You must log in!' });
  }
  next();
}
```

Attempt to visit `/secret` while **not authenticated** (_sign out of your Google
account_, or open a new incognito window). Note that **our application returns a
`401`, with the `you must log in` message!**

## Extra Security

We can use the **built in** `req.isAuthenticated` function on passport to ensure
that our user is authenticated; `req.user` _could_ be set by other middleware,
so we don't want to rely on it alone:

```js
function checkLoggedIn(req, res, next) {
  const isLoggedIn = req.isAuthenticated() && req.user ? true : false;
  if (!isLoggedIn) {
    return res.status(401).json({ err: 'You must log in!' });
  }
  next();
}
```
