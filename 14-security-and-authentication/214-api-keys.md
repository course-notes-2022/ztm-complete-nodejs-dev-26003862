# API Keys

There are 3 main tools developers use to ensure that only authorized users can
access an API:

- API Keys
- OAuth Tokens
- JSON Web Tokens (JWT)

Let's start by discussing API keys.

## What is an API Key?

An **API Key** is a unique string identifier for the application that is making
the request to the server. API keys are usually used when the users of the API
are **other developers** who want to leverage your API.

API Keys are sometimes **public**, i.e. the key value is exposed. This is often
the case when the key is used only to identify which application is using the
API. Usually though, we want to keep API Keys **private**, and not make them
available in the **front-end client code**.

API Keys are to be passed along with **each request to the server**. The server
can do things like **limit the amount of requests** that can be made to the API
server in a particular amount of time ("rate limiting").

Google Maps is an example of an API that uses API Keys. We can create a project
and register it with Google Maps, and then create a new API key for our project.
We'll want to **restrict our key** to only be used under certain conditions:

- By websites only
- By IP addresses
- By Android/iOS apps
- By specified websites
- etc.

The API at Google checking our key would enforce the restrictions. We also want
to follow the principle of **least privilege**, and give our key the **minimum
permissions it needs** to do what we need to do.
