# Authentication Endpoints with Middleware

Let's add Google signon into our security example project. We're going to
**protect the `/secret` endpoint** so that only authenticated users can access
it. Typically, we do this in **middleware** on the request handler. If we want
to restrict access to **all** endpoints, we'd use a middleware function that
returns an error output if the user is not authenticated/authorized, and calls
`next` if they are:

```js
app.use((req, res, next) => {
  const isLoggedIn = true; //TODO
  if (!isLoggedIn) {
    return res.status(401).json({ err: 'You must log in!' });
  }
  next();
});
```

This would protect **every** route, which is not what we want to do in our case.
Express allows us to add middleware **individually** to a route handler. **This
is how we do authorization in Express**:

```js
const https = require('https');
const fs = require('fs');
const path = require('path');
const helmet = require('helmet');
const express = require('express');
const app = express();

const PORT = 3000;

app.use(helmet());

// Define `checkLoggedIn` middleware function
function checkLoggedIn(req, res, next) {
  const isLoggedIn = true; //TODO
  if (!isLoggedIn) {
    return res.status(401).json({ err: 'You must log in!' });
  }
  next();
}

app.get('/', (req, res) => {
  res.status(200).sendFile(path.join(__dirname, 'public', 'index.html'));
});

// Define our login endpoint to let the user log in
app.get('/auth/google', (req, res) => {});

// Callback URL to which Google will send our authorization code
app.get('/auth/google/callback', (req, res) => {});

// Logout route
app.get('/auth/logout', (req, res) => {});

// Protect the `/secret` route with the `checkLoggedIn` middleware
app.get('/secret', checkLoggedIn, (req, res) => {
  return res.send('Your personal secret value is 42!');
});

https
  .createServer(
    {
      key: fs.readFileSync('key.pem'),
      cert: fs.readFileSync('cert.pem'),
    },
    app
  )
  .listen(PORT, () => {
    console.log(`server listening on port: ${PORT}`);
  });
```
