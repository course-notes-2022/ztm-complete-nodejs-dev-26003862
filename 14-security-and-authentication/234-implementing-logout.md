# Implementing Logout

How do we implement signout functionality in our Express application? We can log
out "manually" by clearing the cookies from our developer tools, but what if we
want to allow the user to log out by clicking a button or link?

Passport exposes a `logout` function on the `req` object that we can call to
**clear any logged-in user session**, and \*\*remove the `req.user` property
from the `Request` object:

```js
// Logout route
app.get('/auth/logout', (req, res) => {
  req.logout(); // Removes req.user and clears any logged-in session
  return res.redirect('/'); // Redirect the user to the root route on logout
});
```
