# Helmet.js

As Node developers, we need to **secure our servers against common issues**.
[`Helmet.js`](https://helmetjs.github.io/) is a widely-used npm package that
contains middleware that allows us to secure our servers. Let's add `helmet` to
our security example:

> `npm install helmet`

Once installed, we can `require` and use `helmet` as follows:

```js
const https = require('https');
const fs = require('fs');
const path = require('path');
const helmet = require('helmet');
const express = require('express');
const app = express();

const PORT = 3000;

// `use()` helmet as early as possible in the middleware chain
// to ensure that ALL REQUESTS are subject to its security features
app.use(helmet());

app.get('/', (req, res) => {
  res.status(200).sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.get('/secret', (req, res) => {
  return res.send('Your personal secret value is 42!');
});

https
  .createServer(
    {
      key: fs.readFileSync('key.pem'),
      cert: fs.readFileSync('cert.pem'),
    },
    app
  )
  .listen(PORT, () => {
    console.log(`server listening on port: ${PORT}`);
  });
```

Visit the `/secret` route in a browser. Inspect the headers in the developer
tools, and note the following:

![helmet security](./screenshots/helmet-security.png)

`helmet` is setting **all these security headers on our behalf**.
[Read up on each of the headers in the Helmet.js documentation](https://helmetjs.github.io/).
