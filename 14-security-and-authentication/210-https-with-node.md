# HTTPS with Node, Self-signed Certificates, and Public Key Cryptography

Let's put HTTPS to use in Node. Many services exist, such as Amazon CloudFront,
which serves your pages from a **Content Delivery Network** which gives us the
capability to serve our content over HTTPS. However, we'll look at how to **add
this capability in our Node server** first, to know how it works and to test
that our servers are communicating over HTTPS in development.

Recall that we can `require` the `http` module and call the `http.createServer`
method. The reason why we sometimes use this more verbose way is because of the
**flexibility** it provides. We can _also_ leverage the `https` module's
`createServer` method, and pass in an **object** that contains the following
options:

- `cert`: the **file path** to our certificate
- `key`: the **secret** we'll use to encrypt our data

## Creating the Certificate

We'll use `OpenSSL` to create our **certificate**. OpenSSL is an open-source
tool that allows us to generate self-signed certificates:

> `openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365`

The `req` option requests a new certificate. The `-x509` indicates that this is
a self-signed certificate. `newkey` is the **secret** we'll use to encrypt the
data. `rsa` is the encryption standard, and `:4096` specifies the number of bits
our key will be (larger values = stronger encryption). `-nodes` allows us to
access the key without a password for development purposes. `-keyout` specifies
the location to which we'll output our **key**, and `-out` specifies the
location to which we'll output out **certificate**. Finally, `-days` specifies
the number of days that our certificate will be valid. We want to specify the
days in case the key is ever stolen.

## What do we Get?

The **output** of the above command is:

- a `key.pem` file: Contains 4096 bits that ensure we are the **only ones
  allowed to encrypt data** for the server **identified by the cert**.
- a `cert.pem` file: Allows us to **decrypt data _encrypted_ by the `key`**, as
  the browser would do.

This approach is known as **public key cryptography**. With these two files, we
can now set the required properties in `https.createServer`:

```js
const https = require('https');
const fs = require('fs');
const path = require('path');
const express = require('express');
const app = express();

const PORT = 3000;

app.get('/', (req, res) => {
  res.status(200).sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.get('/secret', (req, res) => {
  return res.send('Your personal secret value is 42!');
});

https
  .createServer(
    {
      key: fs.readFileSync('key.pem'),
      cert: fs.readFileSync('cert.pem'),
    },
    app
  )
  .listen(PORT, () => {
    console.log(`server listening on port: ${PORT}`);
  });
```

Save changes and restart the server. Attempt to visit
`https://localhost:3000/secret`. You'll be presented with a screen similar to
the following:

![connection not secure](./screenshots/connection-not-private.png)

Our browser is alerting us to the fact that the **certificate is not signed by a
trusted certificate authority**. This is _expected_, because we are using a
**self-signed certificate**. We need to explicitly tell our browser that we
understand and trust the certificate, for development purposes. Once we do so,
the browser should remember that we trust this certificate.

Accept and continue. Note that we are **now communicating over HTTPS**. We can
still inspect our data on requests to/responses from the server in our developer
tools, but **third parties attempting to sniff our data will not be able to
decrypt it**!

![tls ok](./screenshots/tls-ok.png);
