# Authentication vs. Authorization

In many of our applications, we want the ability to **log our users in**, so
that not all users can access all data. There are two main concepts we need to
understand: **authentication** and **authorization**.

**Authentication** validates that our users **are who they claim to be**.
**Authorization** checks whether or not the user has **permission** to access
the resources they're requesting.

Authentication usually involves registering with an application and inputting a
username/password, but it can also involve 2-factor authentication with an app
or dedicated device, or even biometrics. Authentication involves proving as best
as possible that the user is who he/she says.

The second check for authorization involves checking whether the user is allowed
to access the resources he/she is attempting to access. These two concepts are
often combined into a single term "auth".

This knowledge applies to HTTP response codes as well. `401 (Unauthorized)`
**actually means _unauthenticated_**, whereas `403 (Forbidden)` means the user
is **_unauthorized_**.
