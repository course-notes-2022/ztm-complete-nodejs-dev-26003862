# Session Middleware in Express

How can we remember that the user has **logged in**? To remember that our users
are logged in, we'll use a **session**. We can use one of many packages, but
we'll look at `express-session` and `cookie-session` in this chapter.

`express-session` is for **server-side** sessions, and `cookie-session` is for
**client-side** sessions.

[`express-session`](https://github.com/expressjs/session) is middleware that
saves sessions on the server in express. It still uses cookies, but it saves
**only the session ID** in the cookie, and not any **session data**, which lives
in a DB on the server. `express-session` integrates with many different session
stores. **By default**, `express-session` uses an in-memory store that is wiped
upon server restart.

[`cookie-session`](https://github.com/expressjs/cookie-session) stores the
session data in the cookie that is stored on the **browser**, instead of just
the session id. It is _simpler_, because it doesn't require a database, or for
us to inspect each request on the server side for the incoming cookie. The
session data must be small, because the browser limits the size of cookies
(usually to 4096 bytes).

**Be aware that the data in our cookies is visible to our users in the
browser**. If we need to store any sensitive information in our cookies,
_server-side sessions are the better choice_. For our example though, we'll used
`cookie-session`. In the majority of cases though, it's enough to **sign the
data** using a key in the server to ensure it's not been tampered with.
