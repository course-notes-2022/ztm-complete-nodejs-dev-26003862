# Registering with the Google Authorization Server

Let's practice implementing OAuth in our **own applications**! The first step
will always be registering your application with the authorization server. We'll
register a Node app with Google to leverage their social sign-on.

Login to your Google account and search for the **Google API Console**. From
there, click "New Project" > Select a name for your project > click Create".

Select your new project, click "APIs and Services > Credentials > +Create
Credentials > OAuth Client Id".

## Configuring the Consent Screen

The next step is to configure the Consent Screen with the details of the
application:

![configure consent screen](./screenshots/configure-consent-screen.png)

The pertinent information here is:

- The **App Name**
- The **user support email**

Click "Create and Continue".

## Edit App Registration

On this screen we can select the "scopes", which give our application access to
specific **data** about the user account that just logged in:

![edit app registration](./screenshots/edit-app-registration.png)

We need to specify our scopes ahead of time, because the user will be asked to
give consent to share his/her account information. For our example, we will
select just the `email`. Click "Save and Continue".

## Test Users

We have the option to add test users to our app. When doing so, only these test
users will be allowed to log in to our app when the app is in **testing**.

## Pushing to Production

We start off in **testing** mode, but for our example we're OK with pushing to
production immediately. Because we're not requesting any sensitive data, we can
skip the verification step. Click "Publish App > Confirm".

## Creating Credentials

Click "Credentials > Create Credentials". Select "OAuth Client ID". Select the
type of your application, and add the application name.

### Authorized Javascript Origins and Authorized Redirect URIs

These settings control the authorized origins and redirect URIs that Google will
allow to log in using Google as the authorization server. If working on
localhost during development we need to add localhost as an authorized JS origin
and insert the **exact URI** of our **front-end** app that will be logging in
using Google, for example `https://localhost:3000`. For the **authorized
redirect URIs**, we add the exact URI for the **endpoint on our server that will
be handling authorization**.

Recall from our OAuth diagram:

![oauth diagram](./screenshots/oauth-diagram.png)

The authorization server will redirect the browser to this **redirect URI** once
the user has authenticated, logged in and consented, and the authorization code
response is ready (the "Authorization Code Response" step in the diagram). Let's
define the endpoint that will handle our redirect as
`https://localhost:3000/auth/google/callback`. We will need to define this
endpoint in our Node application.

On success, Google returns to us our **Client ID** as well as our **Client
Secret**. The secret is what is used to make sure **only your application will
be able to grant authorization**. We MUST protect it and keep it secret!
