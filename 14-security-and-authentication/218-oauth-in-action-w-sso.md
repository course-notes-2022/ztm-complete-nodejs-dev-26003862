# OAuth in Action with Single Sign-On

Let's see the OAuth flow in action.

Let's explore in the developer tools what **really happens** when we sign in to
Medium.com with Google. Open the developer tools and make sure the
`Preserve Log` option in the Network tab is checked.

**Step 1: Click "Sign in With Google"**. Observe that the browser makes a
request to `medium.com/connect/google`, which responds with a `302` redirect to
`accounts.google.com/o/oauth2/auth`:

![oauth flow 1](./screenshots/oauth-flow-1.png)

This is the first step, where our web app makes the Authorization Code Request
to the auth server and receives a `302` response.

Observe the query params that we pass to the `oauth2/auth?` endpoint:

```
https://accounts.google.com/o/oauth2/auth?operation=login&state=google-%7Chttps%3A%2F%2Fmedium.com%2F%3Fsource%3Dlogin--------------------------lo_home_nav-----------%7Clogin&access_type=online&client_id=216296035834-k1k6qe060s2tp2a2jam4ljdcms00sttg.apps.googleusercontent.com&redirect_uri=https%3A%2F%2Fmedium.com%2Fm%2Fcallback%2Fgoogle&response_type=id_token%20token&scope=email%20openid%20profile&nonce=bb728ebc47f4f43eb2c3450e3aaf74609c863948d2853916dd2ec7766682acbc


```

- `client_id`: identifies our application
- `redirect_uri`: the URI to which Google will redirect us once we've logged in
  successfully
- `response_type`: indicates we expect to receive an ID token in response
- `scope`: Asks for specific permissions from the user that the client wants:
  - email
  - profile
  - openid: Indicates that we're using an **additional standard**, OpenId
    Connect, to log in to Medium. OpenId is built on top of OAuth, and makes it
    a little tighter and more opinionated.

**Step 2: Authenticate with Your Password**. This part is not _strictly_ part of
the OAuth flow. We're still giving our "Authentication and consent" to the
Authorization Server (Google). We need to follow the auth server's steps to
receive our credentials.

Enter your Google password. Note that a request is made to a `Check Cookie`
endpoint
`https://accounts.google.com/CheckCookie?continue=https%3A%2F%2Faccounts.google.com`,
followed by another to a `Set SID` endpoint at **YouTube**'s domain
(`https://accounts.youtube.com/accounts/SetSID?`).

Why Youtube? By logging in to Medium via Google, **Google is signing us in to
multiple other Google sites**. This is the benefit of **Single Sign-On**: when
we sign into an authorization server once, we sign on to **all the sites owned
by the authorization provider**!

We then make a request to `https://accounts.google.com/signin/oauth/consent?` to
obtain the user's consent.

**Step 3:** We get a **callback**:

![callback](./screenshots/callback.png)

And eventually a response with our **authorization code**.

## What Happens to the Rest of the Steps?

After we receive our Authorization Code Response, the remaining steps need to
happen on the **web app's server**.

Why? In order to request the auth token from the authorization server, the web
application/API server needs to send along the **authorization code** _plus_ a
**client secret** to the `/token` endpoint. The client secret must be kept on
the **server side** _in order to keep it a secret_. We can't keep our secret
safe if we're **distributing it to our end users' devices**! In our case, the
Medium API server is taking the request to the callback, the authorization code
it receives from the authorization server, **and the secret that only it
knows**, and making a request to the `/token` endpoint on the **authorization
server** to receive the token. It then **saves the token** (either on the
**server**, as a user session, or on the resource owner in `localStorage`)
before passing the token **back to the resource owner**:

![oauth diagram](./screenshots/oauth-diagram.png)

Our authentication flow is **now complete** and we have our **access token**! We
have now confirmed that the authorization flow steps are actually happening!
