# Cookie-based Authentication

**Cookies** are small strings of data that are stored in your browser. They are
usually set by the server, and sent to the server every time the browser makes a
request against the server. This happens automatically, without the developer
having to do anything.

Cookies have _properties_, including a **value**, an associated **domain**, an
expiry date, etc. **Secure** cookies will only be sent over HTTPS, and only to
the associated domain. **HTTPOnly** cookies live in the browser, but are not
accessible from Javascript code. This is to prevent security issues like `XSS`.
This is important because our cookies can store **sensitive information**.

## Authentication with Cookies

We can use **cookies** for authentication. The authentication works in much the
same way as tokens, but rather than sending a token in the response, the server
sends a **cookie** with some information that uniquely identifies the user. From
that point on, **the cookie value will _automatically_ be sent by the browser**
upon each request to the server. The server can inspect the cookie and obtain
the authentication credentials. **Cookies** and **tokens** provide different
ways to authenticate to a server.

![cookies vs tokens](./screenshots/cookies-vs-tokens.png)
