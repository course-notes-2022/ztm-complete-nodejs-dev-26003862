# Authentication with Google and OAuth: Part 1

Let's now add login with Google and OAuth into our example application.

We've registered our application with Google, and obtained our client ID and
client secret. We've also installed passport along with the passport strategy
for google oauth 2.0. Let's `require` them in our application:

```js
const https = require('https');
const fs = require('fs');
const path = require('path');
const helmet = require('helmet');
const passport = require('passport');
const { Strategy } = require('passport-google-oauth20');
const express = require('express');
const app = express();

require('dotenv').config();

const config = {
  CLIENT_ID: process.env.CLIENT_ID,
  CLIENT_SECRET: process.env.CLIENT_SECRET,
};

function verifyCallback(accessToken, refreshToken, profile, done) {
  console.log('Google profile:', profile);
  /*
  Call `done` if accessToken & refreshToken are VALID to supply passport with the authenticated user. If error, or user is INVALID, we pass in an Error as the first argument. If not, pass `profile` as the second parameter to alert passport that the user is logged in.

  What's the point of passing the `profile` object to done? With Google authentication, we **know** that the user is authenticated because we receive `accessToken`. However, if we were doing our own **custom** validation, the user's **password** would be the first argument to `verifyCallback`. It's INSIDE HERE that we'd compare the user's (hashed) password with the hashed password in our database, and decide whether the credentials passed in are valid or not.

  We can also use `verifyCallback` to save our user and their profile information into our database. With OAuth, the heavy lifting has already been done!

  */
  done(null, profile);
}

// Set passport strategy; will determine how passport
// authenticates users
passport.use(
  new Strategy({
    callbackURL: '/auth/google/callback', // send callback URL to Google so it knows where to send auth code
    clientID: config.CLIENT_ID,
    clientSecret: config.CLIENT_SECRET,
  }),
  /*
  verify function: passport will invoke when it   authenticates a user. When authenticating a request to OUR
  API, passport parses the user credentials contained in  the request, and invokes this function with those  credentials.

  Because we're using OAuth, the credentials include the  accessToken, refreshToken(potentially), the user profile, and the `done` callback.
  */
  verifyCallback
);
const PORT = 3000;

app.use(helmet());

app.use(passport.initialize()); // Use passport middleware and initialize

function checkLoggedIn(req, res, next) {
  const isLoggedIn = true; //TODO
  if (!isLoggedIn) {
    return res.status(401).json({ err: 'You must log in!' });
  }
  next();
}

app.get('/', (req, res) => {
  res.status(200).sendFile(path.join(__dirname, 'public', 'index.html'));
});

// Define our login endpoint to let the user log in
app.get('/auth/google', (req, res) => {});

// Callback URL to which Google will send our authorization code
app.get('/auth/google/callback', (req, res) => {});

// Logout route
app.get('/auth/logout', (req, res) => {});

// Protect the `/secret` route with the `checkLoggedIn` middleware
app.get('/secret', checkLoggedIn, (req, res) => {
  return res.send('Your personal secret value is 42!');
});

https
  .createServer(
    {
      key: fs.readFileSync('key.pem'),
      cert: fs.readFileSync('cert.pem'),
    },
    app
  )
  .listen(PORT, () => {
    console.log(`server listening on port: ${PORT}`);
  });
```
