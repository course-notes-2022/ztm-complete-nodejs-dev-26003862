# Social Sign-On

Social sign-on is a widely-used authentication method that allows you to sign
users in to your application using common social media providers (Google,
Facebook, Github, etc...). This is convenient for the user, as the user doesn't
have to remember specific login credentials for every application.

From a development perspective, the application still keeps track of who has
registered to the application, but social sign-on frees the application from
having to implement login functionality, keep passwords and accounts secure,
etc. A giant company like Google or Facebook is much more likely to be able to
secure user data than a small team of developers implementing it themselves!
