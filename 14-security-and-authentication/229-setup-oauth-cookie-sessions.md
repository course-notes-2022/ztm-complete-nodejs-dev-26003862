# Setting Up OAuth Cookie Session

We're serving our demo project over HTTPS, and we can login using Google and the
OAuth 2 flow. However, _anyone can still access our secret value via the
endpoint_. We need to keep track of whether the user is logged in via a
**session**, and implement that check so that only a logged-in user can view the
secrets.

We can do this in the `checkLoggedIn` function:

```js
function checkLoggedIn(req, res, next) {
  const isLoggedIn = true; //TODO
  if (!isLoggedIn) {
    return res.status(401).json({ err: 'You must log in!' });
  }
  next();
}
```

The check will pass only if the session has been properly populated. Let's add
the ability to handle **sessions** to our OAuth example.

The `passport.initialize()` function is middleware that sets up the **passport
session**. How do we **populate the session**? We can't do it automatically
because our session can be stored in different ways: on the server side in a
database, or on the client side in a cookie. As developers, we must connect the
session as **passport** sees it with the session **as it it stored**.

We've decided for this example to use client-side sessions, where the entire
session lives in the users browser and is sent along with every request to the
server. We'll use `cookie-session` middleware to set up and parse our cookies:

> `npm install cookie-session`

Add the following below `app.use(helmet())`:

```js
app.use(
  cookieSession({
    name: 'session',
    maxAge: 24 * 60 * 60 * 1000,
    keys: [config.COOKIE_KEY_1, config.COOKIE_KEY_2], // list of secrets that cookieSession will use to sign cookie
  })
);
app.use(passport.initialize());
```

`cookieSession()` takes a configuration argument that includes the following
properties:

- `name`: the name of our cookie
- `maxAge`: expiry date of cookie in milliseconds; our user will have to log in
  again after `maxAge` ms have expired
- **`keys`**: the **list of secrets that is used to sign your cookie** and keep
  it _secure_. The server will sign cookies it sends to the **browser** with
  these keys, and verify all **incoming cookies** to ensure that they were
  signed with the correct secret key. This prevents **users from tampering with
  cookies** they send to our server.

We pass `keys` in as an **array of strings**, because we may need to **change
our secrets** if they get leaked, or because of our rotation policy. Changing
the secret value will **invalidate all existing cookies using that key**.

If we want to rotate our keys, but still allow existing keys to work for users,
we can pass **multiple values** to `keys`:

```js
{
  keys: ['new secret key', 'original key'];
}
```

All new sessions will be signed with **both keys**, and our middleware will
still accept the original key. Once we're sure that all cookies have been signed
with the new key, we can **remove the original key**. For this reason, **it's
always a good idea to include at least two keys signing our cookie session**.

**IMPORTANT**: It's ALSO a **must** that we **keep our keys secret!** We should
_never_ make our keys public, store our keys as plain text or check them into
SCM. For that reason, we'll continue to use the `.env` file to store our keys as
well as our client ID and client secret. We'll then add those to our `config`
object:

```js
const config = {
  CLIENT_ID: process.env.CLIENT_ID,
  CLIENT_SECRET: process.env.CLIENT_SECRET,
  COOKIE_KEY_1: process.env.COOKIE_KEY_1,
  COOKIE_KEY_2: process.env.COOKIE_KEY_2,
};
```

Our server now understands how the cookie for the session should look, where to
look for it, and how to parse it. Because we're using a **client side session**,
we need to make sure that our server is **signing** our cookies with our
secrets.

We've made good progress, but our session isn't actually being **populated with
our user's data** yet when the user logs in via the `/auth/google/callback`
route and is authenticated via `passport`. Let's look at that in the next
lesson.
