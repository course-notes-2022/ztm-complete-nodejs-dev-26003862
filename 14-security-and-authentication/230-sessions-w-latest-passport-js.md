# Sessions with the Latest Version of Passport.js

Heads up! The latest version of the passport.js package made an unintentional
breaking change. You can follow the discussion here:
[https://github.com/jaredhanson/passport/issues/904](https://github.com/jaredhanson/passport/issues/904)

And here:
[https://github.com/expressjs/cookie-session/issues/166 ](https://github.com/expressjs/cookie-session/issues/166)

If you're using the latest version of passport you may get an error saying
req.session.regenerate is not a function.

While they get this resolved, the quickest way to get passport to work with the
cookie-session middleware would be to install passport 0.5:

```
npm uninstall passport
npm install passport@0.5
```

Everything else remains the same. Let's keep going!
