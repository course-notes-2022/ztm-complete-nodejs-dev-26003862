# Reading and Writing the OAuth Session: Part 2

We're setting our cookie value with the Google profile data coming back from our
authentication flow. The entire profile is pretty long. We can limit the data
we're saving in our session by using the `serializeUser` and `deserializeUser`
functions.

`serializeUser` takes the Google profile and creates the cookie for the session.
We could **select** properties from the profile to set in the session, instead
of the whole profile. Let's use _just_ the user's `id`. For our example, we
don't need any specific user data, we're just concerned that they have a valid
Google account and are logged in successfully.

If we _were_ using server-side sessions, we could store just the `id` in the
cookie, and use the `deserializeUser` function when we're handling a request,
**pass in the cookie data with the `id`** as the `obj` parameter. The cookie
will be verified with our secret key by the server to ensure it is properly
signed. From there, we could look up the user in our **database** using the
`id`, and then check permissions of the user, etc:

```js
// Read the session from the cookie
passport.deserializeUser((id, done) => {
  // Look up the user in the database
  User.findById(id).then((user) => {
    // Check permissions, etc...
    done(null, user);
  });
});
```

Let's refactor our application to get the user's `id` _only_ from the Google
user profile:

```js
// Save the session to the cookie
passport.serializeUser((user, done) => {
  done(null, user.id);
});

// Read the session from the cookie
passport.deserializeUser((id, done) => {
  done(null, id);
});
```

Save changes and restart the application. Login via the Google authentication
flow, and inspect the cookies. Note that our `session` cookie contains only the
`id` value (base64 encoded, _not encrypted_), and is **much** smaller now
(approximately 70 bytes).
