# CommonJS vs ECMAScript Modules

The modules we've seen so far have been `CommonJS` modules. CommonJS is a
standard that was created for Node back in 2009. When we use `require()`, we're
using CommnJS.

The other main standard for JS modules is the `ECMAScript` standard, also called
ES6 modules for the version of ES in which they were initially released. ES
modules use the `import` statement for importing, and `export` for exporting
modules.

Starting from version 13.2, Node began supporting ES modules as well as
CommonJS. This helps us avoid context-switching just a little more. The majority
of Node code has been written with CommonJS, and that will continue. We'll use
CommonJS in this course.
