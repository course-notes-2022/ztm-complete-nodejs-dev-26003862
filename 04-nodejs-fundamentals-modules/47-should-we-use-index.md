# Should We Use `index.js`?

This is a controversial subject among Node developers! Some developers love it
for the ease of importing, others don't.

Using index.js may add some unexpected confusion. Ryan Dahl, the creator of
Node, published a video of his 10 regrets about Node, and `index.js` was one of
them as he felt it needlessly complicated the module loading system.
[See the documentation for more information on how Node loads modules.](https://nodejs.org/api/modules.html#modules_all_together)

We don't _have_ to use `index.js`. It's also not the end of the world if we
_do_. Over time, you'll learn to be careful with it because of the potential
confusion it can add among developers.
