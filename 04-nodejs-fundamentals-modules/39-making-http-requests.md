# Making HTTP Requests

What can we do with the built-in modules? A lot, as it turns out. Let's look at
an example using the `Http` module.

Create a new file, `http-example.js`, and add the following:

```js
const http = require('http');

const req = http.request('http://www.google.com', (res) => {
  // res is an object representing the response
  // from the http request
  // It is an EventEmitter
  res.on('data', (chunk) => {
    console.log(`Data chunk: ${chunk}`);
  });

  res.on('end', () => {
    console.log('No more data');
  });
});

req.end(); // Must call `end` in order to send the request
```

Run the code in the Node REPL. Note that we get multiple chunks before logging
`No more data` to the console.

We can destructure properties from the module returned by `require`:

```js
const { request } = require('https'); // Destructure!

const req = request('https://www.google.com', (res) => {
  res.on('data', (chunk) => {
    console.log(`Data chunk: ${chunk}`);
  });

  res.on('end', () => {
    console.log('No more data');
  });
});

req.end();
```

We've built a simple program using the built-in code from a node module!
