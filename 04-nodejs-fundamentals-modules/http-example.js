const { request } = require('https');

const req = request('https://www.google.com', (res) => {
  // res is an object representing the response
  // from the http request
  // It is an EventEmitter
  res.on('data', (chunk) => {
    console.log(`Data chunk: ${chunk}`);
  });

  res.on('end', () => {
    console.log('No more data');
  });
});

req.end(); // Must call `end` in order to send the request
