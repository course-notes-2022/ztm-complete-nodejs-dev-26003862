# Why Use Modules?

Modules allow us to leverage code created by others to compose our programs.
They allow us to:

1. Reuse existing code
2. Organize our code
3. Expose only what will be used
