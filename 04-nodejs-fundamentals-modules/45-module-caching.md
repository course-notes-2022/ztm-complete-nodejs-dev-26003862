# Module Caching

When we load a module (either CJS or ES modules), node **caches** the module.
This means that if our module is required in **multiple places** by multiple
modules, the dependency module will be loaded **once** in a **cache** of
required modules an re-uses the cached module when it's required again.

The cache is a `global`, and lives in the `require.cache` property. We can
inspect this cache to see what's in it by logging `require.cache` to the
console.
