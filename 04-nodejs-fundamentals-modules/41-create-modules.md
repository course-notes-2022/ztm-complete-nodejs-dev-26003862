# Creating our Own Modules

Let's write our own module! Create a new folder, `modules-example`.

In node, each file is treated as a separate module. Add the following files:

`request.js`:

```js
function encrypt(data) {
  return 'encrypted data';
}

function send(url, data) {
  const ecnryptedData = encrypt(data);
  console.log(`sending ${encryptedData} to ${url}`);
}

module.exports = { encrypt, send }; // `module` is a global that contains information about the current module
```

`response.js`

```js
function decrypt(data) {
  return 'decrypted data';
}

function read() {
  return decrypt('data');
}

module.exports = { read };
```

`https.js`:

```js
const { send } = require('./request'); // import `send` using the `require` function and the relative path to the file
// we can omit the `.js` extension because
// require looks for `.js` extensions automatically
// log require.extensions to see the extensions
// that require looks for
const { read } = require('./response');

function request(url, data) {
  send(url, data);
  return read();
}

const responseData = request('https://google.com', 'hello');

console.log(responseData);
```
