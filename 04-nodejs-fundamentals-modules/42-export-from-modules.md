# Exporting from Modules

There are multiple ways to export modules. We can set the `module.exports`
object in one step:

```js
function encrypt(data) {
  return 'encrypted data';
}

function send(url, data) {
  const ecnryptedData = encrypt(data);
  console.log(`sending ${encryptedData} to ${url}`);
}

module.exports = { encrypt, send };
```

Or we can set them individually:

```js
module.exports.encrypt = function (data) {
  return 'encrypted data';
};

module.exports.send = function (url, data) {
  const ecnryptedData = encrypt(data);
  console.log(`sending ${encryptedData} to ${url}`);
};
```

We can even omit the `module` keyword:

```js
exports.send = function(data) {...}
```

If you have a choice, stick with exporting an object as `module.exports` at the
bottom of the file. This helps make very clear what is being exported from the
module.
