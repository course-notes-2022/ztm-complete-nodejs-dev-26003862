# Creating our Own ES Modules

To refactor our code to support ES modules, we need to refactor our `require()`
function calls to `import` statements, and our `module.exports` to `export`
statements:

`http.js`:

```js
import { send } from './request';
import { read } from './response';

function request(url, data) {
  send(url, data);
  return read();
}
```

`request.js`:

```js
function encrypt(data) {
  return 'encrypted data';
}

function send(url, data) {
  const encryptedData = encrypt(data);
  console.log(`sending ${encryptedData} to ${url}`);
}

export { encrypt, send };
```

`response.js`:

```js
function decrypt(data) {
  return 'decrypted data';
}

function read() {
  return decrypt('data');
}

export { read };
```

Note that if we run this code, we get the following error:

```
import { send } from './request';
^^^^^^

SyntaxError: Cannot use import statement outside a module
```

Node treats JS files as CommonJS modules **by default**. If we want to use ES
modules, **we must rename our files with the `.mjs` extension**, and we _must_
include the file extension on the `import` statement.

Save changes and run the code in the REPL with `node https.mjs`.
