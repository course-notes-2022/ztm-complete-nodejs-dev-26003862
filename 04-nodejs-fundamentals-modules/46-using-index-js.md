# Using `index.js`

When working with Node projects, you'll come across a file called `index.js`.

Imagine we wanted to create a folder called `internals` inside our
`modules-example` folder:

```
| -/modules-example
| --/internals
| ---/request.js
| ---/response.js
| --/https.js
```

We can create a file inside the `internals` folder called `index.js`. `index` is
a special file that allows you to treat a **folder** like a **module**, so that
when you pass a path to a folder to `require()`, it resolves to the `index.js`
file. If we export all our `module.exports` from `index`, we can simply import
all the exports of the folder from a single point. You'll often see this kind of
pattern of importing from index.js.
