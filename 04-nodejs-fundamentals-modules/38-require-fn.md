# The `require` Function

Let's learn about **Node modules** in this chapter.

**Modules** are reusable pieces of code that allow us to focus on the business
logic of our application without focusing on things like the details of making
HTTP requests. We can leverage any of node's built in modules using the
`require` function:

```js
const EventEmitter = require('events');
```

We can use `require` to load up built-in modules, our own modules, or 3rd-party
modules published via package managers such as `npm`.
