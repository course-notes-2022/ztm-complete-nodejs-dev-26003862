# Creating Mongoose Schema for Launches

Let's take the first step in storing our API data in a Mongo database. We'll be
working with our database in the `models` files inside `src/models`.

## Data Modeling

Let's **model** our data, i.e. figure out what **shape** it should follow.
Recall that our mongoose schemas are associated with some collection in Mongo,
and the schema determines the structure of the **documents in that collection**.

So far, we've been using a hard-coded `launch` object to understand the shape of
what a launch should look like. Let's make a new schema next to our models file,
`launches.mongo.js`:

```js
const mongoose = require('mongoose');

const launchesSchema = mongoose.Schema({
  // view the types for Schema at: https://mongoosejs.com/docs/schematypes.html
  flightNumber: {
    type: Number,
    required: true,
    // default: 100, // More validation is possible...
    // min: 100,
    // max: 999,
  },
  launchDate: {
    type: Date,
    required: true,
  },
  mission: {
    type: String,
    required: true,
  },
  rocket: {
    type: String,
    required: true,
  },
  target: {
    // Refers to a Planet schema
    type: mongoose.Types.ObjectId,
    ref: 'Planet',
  },
});
```

### Are References Worth It?

Observe the following code:

```js
const launchesSchema = mongoose.Schema({
  // Omitted...
  target: {
    // Refers to a Planet schema
    type: mongoose.Types.ObjectId,
    ref: 'Planet',
  },
});
```

Creating a `ref` in this way mimics the concept of **foreign keys** in
relational databases. In other words, we can establish that the `target`
property refers to an `ObjectId` in the `Planet` collection. When creating a new
`launch`, mongoose would verify that the `target` attribute corresponds to a
planet in the `Planet` collection.

_Sort of..._

In reality, this concept of refs makes our development in Mongo **more
difficult**. Mongo doesn't support table joins, and we'd need to _write this
functionality ourselves_. Generally, we'd want to take the "Mongo approach", and
\*\*include all the relevant data for a document _in the document itself_:

```js
const mongoose = require('mongoose');

const launchesSchema = mongoose.Schema({
  // view the types for Schema at: https://mongoosejs.com/docs/schematypes.html
  flightNumber: {
    type: Number,
    required: true,
    // default: 100, // More validation is possible...
    // min: 100,
    // max: 999,
  },
  launchDate: {
    type: Date,
    required: true,
  },
  mission: {
    type: String,
    required: true,
  },
  rocket: {
    type: String,
    required: true,
  },
  target: {
    type: String,
    required: true,
  },
  upcoming: {
    type: Boolean,
    required: true,
  },
  success: {
    type: Boolean,
    required: true,
    default: true,
  },
  customers: {
    type: [String],
  },
});
```

We have our first complete Schema!
