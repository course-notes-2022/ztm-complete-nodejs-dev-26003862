# Finding Documents

Let's learn how to **find** planet documents. We'll update the
`planets.controller` to use Mongo's `find` method, which we'll use with
mongoose's models. Afterwards, we'll return to the `planets.model` and upsert
our planet documents.

`find` is used as follows:

```js
function getAllPlanets() {
  return planets.find(
    {
      // filter object; which object(s) to RETURN
      keplerName: 'Kepler-62 f',
    },
    {
      // projection object; which FIELDS to return
      keplerName: 1,
    }
  );
}
```

Update the `getAllPlanets` function of `planets.model` as follows:

```js
async function getAllPlanets() {
  return await planets.find({});
}
```

Update the `httpGetAllPlanets` function of `planets.controller`:

```js
const { getAllPlanets } = require('../../models/planets.model');

// Refactor into an async function
async function httpGetAllPlanets(req, res) {
  // Await the result of getAllPlanets
  return res.status(200).json(await getAllPlanets());
}

module.exports = {
  httpGetAllPlanets,
};
```
