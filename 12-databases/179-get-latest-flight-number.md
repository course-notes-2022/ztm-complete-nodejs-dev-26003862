# Getting the Latest Flight Number

One approach that simplifies things is to store our launches as normal, and
figure out the latest flight number as a **separate step**.

Create a new function `getLatestFlightNumber` in `launches.model`:

```JS
const DEFAULT_FLIGHT_NUM = 100;

// OMITTED...

async function getLatestFlightNumber() {
  // Get the latest launch
  const latestLaunch = await launches.findOne().sort('-flightNumber');
  return latestLaunch.flightNumber || DEFAULT_FLIGHT_NUM;
}
```
