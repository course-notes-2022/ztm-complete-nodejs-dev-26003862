# The `upsert` Operation

Let's return to `loadPlanetsData` in the `planets.model`. Recall that we want to
upsert to avoid duplicate data.

An **upsert** inserts data into a collection _if it doesn't already exist_ or
updates the data if it does. Refactor `loadPlanetsData` as follows:

```js
function loadPlanetsData() {
  return new Promise((resolve, reject) => {
    fs.createReadStream(
      path.resolve(__dirname, '..', '..', 'data', 'kepler_data.csv')
    )
      .pipe(
        parse({
          comment: '#',
          columns: true,
        })
      )
      .on('data', async (planet) => {
        if (isHabitablePlanet(planet)) {
          // Update document in planets collection if exists
          // , create document otherwise
          savePlanet(planet);
        }
      })
      .on('error', (err) => {
        reject(err); // Reject on error
      })
      .on('end', async () => {
        const countPlanetsFound = (await getAllPlanets()).length;
        console.log(`${countPlanetsFound} planets found`);
        resolve();
      });
  });
}

// Create `savePlanet` function
async function savePlanet(planet) {
  try {
    const updated = await planets.updateOne(
      { keplerName: planet.kepler_name },
      { keplerName: planet.kepler_name },
      { upsert: true }
    );
  } catch (err) {
    console.error(`could not save planet: ${err}`);
  }
}
```
