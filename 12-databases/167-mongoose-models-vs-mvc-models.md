# Mongoose Models vs MVC Models

We have our mongoose models, where we compiled a mongoose Schema and assigned it
to a collection. But we also have "models" from our MVC design pattern. What's
the difference?

`mongoose` Models and Schemas are classes mongoose provides us to **talk to
MongoDB** databases. "Models" from MVC are a more general concept that can apply
to any external data source. It captures the data our API is working with. Why
do we need both?

Our `launches.model.js` and `planets.model.js` files are acting as our **Data
Access Objects**. They are responsible for **interacting with our database**
while **abstracting away the implementation details**. If we ever decided to
switch databases, we could swap out the **mongoose models** for another
solution, and **our controllers wouldn't have to change**. This gives our
project more flexibility than if we used the `mongoose` models **directly**.
