# Connecting to MongoDB

How will we connect to our Mongo database?

We'll use a package called [`mongoose`](https://mongoosejs.com/). As the
documentation states:

> "Mongoose provides a straight-forware, schema-based solution to model your
> application data. It includes built-in type casting, validation, query
> building, business logic hooks and more, out of the box."

## Create the Connection

Let's begin creating the connection to our MongoDB instance.

Install `mongoose` npm package:

> `npm install mongoose`

Add the following to `src/index.js`:

```js
const http = require('http');
const mongoose = require('mongoose');
const app = require('./app');
const { loadPlanetsData } = require('./models/planets.model');
const PORT = process.env.PORT || 8000;

// Add the mongodb connection string
const CONNECTION = 'mongodb://kepler-api-server:secret@localhost:27017/nasa';

const server = http.createServer(app);

try {
  // Connect to mongodb upon starting the server
  mongoose
    .connect(CONNECTION)
    .then((connection) => {
      console.log(`Connection to mongoose OK: `);
    })
    .catch((err) => {
      console.log(`Connection to mongoose failed: ${err.message}`);
    });
} catch (err) {
  console.error(`there was an error: ${err}`);
}

await loadPlanetsData();
server.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});

startServer();
```

Save changes and restart server (if not running in `watch` mode). You should see
the following output:

```
[nodemon] restarting due to changes...
[nodemon] starting `node src/index.js`
Listening on port 8000
Connection to mongoose OK:

```
