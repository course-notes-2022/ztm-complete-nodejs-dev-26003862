# Database Schemas & Schemaless Databases

Let's learn one commonly misunderstood concept of databases: **schemas**.

A **schema** defines the structure of the data we store in our database. The
following represents a possible schema for a `Friend` entity:

| attribute   | type   |
| ----------- | ------ |
| `firstName` | string |
| `lastName`  | string |
| `age`       | number |

SQL databases require that you specify the schema of the table **ahead of
time**, before you create the database. It's not always easy to change schema
structures once created. NoSQL databases are more flexible. They're sometimes
called "schemaless". While MongoDB doesn't **require** you to specify a schema,
you can do so in your code.

With these two approaches, we trade off on predictability vs. flexibility.
