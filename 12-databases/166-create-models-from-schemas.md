# Creating Models from Schemas

Let's begin applying the Schemas we've built to **make queries to the
database**. To do so, we'll create **Models**.

A Schema is useful only when its mapped to a **collection**. We map a Schema to
a collection using a **Model**.

It's very easy to create a Model. Add the following code to `launches.mongoose`:

```js
// Connects launchesSchema with the "launches" collection
module.exports = mongoose.model(
  'Launch', // SINGULAR COLLECTION name; mongoose will lowercase and make plural, and use the transformed name to talk to the collection
  launchesSchema
);
```

Do the same in `planets.mongoose`:

```js
module.exports = mongoose.model('Planet', planetSchema);
```
