# Updating Project Architecture

Let's update our project architecture again. We've added a Mongo database and an
Atlas cluster in the cloud. We'll refactor our architecture diagram accordingly:

![project architecture 2](./screenshots/project-architecture-2.png)
