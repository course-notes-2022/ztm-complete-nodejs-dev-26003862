# Creating and Inserting Documents

Let's populate our `planets` collection in MongoDB with the planets data from
our Kepler data. We'll move incrementally in order to avoid introducing too many
breaking changes all at once.

Make the following changes to `planets.model`:

```js
const fs = require('fs');
const path = require('path');
const { parse } = require('csv-parse');
const planets = require('./planets.mongoose');

const habitablePlanets = [];

function isHabitablePlanet(planet) {
  return (
    planet['koi_disposition'] === 'CONFIRMED' &&
    planet['koi_insol'] > 0.36 &&
    planet['koi_insol'] < 1.11 &&
    planet['koi_prad'] < 1.6
  );
}

function loadPlanetsData() {
  return new Promise((resolve, reject) => {
    fs.createReadStream(
      path.resolve(__dirname, '..', '..', 'data', 'kepler_data.csv')
    )
      .pipe(
        parse({
          comment: '#',
          columns: true,
        })
      )
      .on('data', async (planet) => {
        if (isHabitablePlanet(planet)) {
          // Insert new document in planets collection
          const result = await planets.create({
            keplerName: planet.kepler_name,
          });
        }
      })
      .on('error', (err) => {
        reject(err);
      })
      .on('end', () => {
        resolve();
      });
  });
}

function getAllPlanets() {
  return habitablePlanets;
}

module.exports = {
  getAllPlanets,
  loadPlanetsData,
};
```

## A Problem

Observe that we want to create a new `planet` document in the database for each
inhabitable planet in our Kepler data. The `loadPlanetsData` function helps us
accomplish this. However, `loadPlanetsData` is currently being called **every
time the server starts/re-starts**:

```js
const http = require('http');
const { MongoClient, ServerApiVersion } = require('mongodb');
const mongoose = require('mongoose');
const app = require('./app');
const { loadPlanetsData } = require('./models/planets.model');
const PORT = process.env.PORT || 8000;
const uri = require('../../secrets');

const server = http.createServer(app);

const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  },
});

async function startServer() {
  try {
    mongoose
      .connect(uri)
      .then((connection) => {
        console.log(`Connection to mongoose OK: `);
      })
      .catch((err) => {
        console.log(`Connection to mongoose failed: ${err.message}`);
      });
  } catch (err) {
    console.error(`there was an error: ${err}`);
  }

  await loadPlanetsData(); // Load planets data on server start
  server.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
  });
}

startServer();
```

If we insert a _new_ record with `planet.create`, this will insert a _new
record_ every time the server starts up, resulting in **duplicate planet
records**. This is not what we want!

In the next lesson, we'll look at how to use `upsert` to avoid this problem.
