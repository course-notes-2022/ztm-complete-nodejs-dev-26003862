# Intro to Databases

A **database** is a collection of data, as well as a tool we use to **access**
and work with that data.

In real-life apps, our servers make data available to clients, such as data
about habitable planets. We need to **persist** that data, so that it can be
available the next time we need to access it. Currently, our planets data is
**not persisted**. It's lost every time we power down our server!

Fortunately, we have many, many options for database software. Using one of
these solutions allows us to remove our data from the server process and store
it in a dedicated database process. Our web server can then query the database
for the data it needs, and the database sends the requested data back to the
server.
