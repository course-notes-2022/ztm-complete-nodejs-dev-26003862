# `ObjectId`s

**ObjectId**s in MongoDB are the **unique identifiers** for documents.
`ObjectId` is a random-like value unique to a document. Recall that Mongo is
designed to run in a horizontally-scalable way, and a document's ID needs to be
unique across **multiple instances of the same Mongo database**. It's very
unlikely that any two documents will have the same id.

`ObjectId`s are not **totally** unique, but contain a composite value of a
pseudo-random value and the **creation date of the document**.
