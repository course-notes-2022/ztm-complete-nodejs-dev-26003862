# Saving Launches

Let's now save our new launches to the database. Let's start by adding the
functionality to add a launch to the database. Update `launches.mongo.js` as
follows:

```js
const launches = require('./launches.mongo');

let latestFlightNumber = 100;

const launch = {
  flightNumber: latestFlightNumber,
  mission: 'Kepler Exploration X',
  rocket: 'Explorer IS1',
  launchDate: new Date('December 27, 2030'),
  target: 'Kepler-442 b',
  customers: ['ZTM', 'NASA'],
  upcoming: true,
  success: true,
};

saveLaunch(launch);

function existsLaunchWithId(launchId) {
  return launches.has(+launchId);
}

function getAllLaunches() {
  return Array.from(launches.values());
}

// Create the `saveLaunch` method
async function saveLaunch(launch) {
  try {
    await launches.updateOne(
      {
        flightNumber: launch.flightNumber,
      },
      launch,
      {
        upsert: true,
      }
    );
  } catch (err) {
    console.error(`Could not save launch ${launch.flightNumber}: ${err}`);
  }
}

function abortLaunchById(launchId) {
  const aborted = launches.get(+launchId);
  aborted.upcoming = false;
  aborted.success = false;
  return aborted;
}

module.exports = {
  getAllLaunches,
  existsLaunchWithId,
  abortLaunchById,
  saveLaunch, // export `saveLaunch`
};
```
