# Auto-Increment in MongoDB

Let's look at a common SQL database feature, and see how we can implement it in
MongoDB.

We can now begin to **schedule new launches**. The general idea is that we want
to schedule a new launch, save it to the database, and **increment the
`flightNumber` property** of the launch on save. How can we automate the
incrementation?

There _are ways to do it_ in Mongo, but they're _difficult and hacky_. Mongo
simply isn't designed for it. Remember, Mongo is designed for **horizontal
scalability**. If we have multiple instances of a database, _which instance
should track the number_? Should they _all_ do it? If so, how do we keep each
instance in sync?

Luckily, the Mongo approach to solving this issue is straightforward. Let's look
at it in the next lesson.
