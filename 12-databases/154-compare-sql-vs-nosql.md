# Comparing SQL vs. NoSQL

There are 2 main categories of databases we commonly use when working with
servers: **SQL** and **NoSQL**.

We'll explore the differences by looking at a common example of each:
**PostgreSQL** and **MongoDB**.

## Postgres vs. MongoDB

|                 | MongoDB              | Postgres           |
| --------------- | -------------------- | ------------------ |
| type            | document             | relational         |
| orgainized into | collections          | tables             |
| query language  | nosql                | sql                |
| scaling         | primarily horizontal | primarily vertical |

- **Documents** are JSON-like records that store our data in a NoSQL database.
- Data in a relational database organizes data into **tables** of columns and
  rows.
- NoSQL documents include the data for a record **all in the record**.
  Relational databases **split data into separate tables** as much as possible.
- Horizontal scaling: adding multiple database servers and divide the queries
  among the databases to share the workload.
- Vertical scaling: Increasing the capacity of a single server to handle
  additional workload.

## NoSQL vs. SQL

SQL is the query language that all SQL databases share. It is used to query
information from/to the database.

NoSQL databases do **not** use SQL. They often have their own specialized ways
of querying the database, customized specifically for the database. In MongoDB's
case, this is optimized for working with documents.
