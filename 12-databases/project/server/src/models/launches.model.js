const launches = require('./launches.mongo');
const planets = require('./planets.mongo');

const DEFAULT_FLIGHT_NUM = 100;
const launch = {
  flightNumber: DEFAULT_FLIGHT_NUM,
  mission: 'Kepler Exploration X',
  rocket: 'Explorer IS1',
  launchDate: new Date('December 27, 2030'),
  target: 'Kepler-442 b',
  customers: ['ZTM', 'NASA'],
  upcoming: true,
  success: true,
};

saveLaunch(launch);

async function getLatestFlightNumber() {
  // Get the latest launch
  const latestLaunch = await launches.findOne().sort('-flightNumber');
  return latestLaunch.flightNumber || DEFAULT_FLIGHT_NUM;
}

function existsLaunchWithId(launchId) {
  return launches.has(+launchId);
}

async function getAllLaunches() {
  try {
    return await launches.find({}, { __v: 0, _id: 0 });
  } catch (err) {
    console.error(`Failed to fetch launches: ${err}`);
  }
}

async function saveLaunch(launch) {
  try {
    const planet = await planets.findOne({ keplerName: launch.target });
    if (!planet) {
      throw new Error(`planet ${launch.target} not found`);
    }
    await launches.updateOne(
      {
        flightNumber: launch.flightNumber,
      },
      launch,
      {
        upsert: true,
      }
    );
  } catch (err) {
    console.error(`Could not save launch ${launch.flightNumber}: ${err}`);
  }
}

function abortLaunchById(launchId) {
  const aborted = launches.get(+launchId);
  aborted.upcoming = false;
  aborted.success = false;
  return aborted;
}

module.exports = {
  getAllLaunches,
  existsLaunchWithId,
  abortLaunchById,
  saveLaunch,
};
