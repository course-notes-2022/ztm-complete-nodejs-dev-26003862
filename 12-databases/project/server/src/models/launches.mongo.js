const mongoose = require('mongoose');

const launchSchema = mongoose.Schema({
  // view the types for Schema at: https://mongoosejs.com/docs/schematypes.html
  flightNumber: {
    type: Number,
    required: true,
    // default: 100, // More validation is possible...
    // min: 100,
    // max: 999,
  },
  launchDate: {
    type: Date,
    required: true,
  },
  mission: {
    type: String,
    required: true,
  },
  rocket: {
    type: String,
    required: true,
  },
  target: {
    type: String,
    required: true,
  },
  upcoming: {
    type: Boolean,
    required: true,
  },
  success: {
    type: Boolean,
    required: true,
    default: true,
  },
  customers: {
    type: [String],
  },
});

// Connects launchesSchema with the "launches" collection
module.exports = mongoose.model(
  'Launch', // SINGULAR COLLECTION name; mongoose will lowercase and make plural, and use the transformed name to talk to the collection
  launchSchema
);
