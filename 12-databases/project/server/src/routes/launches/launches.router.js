const express = require('express');
const {
  httpGetAllLaunches,
  httpAddNewLaunch,
  httpAbortLaunch,
} = require('./launches.controller');

const launchesRouter = express.Router();

// Cleaner syntax: route handlers can now respond to simply `/` route
launchesRouter.get('/', httpGetAllLaunches);

launchesRouter.post('/', httpAddNewLaunch);

launchesRouter.delete('/:id', httpAbortLaunch);

module.exports = launchesRouter;
