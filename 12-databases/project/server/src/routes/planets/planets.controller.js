const { getAllPlanets } = require('../../models/planets.model');

async function httpGetAllPlanets(req, res) {
  return res.status(200).json(await getAllPlanets()); // Explicit return indicates that this is where we intend our function to stop execution
}

module.exports = {
  httpGetAllPlanets,
};
