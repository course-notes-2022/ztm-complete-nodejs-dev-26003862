const http = require('http');
const { MongoClient, ServerApiVersion } = require('mongodb');
const mongoose = require('mongoose');
const app = require('./app');
const { loadPlanetsData } = require('./models/planets.model');
const PORT = process.env.PORT || 8000;
const uri = require('../../secrets');

const server = http.createServer(app);

const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  },
});

async function startServer() {
  // try {
  //   console.log(uri);
  //   await client.connect();
  //   await client.db('admin').command({ ping: 1 });
  //   console.log(
  //     'Pinged your deployment. You successfully connected to MongoDB'
  //   );
  // } catch (err) {
  //   console.error('there was an error:', err);
  // } finally {
  //   await client.close();
  // }
  try {
    // Connect to mongodb upon starting the server
    mongoose
      .connect(uri)
      .then((connection) => {
        console.log(`Connection to mongoose OK: `);
      })
      .catch((err) => {
        console.log(`Connection to mongoose failed: ${err.message}`);
      });
  } catch (err) {
    console.error(`there was an error: ${err}`);
  }

  await loadPlanetsData();
  server.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
  });
}

startServer();
