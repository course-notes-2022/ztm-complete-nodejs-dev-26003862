# Listing All Launches

Let's refactor our `getAllLaunches` method to leverage our **database**. Update
`launches.model` as follows:

```js
async function getAllLaunches() {
  try {
    return await launches.find({}, { __v: 0, _id: 0 });
  } catch (err) {
    console.error(`Failed to fetch launches: ${err}`);
  }
}
```

Refactor `launches.controller` to use the refactored `getAllLaunches` method:

```js
const {
  getAllLaunches,
  addNewLaunch,
  existsLaunchWithId,
  abortLaunchById,
} = require('../../models/launches.model');

async function httpGetAllLaunches(req, res) {
  // AWAIT getAllLaunches
  return res.status(200).json(await getAllLaunches());
}

async function httpAddNewLaunch(req, res) {
  const launch = req.body;

  if (
    !launch.mission ||
    !launch.rocket ||
    !launch.launchDate ||
    !launch.target
  ) {
    return res.status(400).json({ error: 'Missing required launch property' });
  }
  launch.launchDate = new Date(launch.launchDate); //
  if (launch.launchDate.toString() === 'Invalid Date') {
    return res.staus(400).json({ error: 'Invalid launch date' });
  }

  addNewLaunch(launch);
  return (
    res
      .status(201)
      // AWAIT getAllLaunches
      .json(await getAllLaunches().find((l) => l.mission === launch.mission))
  );
}
// OMITTED...
```

Send a new Postman request to `localhost:8000/launches`. You should get a
response similar to the following:

```json
[
  {
    "flightNumber": 100,
    "customers": ["ZTM", "NASA"],
    "launchDate": "2030-12-27T05:00:00.000Z",
    "mission": "Kepler Exploration X",
    "rocket": "Explorer IS1",
    "success": true,
    "target": "Kepler-442 b",
    "upcoming": true
  }
]
```
