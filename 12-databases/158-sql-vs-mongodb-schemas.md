# SQL vs. MongoDB: Schemas, References, and ACID Transactions

To finally decide on a database, we need to take a closer look at the **data**
we're storing.

**MongoDB** is best for unstructured data, or where the structure will change
often.

**SQL** is best when the **structure of data** is known and well-defined, for
example with banking/credit-card information.

Looking at our application data, our `launches` model is fairly basic, and can
be stored in either a SQL or NoSQL database. Our `launch` relates to a `planet`,
which lends itself well to SQL, however the `planet` model is very simple: a
**single string**. We don't gain much by creating a separate table and a
`launch/planet` relationship. If we _did_ have more complex planet data, SQL
might be a better choice.

In actuality, our planets are just as easily stored as strings on the `launch`
object.

## ACID

One advantage of SQL is **reliability** when performing database transactions.
SQL guarantees our transactions are `ACID`:

- **Atomicity**: A transaction is indivisible and irreducible such that _all_
  data updates occur, or _none_ occur.
- **Consistency**:
- **Isolation**:
- **Durability**: A transaction is guaranateed to survive permanently.

For our app, most of our data operations are _individual_; we don't do many
combinations of reads and writes for a single request.

Transactions as guaranteed by SQL is not especially important for our
application.

## Schemas

SQL could guarantee for us that a target planet we specify when creating a
`launch` is actually among the Kepler exoplanets by **referencing** those
planets in another `habitable_planets` table using a **foreign key**. This would
be nice to have, but makes it more difficult if our planets data changes. We'd
need to refactor our `habitable_planets` table every time it does, as well as
any records n the `launches` table that reference newly non-habitable planets.
