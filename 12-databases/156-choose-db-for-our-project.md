# Choosing a Database for our NASA Project

When choosing a database, we should consider the reasons **why** we want to add
a database:

1. To **persist our data between restarts**
2. API needs to be **stateless** for cluster mode

Practically, this means that our controller files in `/src/routes` should rely
**only on the data in the `request` object** being passed from the client. Any
other data needs to come from **external sources and services** that are
**independent** of our `node` process running our API server.

## What Data are we Storing?

We can see from inspecting our **models** that our database will need to
support:

- A collection of `launches`, where each `launch` contains several properties:

```js
{
  flightNumber: latestFlightNumber,
  mission: 'Kepler Exploration X',
  rocket: 'Explorer IS1',
  launchDate: new Date('December 27, 2030'),
  target: 'Kepler-442 b',
  customers: ['ZTM', 'NASA'],
  upcoming: true,
  success: true,
}
```

- A list of `planets`, each of which contains a `planetName`. The `planetName`
  is related to the `target` attribute of a `launch`.

- The `flightNumber`, which our database will need to track as we create new
  launches.

- Each planet's `flightNumber` needs to be **unique**
