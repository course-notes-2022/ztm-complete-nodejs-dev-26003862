# Referential Integrity

We now have the ability to list all the saved launches. But there's something
strange going on. What happens if we create a launch that is _not targeting a
saved planet_?

Currently, nothing in our application will prevent this from happening. We're
able to create a `launch` document that references a `planet` which **doesn't
exist**!

This wouldn't be a problem in SQL databases because of **referential
integrity**. Using foreign keys, a SQL database would guarantee that the value
in the **referenced table** _actually exists_ before we attempt to insert a row
into our launches. We want to make sure that the target planet **exists in our
DB** before we save a new `launch`!

If we want to validate that the target planet exists before we save a launch, we
have a few approaches we can use in MongoDb. The simplest is to add validation
to check if the target planet exists in the database _before saving the launch_.

Refactor `launches.model`'s `saveLaunch` function to check for the target planet
before saving:

```js
async function saveLaunch(launch) {
  try {
    const planet = await planets.findOne({ keplerName: launch.target });
    if (!planet) {
      throw new Error(`planet ${launch.target} not found`);
    }
    await launches.updateOne(
      {
        flightNumber: launch.flightNumber,
      },
      launch,
      {
        upsert: true,
      }
    );
  } catch (err) {
    console.error(`Could not save launch ${launch.flightNumber}: ${err}`);
  }
```

[Node Best Practices](https://github.com/goldbergyoni/nodebestpractices)
