# Mongoose

Let's learn what `mongoose` provides for us, and how it works.

`mongoose` gives us some advanced features to make our development easier. Our
Mongo databases are made up of **collections**, which contain **documents**.
Each document stores data in a JSON-like format that Mongo calls `BSON`.

Mongo doesn't enforce **schemas**. It's up to us to enforce schemas on the data
at the **application** level. This is what `mongoose` gives us. The `Schema`
lets us give a structure to our data that our application must respect when
accessing the data. `mongoose` also provides advanced validators that allow us
to validate a phone number, for example.

`mongoose` also gives us **models**. We create a `Model` that uses a `Schema`.
It's a bit like taking a `Schema` and applying it to a collection in MongoDB. We
can **query a model** to get the data **stored in a collection**. Models give us
**Javascript objects** that we can work directly with in our application!

![mongoose schemas and models](./screenshots/mongoose-schema-model.png)
