# Reminder: Destination Planets Dropdown

Friendly reminder! If you load the front end and you see an empty list of
planets in the Destination dropdown on the launch page.

You will need to npm run deploy again to update the public folder in the server
directory.

Remember, we've updated the dropdown in the Launch.js file in our client folder
to use planet.keplerName instead of planet.kepler_name. If we don't run the npm
run deploy script before running our npm run server script, our server will be
referencing an old static file and we'll get blank labels for the planet names.
