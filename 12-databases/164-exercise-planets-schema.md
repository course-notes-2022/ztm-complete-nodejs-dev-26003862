# Exercise: Create Mongoose Schema for Planets

Create a new file for the `planet` schema. This is your chance to try on your
own. Use the `launchesSchema` as a guide.

```js
const mongoose = require('mongoose');

const planetSchema = new mongoose.Schema({
  keplerName: {
    type: String,
    required: true,
  },
});

module.exports = planetSchema;
```
