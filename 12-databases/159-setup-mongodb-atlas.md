# Setting up MongoDB Atlas

We're going to work with MongoDB, so let's begin setting up our development
database server. We have several options:

- Running Mongo locally
- Running a cloud database instance
- Running a MongoDB Atlas (cloud) instance
