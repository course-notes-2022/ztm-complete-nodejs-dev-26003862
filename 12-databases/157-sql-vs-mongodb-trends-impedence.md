# SQL vs MongoDB: Trends and Object-Relational Impedance Mismatch

Let's begin to evaluate which of SQL or MongoDB will be best for our
application.

The best way to choose a DB solution is to pick the right tool for the
application flow your users will be following. We can take a look at our routes
in the NASA app to discover **how** our data will be used.

Our server is written in Javascript, and we're returning **JSON** from all our
routes. MongoDB stores our collections and documents as `BSON`, or "binary
JSON". It was created so our Mongo objects can be parsed more quickly. It
_looks_ just like JSON, which means **we don't have to parse data back and
forth** between our application and our database! This is a _huge_ plus for us.
In fact, we can query our Mongo database via its shell **using Javascript**.

## Object Relational Impedence Mismatch

This term refers to the requirement to convert from the way our data is stored
in the **database** to the format in which it's used **in our code**. With SQL
solutions, we usually have this impedence when building an API server that
serves `JSON` data.
