# Excluding Fields from the Response

`mongoose` adds an `__v` value to each document. This is called the **version
key**, and helps you keep track of the **version of the document you create**.

If you change the Schema of your collection, but want to keep documents that
follow the **old** schema, you can increment versions of the new schema when
adding new-schema conforming documents.

`ObjectId`s and version keys are typically **internal values** that we don't
want to return to the client. We can **exclude fields** using the **projection
argument** to the mongo/mongoose query methods. Update the `getAllPlanets`
function in `planets.model.js`:

```js
async function getAllPlanets() {
  return await planets.find(
    {},
    { __v: 0, _id: 0 } // exclude the desired properties
  );
```
