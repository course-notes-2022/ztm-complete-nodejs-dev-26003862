const { parse } = require('csv-parse');
const fs = require('fs');

const results = [];

function isHabitablePlanet(planet) {
  return (
    planet['koi_disposition'] === 'CONFIRMED' &&
    planet['koi_insol'] > 0.36 &&
    planet['koi_insol'] < 1.11 &&
    planet['koi_prad'] < 1.6
  );
}

fs.createReadStream('kepler_data.csv')
  // `pipe` connects a Readable stream SOURCE
  // to a Writable stream DESTINATION
  .pipe(
    parse({
      comment: '#',
      columns: true, // return data as JS objects where attr = column name
    })
  )
  .on('data', (planet) => {
    if (isHabitablePlanet(planet)) {
      results.push(planet);
    }
  })
  .on('error', (err) => {
    console.log(err);
  })
  .on('end', () => {
    console.log(results.map((p) => p['kepler_name']));
    console.log(`The number of habitable planets is: ${results.length}`);
  });
