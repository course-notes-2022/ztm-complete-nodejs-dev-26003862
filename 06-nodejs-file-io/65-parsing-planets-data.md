# Parsing our Planets Data

We now have our Kepler data being read in as a stream. What we really want is to
**parse the data** as rows of CSV values. That's the functionality that
`csv-parse` gives us.

Add the following to `index.js`:

```js
const { parse } = require('csv-parse');
const fs = require('fs');

const results = [];

fs.createReadStream('kepler_data.csv')
  // `pipe` connects a Readable stream SOURCE
  // to a Writable stream DESTINATION
  .pipe(
    parse({
      comment: '#',
      columns: true, // return data as JS objects where attr = column name
    })
  )
  .on('data', (data) => {
    results.push(data);
  })
  .on('error', (err) => {
    console.log(err);
  })
  .on('end', () => {
    console.log(results);
  });
```

Note that we are using the `pipe` function to take the data from our
**Readable** stream (created by `fs.createReadStream` using the planets data
`.csv`), into a **Writable** stream by passing the results of the readable
stream to `parse`.

![pipe](./screenshots/pipe.png)

Observe that the output is an array of objects with the following shape:

```js
[
  {
    kepler_name: '',
    koi_disposition: 'FALSE POSITIVE',
    koi_pdisposition: 'FALSE POSITIVE',
    koi_score: '0.0000',
    koi_fpflag_nt: '0',
    koi_fpflag_ss: '1',
    koi_fpflag_co: '1',
    koi_fpflag_ec: '0',
    koi_period: '10.318723010',
    koi_period_err1: '2.8460000e-05',
    koi_period_err2: '-2.8460000e-05',
    koi_time0bk: '173.3154300',
    koi_time0bk_err1: '2.240000e-03',
    koi_time0bk_err2: '-2.240000e-03',
    koi_impact: '1.2150',
    koi_impact_err1: '59.5200',
    koi_impact_err2: '-0.4630',
    koi_duration: '3.83600',
    koi_duration_err1: '0.12100',
    koi_duration_err2: '-0.12100',
    koi_depth: '8.7170e+02',
    koi_depth_err1: '2.640e+01',
    koi_depth_err2: '-2.640e+01',
    koi_prad: '28.59',
    koi_prad_err1: '8.610e+00',
    koi_prad_err2: '-3.690e+00',
    koi_teq: '876.0',
    koi_teq_err1: '',
    koi_teq_err2: '',
    koi_insol: '139.26',
    koi_insol_err1: '125.69',
    koi_insol_err2: '-46.77',
    koi_model_snr: '38.20',
    koi_tce_plnt_num: '1',
    koi_tce_delivname: 'q1_q17_dr25_tce',
    koi_steff: '5927.00',
    koi_steff_err1: '176.00',
    koi_steff_err2: '-193.00',
    koi_slogg: '4.409',
    koi_slogg_err1: '0.084',
    koi_slogg_err2: '-0.196',
    koi_srad: '1.0600',
    koi_srad_err1: '0.3190',
    koi_srad_err2: '-0.1370',
    ra: '291.848910',
    dec: '37.713501',
    koi_kepmag: '15.065',
  },
];
```
