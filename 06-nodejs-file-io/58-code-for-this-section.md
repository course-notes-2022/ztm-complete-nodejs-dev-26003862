# Code for This Section

You can find the "final" code for this section at this repository below. Keep in
mind that we recommend you code along with us and use this only if you ever get
stuck or you don't like to code along.

> [https://github.com/odziem/planets-project](https://github.com/odziem/planets-project)

You can find the completed code for an individual video by browsing the commits
in the repository. Each commit corresponds to the code we write in one video.
