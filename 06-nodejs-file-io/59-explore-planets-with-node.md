# Exploring Planets with NodeJS

Let's start to put together the theory we've learned into a practical example!

The project we'll work on in this section will leverage data gathered by the
**Kepler Space Telescope** to search for **habitable planets**.
