# Importing Kepler Space Telescope Data

We'll get the data form Nasa's
[Exoplanet Archive](https://exoplanetarchive.ipac.caltech.edu/docs/data.html).
We'll download the table of data gathered by the Kepler Telescope as a `.csv`
file and read it with Node.

Open a new project folder in VS code and place the `.csv` file inside it. A CSV
separates all the data for a single record with commas.
