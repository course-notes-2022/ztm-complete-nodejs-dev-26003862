# Streaming Large Data Files

What is "streaming"? How do we deal with large data files in Node?

`csv` is a parser converting txt input into arrays or objects. It implements the
NodeJS `Stream` API. It also provides alternative APIs for convenience such as
the callback API and sync API.

Why add all the additional APIs? Each has its own advantages and disadvantages.

The [Stream API](https://csv.js.org/parse/api/stream/) is the recommended
approach if you need a maximum of power. It ensures scalability by treating your
data as a stream from source to destination.

What does this mean? This allows Node's event loop to read in the data **as it
comes in** rather than waiting for all of it.
