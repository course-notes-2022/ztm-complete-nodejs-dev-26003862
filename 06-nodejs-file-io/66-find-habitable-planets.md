# Finding Habitable Planets

Let's find some habitable planets that might support life! We're looking for
planets similar to Earth that orbit stars similar to the Sun.

We're going to want to filter out the unsuitable planets from our results.

Refactor `index.js` to check each planet for the most important characteristics
of a habitable planet:

```js
const { parse } = require('csv-parse');
const fs = require('fs');

const results = [];

function isHabitablePlanet(planet) {
  return (
    planet['koi_disposition'] === 'CONFIRMED' &&
    planet['koi_insol'] > 0.36 &&
    planet['koi_insol'] < 1.11 &&
    planet['koi_prad'] < 1.6
  );
}

fs.createReadStream('kepler_data.csv')
  // `pipe` connects a Readable stream SOURCE
  // to a Writable stream DESTINATION
  .pipe(
    parse({
      comment: '#',
      columns: true, // return data as JS objects where attr = column name
    })
  )
  .on('data', (planet) => {
    if (isHabitablePlanet(planet)) {
      results.push(planet);
    }
  })
  .on('error', (err) => {
    console.log(err);
  })
  .on('end', () => {
    console.log(`The number of habitable planets is: ${results.length}`);
  });
```

Observe that we find 8 habitable planets:

```
The number of habitable planets is: 8
```
