# Setting Up CSV Parser

We _could_ read our CSV file with the built-in `fs` module in Node. However,
Node doesn't parse CSVs natively. We can use a **3rd-party** package to help us.
The most stable and most popular is
[`csv-parse`](https://www.npmjs.com/package/csv-parse).

Let's install `csv-parse`. Initialize a new Node project. The `-y` option is
fine. Create an `index.js` file in the project root, and install `csv-parse`:

> `npm install csv-parse`

`require` the `csv-parse` dependency in `index.js`:

```js
const parse = require('csv-parse');
```
