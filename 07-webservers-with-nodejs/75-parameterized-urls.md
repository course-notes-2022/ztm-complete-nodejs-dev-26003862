# Parameterized URLs

The code we've written so far is _functional_, but not very realistic. In real
life, we'll usually be sending data that comes from a database. We need to be
able to query for individual items in our collections.

Refactor `index.js` as follows:

```js
const http = require('http');

const server = http.createServer();

const friends = [
  { id: 0, name: 'Nikola Tesla' },
  { id: 1, name: 'Albert Einstein' },
  { id: 2, name: 'Isaac Newton' },
];

// Recall that the server created by `createServer`
// is an event emitter
server.on('request', (req, res) => {
  const items = req.url.split('/');

  if (items[1] === 'friends') {
    res.writeHead(200, {
      'Content-Type': 'application/json',
    });
    if (items.length === 3) {
      const friendIndex = +items[2];
      res.end(JSON.stringify(friends[friendIndex]));
    } else {
      res.end(JSON.stringify(friends));
    }
  } else if (items[1] === 'messages') {
    res.setHeader('Content-Type', 'text/html');
    res.write('<html>');
    res.write('<body>');
    res.write('<ul>');
    res.write('<li>Hello Isaac</li>');
    res.write('</ul>');
    res.write('</body>');
    res.write('</html>');
    res.end();
  } else {
    res.statusCode(404);
    res.end();
  }
});

// Instruct the server to listen for requests
server.listen(3001, () => {
  console.log('listening on port 3001');
}); // 127.0.0.1 => localhost
```

Save changes and run. Visit `/friends/0` and verify that Nikola Tesla is
returned. This is an example of using **request parameters** to return
**specific data** from a collection.

Note that this code is acceptable for a quick proof-of-concept like we're doing
here, but we would **not** want to do something like this for production. There
are **existing packages** that can help us with these common problems, and we'll
investigate some soon!
