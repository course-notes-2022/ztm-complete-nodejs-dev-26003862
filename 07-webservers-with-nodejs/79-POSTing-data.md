# POSTing Data to the Server

How do we handle requests that submit **new data** to the server?

To submit new data, we use either of two methods: `POST` or `PUT`. `POST` is the
most common. Let's look at how we might use a `POST` request to add a new friend
record.
