# Web Servers Recap

We've learned how to create our first HTTP server that handles incoming requests
and responds to them using strings. We've learned how to parse different types
of requests, set headers, and return JSON and HTML. However, our code is
bare-bones and does not take advantage of the good work that others have done to
make these tasks much easier.

If we want to focus on our business logic and _not_ re-invent the wheel, we
should leverage frameworks that can save us from making the mistakes we made in
this video, and allow us to create **more powerful applications**. Still, for
learning purposes it's good to work without a framework **first**; we gain a
**deeper understanding** of how things work and _why_.
