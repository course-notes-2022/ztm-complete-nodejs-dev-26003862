# HTTP APIs and Routing

Our current server always responds with the same data to **every route**.
Normally we want our server to be able to respond to **different types of
requests** at **different URLs**, or **endpoints**. How do we make different
endpoints for our HTTP server? We inspect the **request** coming in and parse
the **URL** of the request:

```js
const http = require('http');

const server = http.createServer();

// Recall that the server created by `createServer`
// is an event emitter
server.on('request', (req, res) => {
  if (req.url === '/friends') {
    res.writeHead(200, {
      'Content-Type': 'application/json',
    });
    res.end(JSON.stringify({ data: 'Hello! Sir Isaac Newton is your friend' }));
  } else if (req.url === '/messages') {
    res.setHeader('Content-Type', 'text/html');
    res.write('<html>');
    res.write('<body>');
    res.write('<ul>');
    res.write('<li>Hello Isaac</li>');
    res.write('</ul>');
    res.write('</body>');
    res.write('</html>');
    res.end();
  }
});

// Instruct the server to listen for requests
server.listen(3001, () => {
  console.log('listening on port 3001');
}); // 127.0.0.1 => localhost
```

Save changes. Attempt to navigate to the `/friends` and `/messages` routes, and
verify that the response is as expected.

Finally, let's add a `404` route to handle requests that come in for undefined
endpoints:

```js
const http = require('http');

const server = http.createServer();

// Recall that the server created by `createServer`
// is an event emitter
server.on('request', (req, res) => {
  if (req.url === '/friends') {
    res.writeHead(200, {
      'Content-Type': 'application/json',
    });
    res.end(JSON.stringify({ data: 'Hello! Sir Isaac Newton is your friend' }));
  } else if (req.url === '/messages') {
    res.setHeader('Content-Type', 'text/html');
    res.write('<html>');
    res.write('<body>');
    res.write('<ul>');
    res.write('<li>Hello Isaac</li>');
    res.write('</ul>');
    res.write('</body>');
    res.write('</html>');
    res.end();
  } else {
    res.statusCode(404);
    res.end();
  }
});

// Instruct the server to listen for requests
server.listen(3001, () => {
  console.log('listening on port 3001');
}); // 127.0.0.1 => localhost
```
