# HTTP Responses

How does a server **respond** to the requests that it receives?

An HTTP response has 3 components:

- Headers: metadata about the response being sent (such as the `Content-Type`
  header)

- Body: contains the data being fetched from the server

- Status Code: describes the status (success/failure) of the request. Status
  codes are grouped into 5 categories:
  - 100 - 199: Informational responses
  - 200 - 299: Successful responses
  - 300 - 399: Redirection responses
  - 400 - 499: Client errors
  - 500 - 599: Server errors
