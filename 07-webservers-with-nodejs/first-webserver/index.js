const http = require('http');

const server = http.createServer();

const friends = [
  { id: 0, name: 'Nikola Tesla' },
  { id: 1, name: 'Albert Einstein' },
  { id: 2, name: 'Isaac Newton' },
];

// Recall that the server created by `createServer`
// is an event emitter
server.on('request', (req, res) => {
  const items = req.url.split('/');
  // Handle a POST request
  if (req.method === 'POST' && items[1] === 'friends') {
    req.on('data', (data) => {
      const friend = data.toString(); // Convert buffer to string
      console.log('Request: ', friend);
      friends.push(JSON.parse(friend));
    });
    req.pipe(res);
  }

  if (req.method === 'GET' && items[1] === 'friends') {
    res.writeHead(200, {
      'Content-Type': 'application/json',
    });
    if (items.length === 3) {
      const friendIndex = +items[2];
      res.end(JSON.stringify(friends[friendIndex]));
    } else {
      res.end(JSON.stringify(friends));
    }
  } else if (req.method === 'GET' && items[1] === 'messages') {
    res.setHeader('Content-Type', 'text/html');
    res.write('<html>');
    res.write('<body>');
    res.write('<ul>');
    res.write('<li>Hello Isaac</li>');
    res.write('</ul>');
    res.write('</body>');
    res.write('</html>');
    res.end();
  } else {
    res.statusCode = 404;
    res.end();
  }
});

// Instruct the server to listen for requests
server.listen(3001, () => {
  console.log('listening on port 3001');
}); // 127.0.0.1 => localhost
