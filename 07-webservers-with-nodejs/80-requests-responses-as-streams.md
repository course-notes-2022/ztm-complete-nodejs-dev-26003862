# Requests and Responses as Streams

How would we create an endpoint that echoes back the data the user sent to the
server?

Since the request is a **readable stream**, and the response is a **writable
stream**, we can simply `pipe` the request data directly to the response data:

```js
const http = require('http');

const server = http.createServer();

const friends = [
  { id: 0, name: 'Nikola Tesla' },
  { id: 1, name: 'Albert Einstein' },
  { id: 2, name: 'Isaac Newton' },
];

server.on('request', (req, res) => {
  const items = req.url.split('/');

  if (req.method === 'POST' && items[1] === 'friends') {
    req.on('data', (data) => {
      const friend = data.toString();
      console.log('Request: ', friend);
      friends.push(JSON.parse(friend));
    });
    req.pipe(res);
  }

  // omitted...
});

server.listen(3001, () => {
  console.log('listening on port 3001');
});
```
