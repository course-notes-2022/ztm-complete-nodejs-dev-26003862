# HTTP Request

What kinds of messages do we send back and forth in HTTP?

Recall that an API is an **Application Programming Interface**. It specifies how
two applications can talk to one another, such as a browser and a web server.
Generally, the browser acts as the **client**, making requests of the server,
and the server sends responses.

For example, we could send the following requests:

- GET /friends: asks for a **collection** of friends
- GET /friends/5: asks for a friend identified by id `5`
- POST /messages: create a new message in a collection of messages
- PUT /messages/15: update the message record identified by id 15

All requests have **4 main components**:

- The request **method** (e.g. GET, PUT, POST)
- The **path** to the resource on the server (`/messages`)
- The request **body** (most commonly `json`)
- The **headers**: optional properties you can specify on the request to send
  metadata about the request to the server (such as authentication credentials).
  The `Host` header is **mandatory** in order to verify that the message is
  being sent to the correct server
