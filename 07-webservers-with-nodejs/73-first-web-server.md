# First Webserver

Let's build our first **web server** with Node. Create a new project folder with
a new `index.

```js
const http = require('http');

const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('Hello! Sir Isaac Newton is your friend'); // Must call on end of each request to server
});

// Instruct the server to listen for requests
server.listen(3001, () => {
  console.log('listening on port 3001');
}); // 127.0.0.1 => localhost
```

Note that **both the `req` and `res` objects are `Streams`**, as we saw in the
planets module.

We can return different types of content by changing the Content-Type header:

```js
const http = require('http');

const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'application/json' });
  res.end(JSON.stringify({ data: 'Hello! Sir Isaac Newton is your friend' })); // Must call on end of each request to server
});

// Instruct the server to listen for requests
server.listen(3001, () => {
  console.log('listening on port 3001');
}); // 127.0.0.1 => localhost
```

We have built a functional web server with just the built-in Node APIs.
