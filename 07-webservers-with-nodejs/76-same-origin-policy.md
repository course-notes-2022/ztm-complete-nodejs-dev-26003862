# Same-Origin Policy

Let's understand an important concept in web development: the **Same Origin
Policy**.

## What is an Origin?

When we browse the web, we type in something like `https://www.google.com/maps`.
This URL **origin** is made up of 3 critical parts:

1. The **protocol** (`https`)
2. The **host**: `www.google.com` describes the server handling our request
3. The **port**: The browser assumes port `443` when browsing on https

Whenever **any** of these three parts changes, we are no longer on the **same
origin**.

## Why Does This Matter?

The browser enforces a "Same-Origin Policy" that restricts what the browser and
Javascript are allowed to load when browsing pages on the internet. The browser
will **allow all requests** from the site you're currently browsing to endpoints
with the **same** origin, but **deny all requests from SCRIPTS to endpoints with
a _different_ origin**.

_Writes_ are often allowed across origins, though. The receiving server can
choose to accept or reject the request.

The main idea behind Same-Origin is to ensure that web browsing is **secure**,
while also allowing them to browse the internet for the information they need.
