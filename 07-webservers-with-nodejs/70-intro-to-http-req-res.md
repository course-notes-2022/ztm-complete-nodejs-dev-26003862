## Intro to HTTP Responses and Requests

We've seen that we can make **requests** to a web server for assets. The thing
that defines **how the server will respond** is its **API**. HTTP is the
protocol that allows the browser and server to communicate with one another. A
communication protocol is like a common language that the two machines use.

HTTP defines certain "verbs":

- GET: requesting data **from** a server
- POST: sending data **to** a server (creating a NEW record)
- PUT: **updating** a record
- DELETE: **deleting** a record
- PATCH:
- OPTIONS: used to describe the communication options for the target resource
