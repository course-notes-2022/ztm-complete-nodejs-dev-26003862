# CORS

CORS stands for **Cross-Origin Resource Sharing**. It's a way of relaxing the
same-origin policy for developers of web applications.

By setting the `Access-Control-Allow-Origin` header on the **server**, we can
_specify the origins with which our server can share specific resources_ beyond
the same origin. This header allows us as developers to relax the rules of CORS
when we know that requests are safe, or we want to purposefully make a resource
available to requests from other origins. This header is **always** set on the
**response**, and is controlled by the server that owns the data.

What values can the CORS header have?

We could specify a **specific origin**:

- `Access-Control-Allow-Origin: https://www.google.com`

Alternatively, we could allow **all** origins:

- `Access-Control-Allow-Origin: *`: This setting is common for servers that are
  in development, or that serve resources to the public.

In production, you'll usually want to lock access down to a **specific origin**,
i.e. the list of domains you know will be making requests to your server.

CORS follows the model of **explicitly allowing access** to a particular
privilege or service.
