# What is a Web Server?

Node can be used to write programs to do pretty much anything, but it's most
commonly used for building **backends** for our client applications. But what is
a **backend**?

When a browser creates a request for web assets, it starts by sending a request
to a **DNS server** with the name of the resource we're looking for. The DNS
server responds with the **IP address** of the server that serves the resource.
It is this address that we use to communicate with the web server, or **HTTP
server**, so called because it uses the HTTP **protocol** (or HTTPS, a secure
version in which all transmissions are encrypted). Our browser can also
**cache** the IP addresses in its DNS cache.

The server responds with "data": `json`, `xml`, `txt`, `html`, images,
JavaScript, etc.
