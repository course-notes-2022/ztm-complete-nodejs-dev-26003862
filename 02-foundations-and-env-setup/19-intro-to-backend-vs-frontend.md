# Intro to Backend vs. Frontend

In most cases, we use node to **create servers**. Let's understand backend
technology and how it compares to the frontend.

For web and mobile, there's a **client-server** architecture. The **client** is
your browser or mobile device. The **server** contains your business logic that
you usually want to protect.

When you load a website into your browser (like `google.com`), the browser
requests **data** from the `google.com` domain. The server responds with the
data required to build the page: HTML, CSS, JavaScript, images, `JSON`, `XML`,
etc.

The **communication protocol** that the client and server use to communicate is
called **HTTP** (or **HTTPS**, the secure version of HTTP in which all data back
and forth is encrypted).

You can use a **wide variety** of programming languages to create servers:
NodeJS, Python, Go...etc. Node allows developers the \*\*huge advantage of being
able to write _frontend and backend code_ in the _same language_!
