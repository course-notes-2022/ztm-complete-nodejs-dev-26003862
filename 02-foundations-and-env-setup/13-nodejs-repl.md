# The NodeJS REPL

When running the NodeJS REPL in the terminal (using the `node` command with no
arguments), we can type in JS code and the REPL will respond to the input by:

- Reading it
- Evaluating
- Printing the result
- Looping back to the "Read" phase (ready for the next line of input)

The "Read" step reads and **parses** our code, meaning the V8 engine breaks down
our code and understands each part of it:

```js
const cheer = 'woo' + 'hoo';
```

The engine reads this line as a concatenated string into a constant value, and
prints it in our terminal.

Note that the code we typed previously in the same session still remains in our
REPL. We could print out the `cheer` constant again at any time.

REPLs tend to be best for quick testing of small pieces of code in isolation. No
one uses a REPL to write a large application. It doesn't provide a way for us to
save our code in files or edit our code in a future session. Let's learn to
setup a better dev enviornment in the next lessons.
