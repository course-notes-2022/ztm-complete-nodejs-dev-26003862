# NodeJS vs. Javascript

How is Node **different from Javascript**?

Javascript is a **programming language**. Whenever we want to **run** some JS
code, we have to send it to an **engine** that can **run Javascript**, like
Chrome's V8 or Firefox's Spider Monkey. This engine converts the Javascript code
to **machine code** that the computer executes.

This flow applies regardless of which engine we use to run our code.

NodeJS is a way of running JS instructions **outside of a browser**. Prior to
Node, the only JS engines existed in browsers. Node was built so we can run JS
**anywhere**: on a computer, in a mobile device, etc.

There are **newer JS runtimes** available, like `Deno`, but Node is where it all
started.

## What's The Difference in Running JS code in Node vs. Browsers?

For one thing, the `Window` global object is not defined in Node. This makes
sense, as there is no browser window to represent outside of a browser. Instead,
we have an object called `global`. We can access `global.process.argv` without
having to import it (or even use the `global` prefix). `global` gives us some
access to very useful things!
