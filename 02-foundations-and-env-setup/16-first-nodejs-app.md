# Our First NodeJS App

Let's build our first, simple NodeJS project. Refactor `hello.js` as follows:

```js
const mission = 'learn';

if (mission === 'learn') {
  console.log('Time to write some node code!');
} else {
  console.log(`Is ${mission} really more fun?`);
}
```

Note that we're using **template strings**. NodeJS is kept up-to-date with the
latest EcmaScript features and standards. This means that newer versions of Node
will contain the latest JS features.

Up until now, our Node code has been just Javascript. Because node is a
**runtime**, it has extra features that are not available in the browser. Node
comes with its own **API** that are _not_ pure JS that are include in the
runtime.

For example, enter the REPL in the terminal by typing `node`, and then type
`process`. Note that we get an large object as output with information about the
**process**, or the node program that is currently running.

Inspect the `argv` property on `process`. This contains the **path** to your
node executable. Inspect the
[documentation for the `process.argv` property](https://nodejs.org/docs/latest/api/process.html#processargv),
and note that:

> "The `process.argv` property returns an array containing the command-line
> arguments passed when the Node.js process was launched. The first element will
> be `process.execPath`. See `process.argv0` if access to the original value of
> argv[0] is needed. The second element will be the path to the JavaScript file
> being executed. The remaining elements will be any additional command-line
> arguments."

We can use `process.argv` to get access to the command-line arguments in our
node programs. Modify `hello.js` as follows:

```js
const mission = process.argv[2];

if (mission === 'learn') {
  console.log('Time to write some node code!');
} else {
  console.log(`Is ${mission} really more fun?`);
}
```

Run:

> `node hello.js explore`

And note the output is:

```
Is explore really more fun?
```
