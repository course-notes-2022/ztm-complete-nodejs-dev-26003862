# The `global` Object

Let's compare our `Window` API with the Node `global`.

![browser vs node](./screenshots/browser-vs-node.png)

In browsers, we have a `window` global that deals with HTML and browsing
documents, instead of a `process`. We have a `history` and `location` that give
us information about the state of our navigation. These don't make much sense in
node; instead we have information about the current `process` and `modules` that
we've loaded, as well as the `__filename` and `__dirname`.

Probably the most important global is the `require` function. It allows us to
import functions and modules into our programs that allow us to do many useful
things. We'll dive into much more detail on Node globals in future lessons.

[See the NodeJS `globals` documentation](https://nodejs.org/docs/latest/api/globals.html)
for detailed information on the available globals.
