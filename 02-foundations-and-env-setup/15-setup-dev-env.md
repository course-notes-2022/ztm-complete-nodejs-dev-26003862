# Setup Dev Environment

Once Node is installed, open up your favorite text editor/IDE to begin writing
JS code. In this course, we'll use `Visual Studio Code`. Create a new file,
`hello.js`, and add the following:

```js
function findMeaning() {
  return 42;
}

console.log(findMeaning());
```

Save and run the file in your REPL by typing:

> `node hello.js`

in your terminal. Note the output is `42`.

## VS Code Extensions for Node

Install the `VS Code Intellicode` extension for an AI-enabled code-assitance.
This makes the auto-completion in VS Code a bit more effective. This is
**optional**. We'll explore some more extensions as the course goes on.
