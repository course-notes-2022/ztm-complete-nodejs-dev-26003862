# Install NodeJS

**NodeJS** is a **runtime** for Javascript, built on Chrome's **V8** engine. It
uses V8 to **understand Javascript** and **give instructions** to the computer.

Let's get Node set up on our machines and start writing some code.

Go to [https://nodejs.org](https://nodejs.org), and follow the installation
instructions for your OS. You can choose from the `LTS` (long-term support)
option, or the cutting-edge (potentially buggy/unstable) `Current` version. For
production systems, it's best to choose the `LTS` version, and that's what we'll
use in this course.

You can install via package manager (for your Linux distribution), or via a
pacakage manager such as `nvm`. Verify the installation via the `node -v`
command to output the version of node.
