# The NodeJS Release Cycle

Let's discuss the version of node that we just installed. As we discussed
previously, there is always a "Current" and "LTS" version of node available.

Why do we _need_ an LTS version? Isn't it better to always use the most current
version?

In this case, not necessarily! The most current version of any software can
potentially contain bugs.

If we visit
[https://nodejs.org/en/about/previous-releases](https://nodejs.org/en/about/previous-releases),
we can see the release schedule of new NodeJS versions:

![nodejs release schedule](./screenshots/nodejs-release-schedule.png)

All the even-numbered node versions will eventually become **LTS** versions,
while all odd-numbered versions will become **Current** versions. LTS versions
are separated into "Active" and "Maintenance" phases. When active, most changes
are accepted as long as they are non-breaking. When in "Maintenance", only
important bug-fixes and security updates will be applied for that version.

One strategy on deciding which version to use: if you are deploying _soon_ (i.e.
in a few weeks/months), you might select the LTS version for extra confidence
that your code won't break when you update your node runtime to get the latest
security updates. If you are deploying longer term, you might select the Current
version since there will have been fewer changes between the current version and
the newest LTS version.
