# NASA Dashboard Functionality

Let's look at our frontend application's functionality, and what it needs to
support.

Our application allows us to schedule a mission for interstellar travel to one
of the Kepler Exoplanets. On the main page, we can see our criteria for
habitable planets. We have an input for a launch date, a mission name, a rocket
type, and the destination planet. The destination planets select input will be
populated by data coming from our API server that we'll create. The "Launch
Mission" button adds the launch to the Upcoming missions. We can abort the
mission by clicking the `X` in the "Upcoming missions" view. The "History" view
shows us the history of launched missions.
