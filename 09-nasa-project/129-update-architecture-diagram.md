# Updating our Architectural Diagram

![updated project architectural diagram](./screenshots/project-arch-diagram.png)

As we figure out the structure of our project, and the architecture grows, it's
important to keep the documentation updated.

Note that we've:

- Updated the parent project, client and server apps to reflect the **respective
  names of their `Node` projects**
- Added a bubble for the client machine and the protocol by which it accesses
  our client app
- Added a **dependency** to the Node API on the Kepler exoplanet data, which
  originally comes from the cloud
