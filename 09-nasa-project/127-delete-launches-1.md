# `DELETE /launches`: Abort Launches 1

Let's now add the ability to **delete** a launch. When we click the red "X" in
the UI, we want to delete the associated launch record from the server data.

For delete operations, best practice is to use the `DELETE` HTTP method. Our
delete operation will return `200` if the operation is successful, or `404` if
the requested resource was not found. We'll implement a "top-down" approach this
time, starting from the functionality closest to to the **client**.

In the `Upcoming` page component function, when the user clicks the `x` the
`abortLaunch` function is called, passing in the `flightNumber` of the launch to
abort:

```jsx
<Clickable style={{ color: 'red' }}>
  <Link
    className={classes.link}
    onClick={() => abortLaunch(launch.flightNumber)}
  >
    ✖
  </Link>
</Clickable>
```

`abortLaunch` is passed as a prop, and comes from the `hooks/useLaunches` file:

```js
const abortLaunch = useCallback(
  async (id) => {
    const response = await httpAbortLaunch(id);

    // TODO: Set success based on response.
    const success = false;
    if (success) {
      getLaunches();
      onAbortSound();
    } else {
      onFailureSound();
    }
  },
  [getLaunches, onAbortSound, onFailureSound]
);
```

Which in turn calls the `httpAbortLaunch` function in `hooks/requests`:

```js
async function httpAbortLaunch(id) {
  // TODO: Once API is ready.
  // Delete launch with given ID.
}
```

Which we'll implement now:

```js
async function httpAbortLaunch(id) {
  try {
    return await fetch(`${API_URL}/launches/${id}`, { method: 'DELETES' });
  } catch (error) {
    return { ok: false };
  }
}
```

Note that we're again wrapping in a `try-catch` block to handle cases of a
network/CORS error.

## Updating the Server

Refactor `launches.router`:

```js
const express = require('express');
const {
  httpGetAllLaunches,
  httpAddNewLaunch,
  httpAbortLaunch,
} = require('./launches.controller');

const launchesRouter = express.Router();

launchesRouter.get('/', httpGetAllLaunches);

launchesRouter.post('/', httpAddNewLaunch);

// Create new route to delete launch; pass `id` as route parameter
launchesRouter.delete('/:id', httpAbortLaunch);

module.exports = launchesRouter;
```

Create the stub for the `httpAbortLaunch` function in `launches.controller`:

```js
function httpAbortLaunch(req, res) {
  const launchId = req.params.id;

  // if launch doesn't exist
  return res.status(404).json({ error: 'Launch not found' });

  // if launch does exist
  return res.status(200).json(aborted);
}
```

Update `launches.model` with the following function. We'll use it to check if
the launch identified by `launchId` **exists in the `launches` data**:

```js
function existsLaunchWithId(launchId) {
  return launches.has(launchId);
}
```
