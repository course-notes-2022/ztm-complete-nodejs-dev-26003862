# React Front-end Code Walkthrough

Let's learn what our frontend is doing conceptually and learn how we can
integrate our API server.

We're using `arwes`, a package for our sci-fi components, and `react` with
`create-react-app`. `react-router-dom` allows our client to serve different
content based on the URL. `react-dom` allows React to make use of the browser
`DOM` (React doesn't support this by default because it doesn't necessarily
always run in a browser).

In the `public` folder, we have images inside the `img` folder and `.mp3` files
in the `sound` folder, as well as the `index.html` file which loads the React
app in the `#root` element. All these files in the `public` folder will be
served to the browser directly.

`index.js` contains the `render` method, which renders our entire React app
inside the `#root` element.

`App.js` contains the meat of our project, with providers for themes and sounds
from `arwes`. The component where our routes are defined is
`components/AppLayout`.

## `hooks`

Hooks are React's way of responding to events that occur in your application and
managing the resulting state. With hooks, you take the state of your React
components and move it into the hooks instead. This **simplifies passing data
around** among components.

for example, the `usePlanets` hook:

`/hooks/usePlanets.js`:

```js
import { useCallback, useEffect, useState } from 'react';

import { httpGetPlanets } from './requests';

function usePlanets() {
  const [planets, savePlanets] = useState([]);

  const getPlanets = useCallback(async () => {
    const fetchedPlanets = await httpGetPlanets();
    savePlanets(fetchedPlanets);
  }, []);

  useEffect(() => {
    getPlanets();
  }, [getPlanets]);

  return planets;
}

export default usePlanets;
```

returns `planets`, which is initally an empty array but which we set to the
results of an HTTP request to fetch the planets from the backend. React knows to
keep track of the value of `planets`. `useCallback` allows React to avoid
needlessly recomputing the values inside it.

Finally, we import these hooks into our components that need access to their
data, and use them, for example `AppLayout`:

```js
import usePlanets from '../hooks/usePlanets'; // Import usePlanets hook

const AppLayout = (props) => {
  const { sounds, classes } = props;

  const [frameVisible, setFrameVisible] = useState(true);
  const animateFrame = () => {
    setFrameVisible(false);
    setTimeout(() => {
      setFrameVisible(true);
    }, 600);
  };

  const onSuccessSound = () => sounds.success && sounds.success.play();
  const onAbortSound = () => sounds.abort && sounds.abort.play();
  const onFailureSound = () => sounds.warning && sounds.warning.play();

  const { launches, isPendingLaunch, submitLaunch, abortLaunch } = useLaunches(
    onSuccessSound,
    onAbortSound,
    onFailureSound
  );

  // Use the value from `usePlanets` hook:
  const planets = usePlanets();

  // OMITTED...
};
```
