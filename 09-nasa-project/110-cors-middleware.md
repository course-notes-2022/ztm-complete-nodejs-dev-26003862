# CORS Middleware

How do we enable CORS in our APIs?

Recall that we can enable CORS requests using the `Access-Control-Allow-Origin`
header on the server:

- `"Access-Control-Allow-Origin": "*"`: allows requests from ALL origins
- `"Access-Control-Allow-Origin: "mydomain.com"` allows requests from
  `mydomain.com` only

With express, setting the CORS headers is as simple as adding some
**middleware**. Install the `cors` package with `npm install cors`. Since all
our routes will follow the same CORS behavior, we can include it at the top of
our app:

```js
const express = require('express');
const app = express();
const cors = require('cors');
const planetsRouter = require('./routes/planets.router');

app.use(cors, { origin: 'http://localhost:3000' });
app.use(express.json());
app.use(planetsRouter);

module.exports = app;
```

`cors` accepts a configuration object that we can use to **specify the origins**
we want to allow. `cors` also supports **multiple origin definitions**; see the
documentation for more information.
