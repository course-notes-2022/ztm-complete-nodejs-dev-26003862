# The `Launches` Model

Let's implement our `Launches` functionality. We'll start with the `Launches`
model.

Our model will need to track the following data:

- Launch date
- Mission Name
- Rocket type
- Destination exoplanet
- Flight number
- Customers: organizations that fund the mission

Create a new file, `src/models/launches.model.js`, and add the following:

```js
const launches = new Map();

const launch = {
  flightNumber: 100,
  mission: 'Kepler Exploration X',
  rocket: 'Explorer IS1',
  launchDate: new Date('December 27, 2030'),
  destination: 'Kepler-442 b',
  customers: ['ZTM', 'NASA'],
  upcoming: true,
  success: true,
};

launches.set(launch.flightNumber, launch);

module.exports = {
  launches,
};
```
