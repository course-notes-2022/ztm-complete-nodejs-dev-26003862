# Automating Full-Stack Applications with Node

We have a full-stack app with two folders and two npm packages that we need to
switch back and forth between; to run our NASA client we first need to run our
server, and then run the client in another terminal. Not the ideal experience!
Let's make development a little bit easier.

We can create a 3rd `package.json` at the **root** of our project, and use it to
manage our **sub-projects**.

Initialize a new project in the project root with `npm init -y`, and add the
following to the root `package.json`:

```json
{
  "name": "project",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "server": "cd server && npm run watch",
    "client": "cd client && npm run start"
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
}
```

Running the `server` and `client` scripts will allow us to start up the **child
projects** from the \*parent\*\*.

## A Better Approach

We can also leverage the `--prefix` flag on the `npm` command to tell `npm` the
**location folder** of the commands we want to run. Refactor `package.json` as
follows:

```json
{
  "name": "project",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "server": "npm run watch --prefix server",
    "client": "npm run start --prefix client"
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
}
```

## Running Both Commands

We can refactor the `watch` script to run both our client and server:

```json
{
  "name": "project",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "server": "npm run watch --prefix server",
    "client": "npm run start --prefix client",
    "watch": "npm run server & npm run client"
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
}
```

**Note the use of the _single_ `&`** (NOT the `&&`). The `&` signifies to the
shell to **not wait** for the previous command to **complete** before starting
the **next** command. We need to do this because technically our server **never
completes** (never exits), and thus the `npm run client` script will never start
if we use `&&`.

Let's add a couple of more scripts to help with installing `npm` packages:

```json
{
  "name": "project",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "server": "npm run watch --prefix server",
    "client": "npm run start --prefix client",
    "watch": "npm run server & npm run client",
    "install-server": "npm install --prefix server",
    "install-client": "npm install --prefix client",
    "install": "npm run install-server && npm run install-client"
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
}
```

We can now manage our client and server projects as **one project**!
