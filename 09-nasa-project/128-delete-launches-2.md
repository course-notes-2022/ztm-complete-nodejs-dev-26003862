# `DELETE /launches`: Aborting Launches 2

Let's continue with aborting our launch. Refactor `launches.model` as follows:

```js
function abortLaunchById(launchId) {
  const aborted = launches.get(launchId);
  aborted.upcoming = false;
  aborted.success = false;
}
```

Note that we _could have_ deleted the launch from the `launches` data map, but
it's common in the era of big data to **keep it** (for analytics purposes?).
Instead, we'll simply mark it as aborted above.

Refactor `launches.controller` as follows:

```js
const launches = new Map();

let latestFlightNumber = 100;

const launch = {
  flightNumber: latestFlightNumber,
  mission: 'Kepler Exploration X',
  rocket: 'Explorer IS1',
  launchDate: new Date('December 27, 2030'),
  target: 'Kepler-442 b',
  customers: ['ZTM', 'NASA'],
  upcoming: true,
  success: true,
};

launches.set(launch.flightNumber, launch);

function existsLaunchWithId(launchId) {
  return launches.has(+launchId); // Convert if to a NUMBER
}

function getAllLaunches() {
  return Array.from(launches.values());
}

function addNewLaunch(launch) {
  latestFlightNumber++;
  let newLaunch = {
    ...launch,
    customers: ['Zero to Mastery', 'NASA'],
    upcoming: true,
    success: true,
    flightNumber: latestFlightNumber,
  };
  launches.set(latestFlightNumber, newLaunch);
}

function abortLaunchById(launchId) {
  const aborted = launches.get(+launchId); // Convert id to a NUMBER
  aborted.upcoming = false;
  aborted.success = false;
  return aborted; // Return aborted
}

module.exports = {
  addNewLaunch,
  getAllLaunches,
  existsLaunchWithId,
  abortLaunchById,
};
```

**Quick bugfix in the UI**: Update the `abortLaunches` function in
`useLaunches.js` to use the response `ok` value:

```js
const abortLaunch = useCallback(
  async (id) => {
    const response = await httpAbortLaunch(id);

    // Set success based on response.
    const success = response.ok;
    if (success) {
      getLaunches();
      onAbortSound();
    } else {
      onFailureSound();
    }
  },
  [getLaunches, onAbortSound, onFailureSound]
);
```
