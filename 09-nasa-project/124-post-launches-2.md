# `POST /launches`: Create Launches 2

Let's now implement our `launches.controller` to respond to this request.
Refactor `launches.controller` as follows:

```js
const { getAllLaunches, addNewLaunch } = require('../../models/launches.model');

function httpGetAllLaunches(req, res) {
  return res.status(200).json(getAllLaunches());
}

function httpAddNewLaunch(req, res) {
  console.log(req.body);
  const launch = req.body;
  launch.launchDate = new Date(launch.launchDate); // Convert value passed by request body to a Date object
  addNewLaunch(launch);
  return res
    .status(201)
    .json(getAllLaunches().find((l) => l.mission === launch.mission));
}
module.exports = { httpGetAllLaunches, httpAddNewLaunch };
```

Refactor `launches.router` to use the new `httpAddNewLaunch` method from the
`launches.controller`:

```js
const express = require('express');
const {
  httpGetAllLaunches,
  httpAddNewLaunch,
} = require('./launches.controller');

const launchesRouter = express.Router();

// Cleaner syntax: route handlers can now respond to simply `/` route
launchesRouter.get('/', httpGetAllLaunches);

launchesRouter.post('/', httpAddNewLaunch);

module.exports = launchesRouter;
```

Refactor `app` to mount the `launchesRouter` to the `/launches` path:

```js
const express = require('express');
const path = require('path');
const cors = require('cors');
const morgan = require('morgan');
const app = express();
const planetsRouter = require('./routes/planets/planets.router');
const launchesRouter = require('./routes/launches/launches.router');

app.use(cors({ origin: 'http://localhost:3000' }));
app.use(morgan('combined'));
app.use(express.json());
app.use(express.static(path.join(__dirname, '..', 'public')));
app.use('/planets', planetsRouter);
app.use('/launches', launchesRouter); // Cleaner syntax: mount launches router to `/launches` path

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, '..', 'public', 'index.html'));
});

module.exports = app;
```

Save changes and send a new request to `POST /launches` with a valid body. The
response should look like the following:

```json
{
  "mission": "ZTM156",
  "rocket": "ZTM Experimental IS1",
  "destination": "Kepler-186 f",
  "launchDate": "2030-01-17T05:00:00.000Z",
  "customers": ["Zero to Mastery", "NASA"],
  "upcoming": true,
  "success": true,
  "flightNumber": 101
}
```
