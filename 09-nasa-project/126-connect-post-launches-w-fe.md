# Connecting `POST /launches` with Client Dashboard

Let's now take the API we've built and connect it to our React client.

Update the `httpSubmitLaunch` function in `client/src/hooks/requests.js` as
follows:

```js
async function httpSubmitLaunch(launch) {
  try {
    const response = await fetch(`${API_URL}/launches`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json', // MUST pass this header when sending JSON
      },
      body: JSON.stringify(launch),
    });
    return response;
  } catch (err) {
    return { ok: false };
  }
}
```

**Note**:

- We are wrapping the `fetch` function in a **`try-catch` block**. The `fetch`
  function **does not complete** if there is a **network error or `CORS` is
  misconfigured on the server side**, and instead **throws an error**. Therefore
  there _will be no `response`_, and now `ok` property available. We catch the
  error and return an object with an `ok` property set to `false` in the event
  of a server error.

We are _depending on_ the `ok` property below in our `submitLaunches` method in
`useLaunches.js`, to display some feedback to the user upon successful launch
creation:

```js
const submitLaunch = useCallback(
  async (e) => {
    e.preventDefault();
    setPendingLaunch(true);
    const data = new FormData(e.target);
    const launchDate = new Date(data.get('launch-day'));
    const mission = data.get('mission-name');
    const rocket = data.get('rocket-name');
    const target = data.get('planets-selector');
    const response = await httpSubmitLaunch({
      launchDate,
      mission,
      rocket,
      target,
    });

    const success = response.ok;
    if (success) {
      getLaunches();
      setTimeout(() => {
        setPendingLaunch(false);
        onSuccessSound();
      }, 800);
    } else {
      onFailureSound();
    }
  },
  [getLaunches, onSuccessSound, onFailureSound]
);
```

Finally, let's correct a mismatch that we currently have between the request
body our client is _sending_ and what the server is _expecting_, namely the
`target` property coming from the client. The server is expecting a
`destination`. For now, we'll change the **server** to expect a `target`:

`launches.model.js`:

```js
const launch = {
  flightNumber: latestFlightNumber,
  mission: 'Kepler Exploration X',
  rocket: 'Explorer IS1',
  launchDate: new Date('December 27, 2030'),
  target: 'Kepler-442 b',
  customers: ['ZTM', 'NASA'],
  upcoming: true,
  success: true,
};
```

`launches.controller.js`:

```js
function httpAddNewLaunch(req, res) {
  const launch = req.body;

  if (
    !launch.mission ||
    !launch.rocket ||
    !launch.launchData ||
    !launch.target // Not `launch.destination`
  ) {
    return res.status(400).json({ error: 'Missing required launch property' });
  }
  launch.launchDate = new Date(launch.launchDate);

  if (launch.launchDate.toString() === 'Invalid Date') {
    return res.staus(400).json({ error: 'Invalid launch date' });
  }

  addNewLaunch(launch);
  return res
    .status(201)
    .json(getAllLaunches().find((l) => l.mission === launch.mission));
}
```
