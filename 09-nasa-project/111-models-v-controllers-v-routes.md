# Models vs. Controllers vs. Routes

Why do we have our models **separate** from our routers and controllers?

We keep our routers and controllers together because they're always one-to-one.
We may have **many** models used in a single controller. For example, we may
have a `planets` model and a `stars` model that we may be using in a controller
`controller-a` as some composite data. Or, a model may be used in multiple
controllers.

In either case, our models and controllers are _not_ one-to-one. Often, our data
is stored in the DB in a way that doesn't correspond to the way or controller
**needs** it. This is where the **model** provides an abstraction that can
**shape the data** in a way that is usable by our application.

Our routers and controllers will live together. We'll keep our models separate.
