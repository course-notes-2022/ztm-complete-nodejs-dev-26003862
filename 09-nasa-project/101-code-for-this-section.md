# Code for This Section

You can find the "final" code for this section at this repository below. Keep in
mind that we recommend you code along with us and use this only if you ever get
stuck or you don't like to code along.

[https://github.com/odziem/nasa-project](https://github.com/odziem/nasa-project)

The front end code we'll be using for this section is attached to the NASA
Dashboard Front End Setup lecture as `nasa-front-end.zip`.
