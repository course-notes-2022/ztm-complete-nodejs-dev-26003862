# Working with Data Models: Building a Data Access Layer

Where do we draw the line between what goes in the **model**, and what goes in
the **controller**?

Ideally, we don't want our controllers worrying about _how_ the data in the
model is stored: a database, in the cloud, in memory...etc. We want our
controllers to focus on working with the **request and response**, and our
models will give us functions to **read and write the model data**.

Let's refactor our `launches.controller` to remove the logic parsing and
transforming the `json`:

```js
const { getAllLaunches } = require('../../models/launches.model');

function getAllLaunches(req, res) {
  return res.status(200).json(getAllLaunches());
}

module.exports = { getAllLaunches };
```

Instead, we import a `getAllLaunches` method from the `launches.model`, where
we'll handle all parsing and transformation. The controller now **doesn't care**
about the **shape of the data**, and is only concerned with **responding to the
request**.

Let's now refactor `launches.model` to export `getAllLaunches`:

```js
const launches = new Map();

const launch = {
  flightNumber: 100,
  mission: 'Kepler Exploration X',
  rocket: 'Explorer IS1',
  launchDate: new Date('December 27, 2030'),
  destination: 'Kepler-442 b',
  customers: ['ZTM', 'NASA'],
  upcoming: true,
  success: true,
};

launches.set(launch.flightNumber, launch);

function getAllLaunches() {
  return Array.from(launches.values());
}

module.exports = {
  getAllaunches,
};
```

Let's apply this model to our `planets` controller as well. Refactor the
controller as follows;

```js
const { getAllPlanets } = require('../../models/planets.model'); // Import `getAllPlanets` from the planets.model

function httpGetAllPlanets(req, res) {
  return res.status(200).json(getAllPlanets());
}

module.exports = {
  httpGetAllPlanets,
};
```

Create the `getAllPlanets` function in `planets.model`:

```js
function getAllPlanets() {
  return habitablePlanets;
}

module.exports = {
  getAllPlanets,
  loadPlanetsData,
};
```

Refactor `planets.router` to use the `httpGetAllPlanets` function from the
controller:

```js
const express = require('express');
const { httpGetAllPlanets } = require('./planets.controller');

const planetsRouter = express.Router();

planetsRouter.get('/planets', httpGetAllPlanets);

module.exports = planetsRouter;
```

This **layered approach** is part of a design principle called **separation of
concerns**: the idea is that _one module_ should be responsible for _one thing
only_.
