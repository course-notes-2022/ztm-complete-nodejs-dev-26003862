# Introduction and Architecture

Welcome to our final back-end project! We'll build an API and integrate it into
a large, full-stack application.

We'll build a NASA mission-control dashboard. Whenever we build a full-stack app
with many front and back-end components, it pays to think about the
**structure** of our applications **beforehand**. We want to have a **general
idea** of how our code will be structured. The architect or senior developer
often does this work at the beginning of the project to ensure that it gets
started properly.

For our project, we'll use the following structure:

```
| -/client
| -/server
```

`client` will host our client application, and `server` will host the Node API
server.

## LucidChart

We often use drawing tools to help us diagram our application architecture.
[**Lucidchart**](https://www.lucidchart.com/pages/) is one popular tool.

## Our Chart

For now, we'll keep our chart simple:

![NASA project diagram 1](./screenshots/nasa-project-diagram-1.png)

## Exploring the Backend

Let's start with the folder structure for our server. We'll use Express, and the
pattern most express servers use is **MVC**. We'll use this pattern in our
server. Create the following folder structure:

```
| -/server
| --/server.js
| --/models
| --/routes
```

Recall that the **controllers** are the functions that respond to requests to
our **routes**. The code that defines our API endpoints and the controllers
should live together, so we'll put both in the `routes` folder.

For us, the **view** is entirely controlled by the frontend application.

It's common to create **separate packages** for the client and the server. This
means that each will have its _own_ package.json, and _another_ package.json at
the **root** of the project. This will allow us to run `npm` commands from the
project root that perform actions on each of our **inner** packages.
`package.json` should always be created by `npm init`.
