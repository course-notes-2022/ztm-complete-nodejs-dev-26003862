# Serving Applications with Client-Side Routing

When we navigate to the `/history` page, our production build fails with:

```
Cannot GET /history
```

Why?

In our production build, our server is serving our client app from the `/`
endpoint. However, routing **within the client app is still being handled on the
_client side_**, in this case by `react-router-dom`:

```js
// OMITTED...

const AppLayout = (props) => {
  const { sounds, classes } = props;

  // OMITTED...

  return (
    <div className={classes.content}>
      <Header onNav={animateFrame} />
      <Centered className={classes.centered}>
        <Frame
          animate
          show={frameVisible}
          corners={4}
          style={{ visibility: frameVisible ? 'visible' : 'hidden' }}
        >
          {(anim) => (
            <div style={{ padding: '20px' }}>
              <Switch>
                <Route exact path="/">
                  <Launch
                    entered={anim.entered}
                    planets={planets}
                    submitLaunch={submitLaunch}
                    isPendingLaunch={isPendingLaunch}
                  />
                </Route>
                <Route exact path="/launch">
                  <Launch
                    entered={anim.entered}
                    planets={planets}
                    submitLaunch={submitLaunch}
                    isPendingLaunch={isPendingLaunch}
                  />
                </Route>
                <Route exact path="/upcoming">
                  <Upcoming
                    entered={anim.entered}
                    launches={launches}
                    abortLaunch={abortLaunch}
                  />
                </Route>
                <Route exact path="/history">
                  <History entered={anim.entered} launches={launches} />
                </Route>
              </Switch>
            </div>
          )}
        </Frame>
      </Centered>
      <Footer />
    </div>
  );
};

export default withSounds()(withStyles(styles)(AppLayout));
```

Our express server is trying to find the file `/history` using our static
file-serving middleware. If that doesn't match, it tries to find the `/history`
endpoint in our API, which doesn't exist.

With a client-side-routed React app served by an Express server, we want to
respond with our `index.html` file to **all requests for static html files**.
Again, the routing in the **client app** will be handled **in the browser**,
**by the client app**.

Make the following change to `app.js`:

```js
const express = require('express');
const path = require('path');
const cors = require('cors');
const morgan = require('morgan');
const app = express();
const planetsRouter = require('./routes/planets/planets.router');
const launchesRouter = require('./routes/launches/launches.router');

app.use(cors({ origin: 'http://localhost:3000' }));
app.use(morgan('combined'));
app.use(express.json());
app.use(express.static(path.join(__dirname, '..', 'public')));
app.use(planetsRouter);
app.use(launchesRouter);

// Respond to ALL get requests with React app's `index.html`
app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, '..', 'public', 'index.html'));
});

module.exports = app;
```

This will respond to any `GET` requests that are _not matched by any router
above it_ with the `index.html` file in the `public` directory.

Re-deploy the production app with `npm run deploy` in the project root. Note
that we can now navigate **directly to any page in our app** by typing the URL
into the browser bar! This technique will work with Angular, Vue, or any other
client-side framework that uses the
[`pushState()` method of the `HistoryAPI`](https://developer.mozilla.org/en-US/docs/Web/API/History_API/Working_with_the_History_API#using_pushstate).
