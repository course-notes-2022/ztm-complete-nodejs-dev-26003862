# Loading Data on Startup

Let's learn what to do when we need to **populate our server with data on
startup**. This is a very common task.

In the previous lesson, we loaded and parsed our `planets` data as a `Stream` in
`planets.model.js`. This happens asynchronously: we start the process by calling
`fs.creteReadStream` on the `.csv` file, _but Node doesn't wait until this
process is finished before executing the rest of the file and exporting the
`planets` data_. All this code is run when we `require` the `planets.model.js`
file in the `planets.controller`.

If our server starts serving routes before the `planets` data has loaded, our
clients may attempt to access the planets data _before_ it is available! This is
not the experience we want. Let's see how we can fix that now.

## A Review of `Promises`

We can wrap our loading code in a `Promise`, and **await the `Promise`'s
resolution _before starting our server_**. Recall the following about
`Promises`:

```js
const promise = new Promise((resolve, reject) => {
  resolve(42); // The value passed to `resolve` will become the `result` value passed to `promise.then`
});

promise.then((result) => {
  // Do something with the resolved value
});

const result = await promise; // Alternatively, use the `await` keyword
```

## Refactoring our `planets.model`

Our solution is to refactor `planets.model` to **return a `Promise` once the
planets have been found**, and **wait for that promise to resolve _before_
starting our server**. Refactor as follows:

```js
const fs = require('fs');
const path = require('path');
const { parse } = require('csv-parse');

const habitablePlanets = [];

function isHabitablePlanet(planet) {
  return (
    planet['koi_disposition'] === 'CONFIRMED' &&
    planet['koi_insol'] > 0.36 &&
    planet['koi_insol'] < 1.11 &&
    planet['koi_prad'] < 1.6
  );
}

function loadPlanetsData() {
  // Create new function `loadPlanetsData`
  return new Promise((resolve, reject) => {
    fs.createReadStream(
      path.resolve(__dirname, '..', '..', 'data', 'kepler_data.csv')
    )
      .pipe(
        parse({
          comment: '#',
          columns: true,
        })
      )
      .on('data', (planet) => {
        if (isHabitablePlanet(planet)) {
          habitablePlanets.push(planet);
        }
      })
      .on('error', (err) => {
        reject(err); // Reject on error
      })
      .on('end', () => {
        // Resolve the promise; note that we're NOT
        // passing any data to `resolve` because we're
        // SETTING the `habitablePlanets` array that exits
        // OUTSIDE this function's scope above
        resolve();
      });
  });
}

module.exports = {
  planets: habitablePlanets,
  loadPlanetsData,
};
```

Our `planetsModel` is returning an Object now, so we need to **destructure** the
`planets` off of it in `planets.controller`:

```js
const { planets } = require('../models/planets.model');

function getAllPlanets(req, res) {
  return res.status(200).json(planets);
}

module.exports = {
  getAllPlanets,
};
```

Now, in `index.js`, we want to wait until `loadPlanetsData` completes before
starting the server:

```js
const http = require('http');
const app = require('./app');
const { loadPlanetsData } = require('./models/planets.model');
const PORT = process.env.PORT || 8000;

const server = http.createServer(app);

async function fetchPlanets() {
  await loadPlanetsData(); // Call `loadPlanetsData`; this will block server froms starting until planets data load is complete
  server.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
  });
}

fetchPlanets();
```

**This is a very common Node pattern that we can reuse** whenever we have some
**actions that must be performed** before we want to start a server, including:

- Starting up a database
- Downloading some files
- Checking if some services we rely on are available
- etc.

Save all changes and refresh. Note that our frontend is now populated with our
Kepler exoplanets!

## Final Note

Starting from Node v.15, the `Streams Promises API` is available that converts
`Streams` to `Promises`.
