# NASA Dashboard Front-end Setup

Our front-end code is ready to be integrated into our backend. Download the
`nasa-front-end.zip` client application from the lesson. A common situation in
web development is to make the front-end **ahead of** the back end. The
front-end usually needs to be modified once the backend is available, as it's
difficult to predict what the backend implementation will look like~

Unzip the client application. Our front-end is an `npm` package, and the react
application is built with `create-react-app`. Move the application code to the
top-level of `client` and `npm install`.

We're ready to run our scripts and start our application.
