const {
  getAllLaunches,
  addNewLaunch,
  existsLaunchWithId,
  abortLaunchById,
} = require('../../models/launches.model');

function httpGetAllLaunches(req, res) {
  return res.status(200).json(getAllLaunches());
}

function httpAddNewLaunch(req, res) {
  const launch = req.body;

  // Check for required attributes on request body
  if (
    !launch.mission ||
    !launch.rocket ||
    !launch.launchDate ||
    !launch.target
  ) {
    // Return error response if body invalid
    return res.status(400).json({ error: 'Missing required launch property' });
  }
  launch.launchDate = new Date(launch.launchDate); // Convert value passed by request body to a Date object

  // if (isNaN(launch.launchDate)) {// ...}
  if (launch.launchDate.toString() === 'Invalid Date') {
    return res.staus(400).json({ error: 'Invalid launch date' });
  }

  addNewLaunch(launch);
  return res
    .status(201)
    .json(getAllLaunches().find((l) => l.mission === launch.mission));
}

function httpAbortLaunch(req, res) {
  const launchId = req.params.id;

  // if launch doesn't exist
  if (!existsLaunchWithId(launchId)) {
    return res.status(404).json({ error: 'Launch not found' });
  }

  // if launch does exist
  const aborted = abortLaunchById(launchId);
  return res.status(200).json(aborted);
}

module.exports = { httpAbortLaunch, httpGetAllLaunches, httpAddNewLaunch };
