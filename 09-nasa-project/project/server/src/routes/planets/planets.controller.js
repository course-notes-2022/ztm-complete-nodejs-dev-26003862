const { getAllPlanets } = require('../../models/planets.model');

function httpGetAllPlanets(req, res) {
  return res.status(200).json(getAllPlanets()); // Explicit return indicates that this is where we intend our function to stop execution
}

module.exports = {
  httpGetAllPlanets,
};
