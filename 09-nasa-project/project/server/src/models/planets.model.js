const fs = require('fs');
const path = require('path');
const { parse } = require('csv-parse');

const habitablePlanets = [];

function isHabitablePlanet(planet) {
  return (
    planet['koi_disposition'] === 'CONFIRMED' &&
    planet['koi_insol'] > 0.36 &&
    planet['koi_insol'] < 1.11 &&
    planet['koi_prad'] < 1.6
  );
}

function loadPlanetsData() {
  // Create new function `loadPlanetsData`
  return new Promise((resolve, reject) => {
    fs.createReadStream(
      path.resolve(__dirname, '..', '..', 'data', 'kepler_data.csv')
    )
      .pipe(
        parse({
          comment: '#',
          columns: true,
        })
      )
      .on('data', (planet) => {
        if (isHabitablePlanet(planet)) {
          habitablePlanets.push(planet);
        }
      })
      .on('error', (err) => {
        reject(err); // Reject on error
      })
      .on('end', () => {
        // Resolve the promise; note that we're NOT
        // passing any data to `resolve` because we're
        // SETTING the `habitablePlanets` array that exits
        // OUTSIDE this function's scope above
        resolve();
      });
  });
}

function getAllPlanets() {
  return habitablePlanets;
}

module.exports = {
  getAllPlanets,
  loadPlanetsData,
};
