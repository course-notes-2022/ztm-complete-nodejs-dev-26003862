const launches = new Map();

// Keep track of the latest flight number
let latestFlightNumber = 100;

const launch = {
  flightNumber: latestFlightNumber,
  mission: 'Kepler Exploration X',
  rocket: 'Explorer IS1',
  launchDate: new Date('December 27, 2030'),
  target: 'Kepler-442 b',
  customers: ['ZTM', 'NASA'],
  upcoming: true,
  success: true,
};

launches.set(launch.flightNumber, launch);

function existsLaunchWithId(launchId) {
  return launches.has(+launchId);
}

function getAllLaunches() {
  return Array.from(launches.values());
}

// Create new method to add a launch to launches map
function addNewLaunch(launch) {
  latestFlightNumber++;
  let newLaunch = {
    ...launch,
    customers: ['Zero to Mastery', 'NASA'],
    upcoming: true,
    success: true,
    flightNumber: latestFlightNumber,
  };
  launches.set(latestFlightNumber, newLaunch);
}

function abortLaunchById(launchId) {
  const aborted = launches.get(+launchId);
  aborted.upcoming = false;
  aborted.success = false;
  return aborted;
}

module.exports = {
  addNewLaunch,
  getAllLaunches,
  existsLaunchWithId,
  abortLaunchById,
};
