# API Server Setup

Let's set up our Node server and build a production-ready API. We'll be using
the Express framework to make our development easier.

Change into the `server` directory and initialize a new `npm` project. Install
`express` as a dependency, and `nodemon` as a `dev` dependency. Add the
following `scripts` to `package.json`:

```json
  "scripts": {
    "watch": "nodemon index.js",
    "start": "node index.js"
  },
```

Refactor all the project source code inside a new `src` folder, to keep it
separate from the `package` files. Update `package.json` to point to
`src/server.js`.

## Leveraging Environment Variables

Let's begin creating our server. Add the following definition for `PORT` to
`index.js`:

```js
const PORT = process.env.PORT || 8000;
```

Note that we're making use of `process.env`. This binding in Node gives us
access to the **environment variables** that exist in the running process. We
can **set the value** of environment variables from our scripts:

```json
  "scripts": {
    "watch": "nodemon src/index.js",
    "start": "PORT=5000 node src/index.js"
  }
```

We can log the port from `index`:

```js
const PORT = process.env.PORT || 8000;

console.log(PORT);
```

Running `npm start` now will log the value `5000` to the console.

## A Different Way to Create The Express Server

`express` is really just a middleware function that we can add on top of the
built-in Node HTTP server. Create the following in `src`:

```js
const http = require('http');
const app = require('./app');
const PORT = process.env.PORT || 8000;

const server = http.createServer(app);

server.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
console.log(PORT);
```

```js
const express = require('express');
const app = express();

app.use(express.json());

module.exports = app;
```

Note that we're creating `app` from the return value of `express()` and
**importing** it into `index.js`. We then simply pass it to the
`http.createServer` function, _just as we did with the custom listener that we
wrote ourselves_:

```js
const http = require('http');

const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('Hello! Sir Isaac Newton is your friend');
});

server.listen(3001, () => {
  console.log('listening on port 3001');
});
```

This allows us to break the `app` creation logic into a separate file.
