# What You Need to Know

If you're not already familiar with `React`, don't worry. This course assumes no
prior knowledge. However, as backend devs it's helpful to know how a client will
interact with our servers. The next chapter will give a brief introduction to a
React project.
