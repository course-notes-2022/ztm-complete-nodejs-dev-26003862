# `GET /planets`

Let's add our first endpoint. We'll start by getting the list of target planets
that we want to explore.

We'll be dealing with planets data, so let's create a new `planets` router in
`routes/planets`. Recall that a **router** is simply express middleware that
groups endpoints/routes together:

```js
const express = require('express');

const planetsRouter = express.Router();

planetsRouter.get('/planets', getAllPlanets);

module.exports = planetsRouter;
```

Add the `planetsRouter` to `app`:

```js
const express = require('express');
const app = express();
const planetsRouter = require('./routes/planets.router');

app.use(express.json());
app.use(planetsRouter);

module.exports = app;
```

We now need to create a **controller** for getting planets. Recall that
**controllers** are the functions that respond to requests to our **routes**.
Let's create our planets controller alongside the planets routes in
`planets.controller.js`:

```js
const planets = require('../models/planets.model');

function getAllPlanets(req, res) {
  return res.status(200).json(planets); // Explicit return indicates that this is where we intend our function to stop execution
}

module.exports = {
  getAllPlanets,
};
```

Let's create a file `/models/planet.model.js` to model our `planets` data:

```js
const planets = [];

module.exports = planets;
```

We can now import our `planets` controller and use it in the planets router:

```js
const express = require('express');
const { getAllPlanets } = require('./planets.controller');

const planetsRouter = express.Router();

planetsRouter.get('/planets', getAllPlanets);

module.exports = planetsRouter;
```

Finally, refactor the client to send a request to our new endpoint:

`/hooks/request.js`:

```js
const API_URL = 'http://localhost:8000';

async function httpGetPlanets() {
  const response = await fetch(`${API_URL}/planets`);
  return response.json();
}

// Omitted...
```

Save changes and refresh. Note that if we inspect the client console, we get an
**error indicating CORS failures**. This is expected since the **client** is
sending a request from port 3000, but the server is running on port 8000. Let's
learn how to fix CORS in Node in the next lesson.
