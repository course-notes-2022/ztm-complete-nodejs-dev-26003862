# Logging Requests with `morgan`

Now that our server is taking in different types of requests, we want to **log**
those requests so that we can debug our server. We _could_ write our own logging
middleware, but there's a **better way**!

## `morgan`

[`morgan`](https://www.npmjs.com/package/morgan) is the most reliable logging
middleware for Node around. It provides many useful features, such as:

- Redirecting logs to various Streams: files, cloud logging services
- Log rotation: sending our logs to a new file every day/every hour
- Allows us to define the data and a custom format for our logs, or use
  **pre-defined** log formats.

`morgan` is much more complete that any logging middleware we are likely to
write ourselves. Let's use it in our server!

First, `npm install morgan` in the server project, and add it to the middleware
chain. It should go as high in the chain as it can go, after any
security-related middleware that controls which requests come into/go out of our
server:
