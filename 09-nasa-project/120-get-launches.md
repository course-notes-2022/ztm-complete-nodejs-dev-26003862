# `GET /launches`

Let's now add a new Router for handling our Launches routes.

Create a new folder, `src/routes/launches`, and add `launches.router.js` with
the following content:

```js
const express = require('express');
const { getAllLaunches } = require('./launches.controller');

const launchesRouter = express.Router();

launchesRouter.get('/launches', getAllLaunches);

module.exports = launchesRouter;
```

Create a new controller file, `launches.controller.js`, and add the following:

```js
const { launches } = require('../../models/launches.model');

function getAllLaunches(req, res) {
  return res.status(200).json(Array.from(launches.values()));
}

module.exports = { getAllLaunches };
```

`use` the new `launchesRouter` middleware in `app.js`:

```js
const express = require('express');
const path = require('path');
const cors = require('cors');
const morgan = require('morgan');
const app = express();
const planetsRouter = require('./routes/planets/planets.router');
const launchesRouter = require('./routes/launches/launches.router');

app.use(cors({ origin: 'http://localhost:3000' }));
app.use(morgan('combined'));
app.use(express.json());
app.use(express.static(path.join(__dirname, '..', 'public')));
app.use(planetsRouter);
app.use(launchesRouter);

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '..', 'public', 'index.html'));
});

module.exports = app;
```

Save changes and test the `/launches` route in Postman. The response should be
the following:

```json
[
  {
    "flightNumber": 100,
    "mission": "Kepler Exploration X",
    "rocket": "Explorer IS1",
    "launchDate": "2030-12-27T05:00:00.000Z",
    "destination": "Kepler-442 b",
    "customers": ["ZTM", "NASA"],
    "upcoming": true,
    "success": true
  }
]
```

## Getting the Launches on the Client

Now let's fetch our launches data from the server to the client. Refactor
`src/hooks/requests.js` and add the following:

```js
async function httpGetLaunches() {
  const response = await fetch(`${API_URL}/launches`);
  const fetchedLaunches = await response.json();
  return fetchedLaunches.sort((a, b) => {
    return a.flightNumber - b.flightNumber;
  });
}
```

Stop the server running with `npm run watch` if running, and run
`npm run deploy` to start the **client production app**.
