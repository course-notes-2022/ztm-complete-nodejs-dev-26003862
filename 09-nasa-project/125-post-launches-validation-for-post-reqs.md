# `POST /launches`: Validation for `POST` Requests

Let's make our Launch API a little more resilient. We need to refactor it to be
able to gracefully handle **incorrect** or **missing** request data.

The first step is to **validate our input**. We've decided that the four pieces
of **required** information to create a new launch are the:

- `mission`
- `rocket`
- `destination`
- `launchDate`

Furthermore, `launchDate` _must_ be a vaild Date string.

There are entire libraries dedicated to validation and date parsing in Node.
We'll look at the **foundation** you can use to understand what kind of
validation situations you might encounter with **any** API.

Add the following to `launches.controller`:

```js
function httpAddNewLaunch(req, res) {
  const launch = req.body;

  // Check for required attributes on request body
  if (
    !launch.mission ||
    !launch.rocket ||
    !launch.launchData ||
    !launch.destination
  ) {
    // Return error response if body invalid
    return res.status(400).json({ error: 'Missing required launch property' });
  }
  launch.launchDate = new Date(launch.launchDate); // Convert value passed by request body to a Date object

  // if (isNaN(launch.launchDate)) {// ...}
  if (launch.launchDate.toString() === 'Invalid Date') {
    return res.staus(400).json({ error: 'Invalid launch date' });
  }

  addNewLaunch(launch);
  return res
    .status(201)
    .json(getAllLaunches().find((l) => l.mission === launch.mission));
}
```
