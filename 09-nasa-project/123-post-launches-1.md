# `POST /launches`: Creating Launches Part 1

Let's add the ability to create a new `Launch`. We'll be using the `POST` HTTP
verb. Our endpoint will return a `201` status if the creation was successful.

Let's start with the `launches.model`. We need to write a function that our
router can use to set the new `launch` in our `launches` map:

`launches.model.js`:

```js
const launches = new Map();

// Keep track of the latest flight number
let latestFlightNumber = 100;

const launch = {
  flightNumber: latestFlightNumber,
  mission: 'Kepler Exploration X',
  rocket: 'Explorer IS1',
  launchDate: new Date('December 27, 2030'),
  destination: 'Kepler-442 b',
  customers: ['ZTM', 'NASA'],
  upcoming: true,
  success: true,
};

launches.set(launch.flightNumber, launch);

function getAllLaunches() {
  return Array.from(launches.values());
}

// Create new method to add a launch to launches map
function addNewLaunch(launch) {
  latestFlightNumber++; // Increment launch number on each save
  launches.set(latestFlightNumber, {
    ...launch,
    customers: ['Zero to Mastery', 'NASA'],
    upcoming: true,
    success: true,
    flightNumber: latestFlightNumber,
  });
}

module.exports = {
  addNewLaunch,
  getAllLaunches,
};
```
