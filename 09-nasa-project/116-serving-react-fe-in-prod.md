# Serving React Front-End in Production

How can we run our project in **production**? We can run our client and server
with run command, but they still run separately on different ports. In prod, we
often want to run the entire project on **one server**. Ideally we want to serve
our client and server from the **same server**.

With our client project, we'd usually deploy a `prod` build that is optimized
for production. Running the `npm run build` script creates our optimized
production bundle. Most frameworks provide a similar script to build a prod
bundle.

Our objective is to serve the optimized production bundle of our client app
**from our server**. To do this, we can make the following changes to the
`build` script in `client/package.json`:

```json
  "scripts": {
    "start": "react-scripts start",
    "build": "BUILD_PATH=../server/public react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject"
  },
```

The `BUILD_PATH` environment variable specifies the filepath to which the output
prod bundle will be generated. Running `npm run build` now will output our prod
bundle to the `public` folder of our `server` project.

**NOTE: `BUILD_PATH` MAY BE `create_react_app` SPECIFIC!** Investigate other
solutions if using different build tools.

In `app.js`, we can now serve our client app using the `express.static`
middleware just as we did before:

```js
const express = require('express');
const path = require('path');
const app = express();
const cors = require('cors');
const planetsRouter = require('./routes/planets.router');

app.use(cors({ origin: 'http://localhost:3000' }));
app.use(express.json());
app.use(express.static(path.join(__dirname, '..', 'public')));
app.use(planetsRouter);

module.exports = app;
```

Recall that `express.static` allows us to serve static files from the specified
directory. Save changes and run `npm run watch`, which in turn runs:

> `npm run server & npm run client`

Visit `localhost:8000` in a browser (_not_ `3000`) and note that _our client
application is being served from the server domain!_

## Completing the Deployment Script

Finally, we can create a **single script** to deploy our client to our Node
server. Refactor the root `package.json` scripts as follows:

```json
  "scripts": {
    "server": "npm run watch --prefix server",
    "client": "npm run start --prefix client",
    "watch": "npm run server & npm run client",
    "install-server": "npm install --prefix server",
    "install-client": "npm install --prefix client",
    "install": "npm run install-server && npm run install-client",
    "deploy": "npm run build --prefix client && npm start --prefix server"
  },
```

Note that the `deploy` script:

- Builds our production client
- Outputs the prod build to the server's `public` folder
- Starts the server in production mode
