# Creating our First `npm` Package

Let's learn how to use `npm` to create a package.

## "Module" vs. "Package"

A module is a file that contains some code that may be exported from that
module. A **package** is a collection of modules that have been bundled
together.

Let's make our first package. Recall our `modules-example` module from the
previous chapter. Imagine that we want to create a **package** from this project
and publish it to `npmjs.com`.

## Initializing a New `npm` Package

To initialize a new `npm` package, run:

> `npm init`

The command-line utility will ask you a series of questions about the package
name, entry file, version etc. To accept the defaults, run with the `-y` flag:

> `npm init -y`

After the utility completes, note that node creates a `package.json` file in
your project root containing metadata describing our project:

`package.json`:

```json
{
  "name": "modules-example",
  "version": "1.0.0",
  "description": "A simple package",
  "main": "https.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "jfarrow02",
  "license": "ISC"
}
```

We could now add a "start" script to start up our application:

```json
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "node https.js"
  },
```
