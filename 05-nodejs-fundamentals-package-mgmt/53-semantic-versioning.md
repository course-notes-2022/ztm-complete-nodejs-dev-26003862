# Semantic Versioning ("semver")

**Semantic versioning** is a way of versioning packages. It follows the format:

`major.minor.patch`

The **major** version indicates **breaking changes**.

The **minor** version indicates additional functionality was added in a
backwards-compatible manner.

**Patch** versions usually indicate bugfixes.
