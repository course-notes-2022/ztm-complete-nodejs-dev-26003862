# Packages and the `npm` Registry

Let's imagine that we want to install a 3rd-party package in our project.
Install the `axios` package using the following command:

> `npm i axios`

Notice we have a new `dependencies` entry in our `package.json`:

```json
{
  "name": "modules-example",
  "version": "1.0.0",
  "description": "A simple package",
  "main": "https.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "jfarrow02",
  "license": "ISC",
  "dependencies": {
    "axios": "^1.6.2"
  }
}
```

Most node programs are packages. You may or may not choose to share your
packages with the public via a **registry** such as `npm`. As long as you have a
`package.json` and your package is managed by `npm`, your application is
technically a package! Most often, the word "package" refers to a reusable
package shared via `npm`.
