# The `node_modules` Folder

Let's start with a fresh package. Run `npm init -y` and install `axios`.

`install` does 2 things:

1. It **downloads all the code from the `axios` package** into a folder called
   `node_modules

2. It **tracks all our dependencies** in `package-lock.json`

Lets start by using `axios`. Create a new file, `request.js`:

```js
const axios = require('axios');

axios.get('https://www.google.com').then((res) => {
  console.log(res);
});
```

Run this code in the browser and note that we get the same response as we did
previously from our custom http request method. Since axios returns a `Promise`,
we can chain some error-handling behavior:

```js
const axios = require('axios');

axios
  .get('https://www.google.com')
  .then((res) => {
    console.log(res);
  })
  .catch((err) => {
    console.log(err);
  });
```

Recall that our example is now a **package**. We can add a "start" script to it
just as we did before. We have unlocked the power of 3rd-party modules!
