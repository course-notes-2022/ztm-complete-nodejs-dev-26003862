# Installing `npm` Tools: `nodemon`

Let's look at how we might use some helpful tools via `npm` to help make our
development more convenient.

`nodemon` is a tool that helsp develop node apps wy automatically restarting the
application when a file changes. Let's install it and see it in action!

Run the following command:

> `npm install nodemon`

Add the following script to `package.json`:

```json
{
  "scripts": {
    "dev": "nodemon request.js"
  }
}
```

Save the file. Note that the file is executed **automatically**, without having
to re-type anything.

Imagine that we wanted to be able to run `nodemon` **from the terminal**, as we
do with the `node` executable. `nodemon` is currently installed **only locally**
in our project.

Inspect the `.bin` directory in the `node_modules` folder. The `nodemon`
binary/executable we installed with `npm install` is located here. If we run the
following in the terminal:

> `./node_modules/.bin/nodemon run dev`

We can run `nodemon` as an executable _without_ a script.

We don't usually run things this way, though. Typically, we install the module
**globally**:

> `npm install -g nodemon`

This allows the `nodemon` command to be available **anywhere**. `nodemon` gets
installed **directly on your machine**, rather than inside the package. We can
now run `nodemon` from any location on our machine. **Note however that this
makes our `package.json` and `package-lock.json` _less representative_** of our
actual project dependencies. For this reason, it's recommended to _not_ install
packages globally.
