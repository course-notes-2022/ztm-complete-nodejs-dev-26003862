# `package-lock.json` and Versioning

What is `package-lock.json`?

This file makes it easier to work with ohter developers and helps avoid bugs. It
is automatically generated when we run any `npm` commands that modify
`node_modules`, or when `package.json` is modified. It includes the package
name, **version number**, **package origin**, and verifies the integrity of the
packages:

```json
{
  "node_modules/axios": {
    "version": "1.6.2",
    "resolved": "https://registry.npmjs.org/axios/-/axios-1.6.2.tgz",
    "integrity": "sha512-7i24Ri4pmDRfJTR7LDBhsOTtcm+9kjX5WiY1X3wIisx6G9So3pfMkEiU7emUBe46oceVImccTEM3k6C5dbVW8A==",
    "dependencies": {
      "follow-redirects": "^1.15.0",
      "form-data": "^4.0.0",
      "proxy-from-env": "^1.1.0"
    }
  }
}
```

[`npmjs` Semver Calculator](https://semver.npmjs.com/)

Note that in `package.json`, our `axios` dependency specifies a **range** of
acceptable versions:

```json
{
  "dependencies": {
    "axios": "^1.6.2"
  }
}
```

`package-lock.json` above specifies the **exact version** used in our project.
This helps other developers who are collaborating with us download the **exact
same dependencies** and avoid issues with semantic versioning mismatches.
Therefore, **it's best practice to share `package.json` _and_
`package-lock.json`** when collaborating with other devs!
