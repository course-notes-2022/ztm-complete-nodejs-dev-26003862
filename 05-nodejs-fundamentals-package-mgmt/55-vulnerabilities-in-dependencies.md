# Vulnerabilities in Dependencies

Let's talk about **hackers**.

It's common for security issues to be found in packages. There are so many
packages in `npm` that chances are you'll encounter a security issue in one of
your dependencies soon.

**That's OK**. The key is to **be aware of the vulnerabilities** and **audit**
them, fixing any issues we find. `npm` provides us the `npm audit` tool to make
this easier.

Running `npm audit` informs us of the known security isses with our packages,
and running `npm audit fix` helps us keep our applications secure by making it
easier to download available updates. In the case that **no update is
available**, it might be a good idea to switch to a more secure library!
