# `npm`: The Node Package Manager

When we're building an important project, sometimes the node built-in tools are
not enough. For these instances, **3rd-party modules** are useful. Node has a
**huge** ecosystem of libraries, or **packages** as they are called in node.
Packages are **reusable code libraries** that we can use in our code. We can
find packages on [npmjs.com](https://npmjs.com), named after the
`Node Package Manager`, the tool we use to install and create these packages.

When we talk about `npm`, we're either talking about the `npmjs.com` website,
_or_ the Node Package Manager tool that we downloaded along with `node` itself.
`npm` is bundled with the node executable. If we want to install a package, we
can simply look around on `npmjs.com`. It's _completely free to use_! Node has a
huge open-source community that has been central to Node's success.
