# Templating Engines

What if we wanted to make our static HTML page more **dynamic**, for example
**populating it** with values from our **\*server**?

To do this, we can use
[Express' great support for **templating engines**](https://expressjs.com/en/guide/using-template-engines.html).
A **template engine** enables you to use static template files in your
appliction. At runtime, the template engine replaces variables in a template
file with actual values, and transforms the template into an HTML file sent to
the client.

To use one of these template engines, we need to install the corresponding `npm`
package. For example, we can install `Handlebars`, a popular engine, with:

> `npm install hbs`

Once installed, we don't need to `require` or `import` it into our application;
express loads it internally. We _do_ need to inform express _which template
engine we're using_, and _where to find our templates_. We can do this with the
[`app.set()`](https://expressjs.com/en/4x/api.html#app.set) command:

```js
const express = require('express');
const server = express();
const path = require('path');
const friendsRouter = require('./routes/friends.router');
const messagesRouter = require('./routes/messages.router');
const PORT = 3000;

server.set('view engine', 'hbs'); // Set template engine
server.set('views', path.join(__dirname, 'views')); // Set location of templates

server.use((req, res, next) => {
  const start = Date.now();
  next();
  const delta = Date.now() - start;
  console.log(`${req.method} ${req.baseUrl}${req.url} ${delta}ms`);
});

server.get('/', (req, res) => {
  res.render('index', { title: 'My Friends are VERY Clever!' }); // Tell express that we're rendering the handlebars file, and pass the DATA the file is expecting
// as an Object
});

// Refactor the static route to respond to the
// root route, so our template can find the CSS
server.use('/', express.static(path.join(__dirname, 'public')));
server.use(express.json());

server.use('/friends', friendsRouter);

server.use('/messages', messagesRouter);

server.listen(PORT, () => {
  console.log('Server listening on port: ', PORT);
});

});
```

Create a new folder in the project root, `views`. This folder will hold our
templates. Refactor `index.html` to `index.hbs` (note the file extension;
handlebars files must use `.hbs`), and place it in the `views` folder:

```hbs
<html>
  <head>
    <title>{{title}}</title>
    <link rel='stylesheet' href='styles/index.css' />
  </head>
  <body>
    <h1>{{title}}</h1>
  </body>
</html>
```
