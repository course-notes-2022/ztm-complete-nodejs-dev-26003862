# Postman and Insomnia

So far we've used the browser to explore our APIs. There are better tools for
testing APIs, such as **Postman**. Postman allows you to create HTTP requests in
**collections**, and share them with your teammates. It provides **automated
testing** to run API requests against your server to ensure you haven't broken
anything, documentation of your API based on your requests and responses, and
health status monitoring.

**Insomnia** is a Postman competitor, with similar features. It is known for its
simple, easy-to-use interface.

## Using Postman

Create a new **Collection**, `express-example`. Add a new `GET` request to the
collection with the URl `http://localhost:300/friends`. Send the request and
note the response in the Postman UI. We can send `POST` requests with a JSON
request body using the `Body` tag, or any other HTTP method.

![postman request](./screenshots/postman-request.png)
