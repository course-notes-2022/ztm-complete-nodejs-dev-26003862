# Introduction to Express

Let's initialize a new `npm` project as we have before. Once initialized,
install Express with:

> `npm install express`

Create a new `index.js` file that will house our Express server:

```js
const express = require('express');
const server = express();

const PORT = 3000;

server.listen(PORT, () => {
  console.log('Server listening on port: ', PORT);
});
```

Run `node server.js` (or create a new "start" command in your `package.json`).

Start the server and attempt to visit the `/` route in a browser. Note that we
get a `404`, because we haven't yet defined a **route handler** for any routes.
Add the following to `index.js`:

```js
const express = require('express');
const server = express();

const PORT = 3000;

server.listen(PORT, () => {
  console.log('Server listening on port: ', PORT);
});
```

Save changes and restart the server. Note that we get the expected responses,
_with the **response status code** and the `Content-Type` headers are set for
us_.

The `req` and `res` objects are a bit different from the built-in Node Request
and Response objects. For example, we can use the `res.send` method to send a
response to the client directly.

Express looks for a match **on each route** agains the request `URL` it
receives, and runs the first route handler function that matches the route. If
**no routes match**, it returns a `404` status code.
