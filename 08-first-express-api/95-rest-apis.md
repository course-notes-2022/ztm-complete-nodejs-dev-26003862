# REST-ful APIs

What best practices do we use when desiging our APIs?

We'll be following `REST`, the most important pattern to know when desinging
APIs.

## Best Practices

- Endpoints should be **plural nouns** (`/friends`, `/messages`)
- We call these plural nouns **collections**; they represent collections of data
  being made available by the server
- We can add to/delete from/read using the appropriate **HTTP methods**

## What Makes an API REST-ful?

REST-ful APIs follow a certain **pattern**. REST is an acronym meaning
"Representational State Transfer".

Generally, REST-ful APIs have the following characteristics:

- Use existing web standards (HTTP, JSON, URLs)
- Endpoints are collections of data stored on the server side
- Use GET, POST, PUT, and DELETE
- Client and server architecture
- Requests are **stateless** (each request is separate and unconnected to any
  state on the client not included in the request; server does not track the
  state of any requests) and **cacheable**

We've already been applying these principles: our endpoints are using plural
nouns that signify collections, we're using the appropriate HTTP methods (`GET`
for getting data, `POST` for creating data, etc.).
