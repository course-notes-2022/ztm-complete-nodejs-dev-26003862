# Model/View/Controller (MVC)

How might we organize our code so that it's more **manageable, scalable, and
easy to understand**?

MVC is a pattern that describes how to organize our code base on what they do.
Our code is split into the:

- Model: Updates the **view**
- View: What the user **sees**
- Controller: Manipulates the **model**

![mvc diagram](./screenshots/mvc-diagram.png)

In express, the **controllers** are the functions that react to the incoming
request and set the response accordingly. The **model** is our data and the
functions used to access that data. The **view** is how the data from the model
is presented to the user. The view can be presented in graphs/diagrams, an
entire HTML application, _or_ the API's `JSON` response.

When we follow design patterns, our code is more **understandable**. We'll apply
MVC to our project in the next lesson.
