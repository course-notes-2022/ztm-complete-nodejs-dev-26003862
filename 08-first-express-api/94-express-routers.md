# Express Routers

When building large express apps, we often use **Routers** to organize our
routes into smaller groups.

A **Router** is like a "mini-application" that contains its own middleware and
routes. We use Routers to make our applications more modular. We use Routers
like any other Express middleware:

```js
const express = require('express');
const server = express();
const {
  getMessages,
  postMessage,
} = require('./controllers/messages.controller');
const {
  postFriend,
  getFriends,
  getFriend,
} = require('./controllers/friends.controller');
const PORT = 3000;

server.use((req, res, next) => {
  const start = Date.now();
  next();
  const delta = Date.now() - start;
  console.log(`${req.method} ${req.url} ${delta}ms`);
});

server.use(express.json());

// Define the router
const friendsRouter = express.Router();
friendsRouter.post('/', postFriend);
friendsRouter.get('/', getFriends);
friendsRouter.get('/:friendId', getFriend);

server.use('/friends', friendsRouter); // Use the router like any other middleware

server.get('/messages', getMessages);

server.post('/messages', postMessage);

server.listen(PORT, () => {
  console.log('Server listening on port: ', PORT);
});
```

Note that we can define our routes **relative to the Router**: mounting the
router to the `/friends` path in `server.use` allows us to define the routes
relative to `/friends` as the root. Make sure to call `app.use` to let express
know to respond to requests at the Router's route.

We usually define Routers in a **separate folder**:

`routers/friends.router.js`:

```js
const express = require('express');
const {
  postFriend,
  getFriends,
  getFriend,
} = require('../controllers/friends.controller');

const friendsRouter = express.Router();

// We can define middleware for use
// just by our friendsRouter
friendsRouter.use((req, res, next) => {
  console.log('ip address: ', req.ip);
  next(); // Don't forget to call `next()`!
});

friendsRouter.post('/', postFriend);
friendsRouter.get('/', getFriends);
friendsRouter.get('/:friendId', getFriend);

module.exports = friendsRouter;
```

We can now use our routers in `index`:

```js
const express = require('express');
const server = express();

const friendsRouter = require('./routes/friends.router');
const messagesRouter = require('./routes/messages.router');
const PORT = 3000;

server.use((req, res, next) => {
  const start = Date.now();
  next();
  const delta = Date.now() - start;
  console.log(`${req.method} ${req.url} ${delta}ms`);
});

server.use(express.json());

server.use('/friends', friendsRouter); // Use the router like any other middleware

server.use('/messages', messagesRouter);

server.listen(PORT, () => {
  console.log('Server listening on port: ', PORT);
});
```
