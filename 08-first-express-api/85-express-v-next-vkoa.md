# Express vs. Next vs. Koa

Let's compare some competing frameworks with Express. It's good to be aware of
the **best available options** when selecting the technology stack.

## Express

Express is the **clear winner** when it comes to adoption, stability, usage,
etc. If follows the same patterns that the Node http server is built on. It
focuses on:

- Robust routing
- High-performance
- Reliable and well-tested

Express is **extensible** using a feature called **middleware**. Middleware
allows you to manipulate **incoming requests** and **outgoing responses**.
Express is well-documented. The Express project is managed by **StrongLoop**,
which also manages the [Loopback Framework](https://loopback.io/). Loopback
builds on Express and aims to make API development easier in **large enterprise
context**. Loopback can be more challenging than Express to learn.

## Koa

Koa is the follow-up to Express, from the **creator** of Express. It is improved
to make better use modern JS features. Koa is designed to be more modular than
express, and to be more Promise-based (using async-await) rather than callback
functions. Koa is designed to minimize built-in features and rely on a powerful
middleware system that makes it very extensible.

## NextJS

NextJs is a framework that simplifies integration of React applications with
NodeJS backends. It is used by top companies such as Netflix.

Next focuses on **server-side rendering**. It's a technique used for
**performance** and **SEO** purposes. The client side code is generated on the
server (though you can control this behavior). SSR decreases the amount of work
that must be done on the **client** in favor of pushing that work to the
**server**. It is opinionated and feature-rich. Because of its focus on React
and its integrations with other technologies, its not as versatile for building
APIs and backends.
