# `POST` Requests in Express

Let's learn how Express handles `POST` requests. We'll create a new endpoint to
add new friends to our `friends` collection. Add the following to `index.js`:

Our server doesn't understand `JSON` by default. The request object that is
passed from the client is usually in `JSON`. In our built-in Node HTTP server we
had to manually parse the `JSON` with the `json.parse` function.

Fortunately, Express makes a `.json` middleware function that understands when a
request is being passed as JSON, and will do the parsing **for us**. `.json`
sets the body to a JS object when the `Content-Type` header is equal to
`application/json`:

```js
const express = require('express');
const server = express();

const PORT = 3000;

const friends = [
  { id: 0, name: 'Albert Einstein' },
  { id: 1, name: 'Isaac Newton' },
];

server.use((req, res, next) => {
  const start = Date.now();
  next();
  const delta = Date.now() - start;
  console.log(`${req.method} ${req.url} ${delta}ms`);
});

server.use(express.json()); // Returns the json middleware

// Create the new `POST` route handler
server.post('/friends', (req, res) => {
  if (!req.body.name) {
    res.status(400).json({ err: 'request body must have a name' });
  }
  const newFriend = {
    name: req.body.name,
    id: friends.length,
  };
  friends.push(newFriend);

  res.status(201).json(newFriend);
});

server.get('/friends', (req, res) => {
  res.status(200).json(friends);
});

server.get('/friends/:friendId', (req, res) => {
  const friendId = Number(req.params.friendId);
  const friend = friends[friendId];
  if (friend) {
    res.status(200).json(friend);
  } else {
    res.status(404).json({ err: 'friend ' + friendId + ' not found' });
  }
});

server.get('/messages', (req, res) => {
  res.send({
    name: 'Sir Isaac Newton',
  });
});

server.listen(PORT, () => {
  console.log('Server listening on port: ', PORT);
});
```

Send a request with a `name` property, and note the request succeeds. Send a
second request _without_ a `name` property. Note that the expected error message
is returned, but there is an error in the terminal:

```
Error [ERR_HTTP_HEADERS_SENT]: Cannot set headers after they are sent to the client
```

This is a common error when we try to do something **after** setting the
response to a request. We must `return` after we send the `400` response:

```js
// Create the new `POST` route handler
server.post('/friends', (req, res) => {
  if (!req.body.name) {
    res.status(400).json({ err: 'request body must have a name' });
    return; // Return early if there is an error
  }
  const newFriend = {
    name: req.body.name,
    id: friends.length,
  };
  friends.push(newFriend);

  res.status(201).json(newFriend);
});
```
