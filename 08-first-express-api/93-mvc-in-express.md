# MVC in Express

Let's apply what we've learned about MVC to our Express API. We'll begin with
the route handler functions. These will be our **controllers**, the functions
that interact with the incoming request. Create a new folder, `controllers` in
the project root. We'll create on controller module for each of our collections
of data: `friends` and `messages`. When working on larger servers this allows us
to break functionality down into easier-to-manage pieces. Create a new file
`controllers.messages.js` and `controllers.friends.js`.

Add the following to `messages`:

```js
function getMessages(req, res) {
  res.send('<ul><li>Hello Albert!</li></ul>');
}

function postMessage(req, res) {
  console.log('Updating messages...');
}
module.exports = {
  getMessages,
  postMessage,
};
```

**Note**: When defining a function from the **top-level** of a file, it's good
to use a **named function** (defined with the `function` keyword). This is
because when debugging our node app, **node can tell us the name of the
function**, unlike with an **arrow function**.

Add the following to `index.js`:

```js
const express = require('express');
const server = express();
const {
  getMessages,
  postMessage,
} = require('./controllers/messages.controller');

const PORT = 3000;

const friends = [
  { id: 0, name: 'Albert Einstein' },
  { id: 1, name: 'Isaac Newton' },
];

server.use((req, res, next) => {
  const start = Date.now();
  next();
  const delta = Date.now() - start;
  console.log(`${req.method} ${req.url} ${delta}ms`);
});

server.use(express.json());

// Omitted...

server.get('/messages', getMessages);

server.post('/messages', postMessage);

server.listen(PORT, () => {
  console.log('Server listening on port: ', PORT);
});
```

Updates `friends.js` in the same way:

```js
function postFriend(req, res) {
  if (!req.body.name) {
    res.status(400).json({ err: 'request body must have a name' });
    return;
  }
  const newFriend = {
    name: req.body.name,
    id: friends.length,
  };
  friends.push(newFriend);

  res.status(201).json(newFriend);
}

function getFriends(req, res) {
  res.status(200).json(friends);
}

module.exports = {
  postFriend,
  getFriends,
};
```

Let's create a **new model** that we can leverage for our `friends` data. Create
a new folder, `models`, and a new file inside `friends.model.js`:

```js
const friends = [
  { id: 0, name: 'Albert Einstein' },
  { id: 1, name: 'Isaac Newton' },
];

module.exports = friends;
```

Import the new model in `friends.controller.js`:

```js
const friends = require('../models/friends.model');

function postFriend(req, res) {
  if (!req.body.name) {
    res.status(400).json({ err: 'request body must have a name' });
    return;
  }
  const newFriend = {
    name: req.body.name,
    id: friends.length,
  };
  friends.push(newFriend);

  res.status(201).json(newFriend);
}

function getFriends(req, res) {
  res.status(200).json(friends);
}

function getFriend(req, res) {
  const friendId = Number(req.params.friendId);
  const friend = friends[friendId];
  if (friend) {
    res.status(200).json(friend);
  } else {
    res.status(404).json({ err: 'friend ' + friendId + ' not found' });
  }
}

module.exports = {
  postFriend,
  getFriends,
  getFriend,
};
```

Now import the route handlers and use them in `index.js`:

```js
const express = require('express');
const server = express();
const {
  getMessages,
  postMessage,
} = require('./controllers/messages.controller');
const {
  postFriend,
  getFriends,
  getFriend,
} = require('./controllers/friends.controller');
const PORT = 3000;

server.use((req, res, next) => {
  const start = Date.now();
  next();
  const delta = Date.now() - start;
  console.log(`${req.method} ${req.url} ${delta}ms`);
});

server.use(express.json());
server.post('/friends', postFriend);

server.get('/friends', getFriends);

server.get('/friends/:friendId', getFriend);

server.get('/messages', getMessages);

server.post('/messages', postMessage);

server.listen(PORT, () => {
  console.log('Server listening on port: ', PORT);
});
```

## What About Our Views?

Right now our views are the `JSON` responses we're sending back from our route
handlers. The client will be responsible for displaying the views.
