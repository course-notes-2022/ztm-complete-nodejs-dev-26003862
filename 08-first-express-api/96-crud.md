# CRUD

When working as backend developers, you'll often hear about `CRUD`. CRUD refers
to the four basic operations we can apply on data: `Create`, `Read`, `Update`
and `Delete`. For REST-ful APIs, the CRUD operations correspond to HTTP verbs:

- `Create` <-> `POST`
- `Read` <-> `GET`
- `Update` <-> `PUT`
- `Delete` <-> `DELETE`

## `PUT` vs. `PATCH`

PUT updates a specific item in a collection by **replacing the entire item**.
PATCH updates an item by **modifying properties of the existing item**.
