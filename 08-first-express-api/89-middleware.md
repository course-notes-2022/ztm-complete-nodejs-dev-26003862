# Middleware

An idea that comes up all the time in Express is **middleware**. Middleware
allows us to add features to our server by working with the incoming request and
outgoing response. Middleware is **essential** to understanding how Express
works. Let's get to it!

Consider the following diagram:

![basic app diagram](./screenshots/basic-app-diagram.png)

**Middleware** are special functions that run **in between** (or "in the
middle") of the **request** being received and the **response** being sent.

**Our Express app is really just a series of Middleware functions** that execute
**in sequence**:

![middleware](./screenshots/middleware.png)

# Middleware Functions

Our middleware functions have a specific structure:

```js
// Register teh middleware with express via app.use
app.use(function (req, res, next) {
  // The middleware is the function we pass to handle the request/response
});
```

The `next` parameter is what differentiates our middleware. Our callback
function has the opportunity to work with the request, use the data from it, and
take some action **before it reaches any route handlers**. It might:

- Log the request
- Perform some data validation
- Check the user authorization

`next` is a function that Express gives us that allows us to call the **next
middleware function**. It controls the flow of our middleware. From the next
middleware function, we must **either** call the `next` function again, or
`return`. Once all our middleware is executed, we send the response back to the
client.
