# Layouts and Separation of Concerns

Let's continue to understand templating engines and how they apply to what we've
learned so far.

With handlebars, if you create a file called **layout.hbs**, in `views`, this
file will contain all the common HTML that your pages will use.

Create `views/layout.hbs`. Cut the following snippet from `index.hbs` and paste
it to `layout`. Add the `{{{}}}` triple-mustache syntax, and place the special
`body` variable inside::

```hbs
<html>
  <head>
    <title>{{title}}</title>
    <link rel='stylesheet' href='styles/index.css' />
  </head>
  <body>
    {{{body}}}
  </body>
</html>
```

Now, refactor `index.hbs` to add the content we wish to show on the `index`
route:

```hbs
<h1>{{title}}</h1>
```

This layout will be used by default when Express calls `res.render`.
`res.render` will render the **template** passed to it inside the `{{{body}}}`.

We can add another template in the same way. Create a new `views/messages.hbs`
file and add the following:

```hbs
<h2>Hello {{friend}}!</h2>
<p>What do you think of modern medicine?</p>
```

Add the following to `index.js`:

```js
server.get('/thoughts', (req, res) => {
  res.render('messages', { friend: 'Virginia Leith' });
});
```

Now, when we hit the `/thoughts` route, **our layout will replace the
{{{body}}}** with the contents of `messages.hbs`! We're leveraging the same
layout with **dynamic contents** based on the rendered template.

## Fitting it All Together

Before the REST-ful pattern became the common best practice for writing HTTP
APIs, it was common to have views being rendered statically in the server like
we just did. Unless we're doing SSR for performance rendering, doing this kind
of server-side rendering with templates is **much less common** these days. Our
servers should deal with **server-side logic** as much as possible, letting the
front-end render the data that it gets from the server in a beautiful way.
**Separation of concerns** help make our code cleaner and easier to understand!
