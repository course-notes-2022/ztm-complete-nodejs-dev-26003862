const express = require('express');
const server = express();
const path = require('path');
const friendsRouter = require('./routes/friends.router');
const messagesRouter = require('./routes/messages.router');
const PORT = 3000;

server.set('view engine', 'hbs'); // Set template engine
server.set('views', path.join(__dirname, 'views')); // Set location of templates

server.use((req, res, next) => {
  const start = Date.now();
  next();
  const delta = Date.now() - start;
  console.log(`${req.method} ${req.baseUrl}${req.url} ${delta}ms`);
});

server.get('/', (req, res) => {
  res.render('index', { title: 'My Friends are VERY Clever!' }); // Tell express that we're rendering the handlebars file
});

server.get('/thoughts', (req, res) => {
  res.render('messages', { friend: 'Virginia Leith' });
});

server.use('/', express.static(path.join(__dirname, 'public')));
server.use(express.json());

server.use('/friends', friendsRouter); // Use the router like any other middleware

server.use('/messages', messagesRouter);

server.listen(PORT, () => {
  console.log('Server listening on port: ', PORT);
});
