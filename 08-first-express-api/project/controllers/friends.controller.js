const friends = require('../models/friends.model');

function postFriend(req, res) {
  if (!req.body.name) {
    res.status(400).json({ err: 'request body must have a name' });
    return;
  }
  const newFriend = {
    name: req.body.name,
    id: friends.length,
  };
  friends.push(newFriend);

  res.status(201).json(newFriend);
}

function getFriends(req, res) {
  res.status(200).json(friends);
}

function getFriend(req, res) {
  const friendId = Number(req.params.friendId);
  const friend = friends[friendId];
  if (friend) {
    res.status(200).json(friend);
  } else {
    res.status(404).json({ err: 'friend ' + friendId + ' not found' });
  }
}

module.exports = {
  postFriend,
  getFriends,
  getFriend,
};
