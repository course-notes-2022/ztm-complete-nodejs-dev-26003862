const path = require('path'); // `path` allows us to work with filepaths and handle all the ways different OS handle file paths

function getMessages(req, res) {
  res.sendFile(path.join(__dirname, '..', 'public', 'myFile.com')); // sendFile() needs an absolute path
  // __dirname gives the directory in which the current file is located
}

function postMessage(req, res) {
  console.log('Updating messages...');
}
module.exports = {
  getMessages,
  postMessage,
};
