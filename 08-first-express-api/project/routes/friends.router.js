const express = require('express');
const {
  postFriend,
  getFriends,
  getFriend,
} = require('../controllers/friends.controller');

const friendsRouter = express.Router();

// We can define middleware for use
// just by our friendsRouter
friendsRouter.use((req, res, next) => {
  console.log('ip address: ', req.ip);
  next();
});

friendsRouter.post('/', postFriend);
friendsRouter.get('/', getFriends);
friendsRouter.get('/:friendId', getFriend);

module.exports = friendsRouter;
