# Serving Websites with Node

One common scenario is using Node to serve a front-end web app. It could be a
static HTML site, or a React app (or any other framework). One way we can
accomplish this is to use express' `static` middleware.

We use the `static` middleware just like any other Express middleware: with the
`server.use` function. Add the following to `index.js`:

```js
const express = require('express');
const server = express();

const friendsRouter = require('./routes/friends.router');
const messagesRouter = require('./routes/messages.router');
const PORT = 3000;

server.use((req, res, next) => {
  const start = Date.now();
  next();
  const delta = Date.now() - start;
  console.log(`${req.method} ${req.baseUrl}${req.url} ${delta}ms`);
});

// Pass the location of the directory from which
// you want to server your static files to `express.static()`
server.use(express.static('public'));
server.use(express.json());

server.use('/friends', friendsRouter);

server.use('/messages', messagesRouter);

server.listen(PORT, () => {
  console.log('Server listening on port: ', PORT);
});
```

We can mount our site to a specific path, just as we can with our routers:

```js
server.use('/site', express.static('public'));
```

We should leverage `__dirname` to specify the **absolute path** from the folder
where `server.js` is defined to our `public` folder:

```js
server.use('/site', express.static(path.join(__dirname, 'public')));
```

We can serve **any type of static files** in this way: static HTML, CSS, images,
**React** or other JS framework applications, etc.

## Handling Thousands of Users

For high-traffic applications, it's usually better to serve your applications
from solutions such as **Amazon CloudFront**. Your data is hosted and served
from a CDN that deals with your static files with locations closer to your
users. That way, Node can focus on what it does best: **non-blocking**, **async
I/O** for your **REST-ful API** endpoints. But for small/medium-sized
applications, we now know how to do so in Node!
