# Why Express?

We've created our first HTTP server. But the built-in Node functionality is
pretty bare-bones. Let's find a more **convenient** solution.

As developers, we want to look for ways to **leverage tools to help us with
common tasks**, such as building a web server. There are many packages that we
can use to build servers in Node. The most widely-used is by far **Express**.

[Stack Overflow 2023 Developer Survey Results](https://survey.stackoverflow.co/2023/#technology)
