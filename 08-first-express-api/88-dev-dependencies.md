# Development Dependencies

Let's add a dev dependency that helps us by **restarting the server**
automatically on change with `nodemon`. This time, we'll place `nodemon` under
the **development** dependencies in our `package.json`. This will allow us to
use `nodemon` during development, but will _not_ include it as a dependency for
our **production build**:

> `npm install nodemon --save-dev`

This will allow us to keep our production build as slim as possible.

Add the `watch` script to `package.json` to allow us to run `nodemon`:

```json
  "scripts": {
    "start": "node index.js",
    "watch": "nodemon index.js"
  },
```
