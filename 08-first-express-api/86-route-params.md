# Route Parameters

Let's look at another example of how Express makes our development easier. We'll
query a list of friends as we did before. Add the following to `index.js`:

```js
const express = require('express');
const server = express();

const PORT = 3000;

const friends = [
  { id: 0, name: 'Albert Einstein' },
  { id: 1, name: 'Isaac Newton' },
];
server.get('/friends', (req, res) => {
  res.status(200).json(friends);
});

server.get('/friends/:friendId', (req, res) => {
  const friendId = Number(req.params.friendId);
  // Validate user input
  const friend = friends[friendId];
  if (friend) {
    res.status(200).json(friend);
  } else {
    res.status(404).json({ err: 'friend ' + friendId + ' not found' });
  }
});

server.get('/messages', (req, res) => {
  res.send({
    name: 'Sir Isaac Newton',
  });
});

server.listen(PORT, () => {
  console.log('Server listening on port: ', PORT);
});
```

Note the use of the `'/friends/:friendId'` syntax in the `server.get` method.
This makes use of **route parameters**. We can parse the value of the parameters
from the `req.params` object using the **name** after the `:`, in this case
`friendId`.
