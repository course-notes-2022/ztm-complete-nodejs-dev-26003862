# Writing Our Own Loggin Middleware

Let's write some middleware that logs each request and tells how long the
request took to complete. Add the following to `index.js`:

```js
const express = require('express');
const server = express();

const PORT = 3000;

const friends = [
  { id: 0, name: 'Albert Einstein' },
  { id: 1, name: 'Isaac Newton' },
];

// Create new middleware function
server.use((req, res, next) => {
  const start = Date.now();
  next();
  // actions go here...
  const delta = Date.now() - start;
  console.log(`${req.method} ${req.url} ${delta}ms`);
});

server.get('/friends', (req, res) => {
  res.status(200).json(friends);
});

server.get('/friends/:friendId', (req, res) => {
  const friendId = Number(req.params.friendId);
  const friend = friends[friendId];
  if (friend) {
    res.status(200).json(friend);
  } else {
    res.status(404).json({ err: 'friend ' + friendId + ' not found' });
  }
});

server.get('/messages', (req, res) => {
  res.send({
    name: 'Sir Isaac Newton',
  });
});

server.listen(PORT, () => {
  console.log('Server listening on port: ', PORT);
});
```

You do not need to restart the server if you've run it with the `npm run watch`
command. Make a `GET` request via Postman to `/friends` and observe the
following output in the terminal:

```
GET /friends
```

Comment out the `next()` call, and attempt to send the request again. Note that
Express "hangs" and the response is **never sent**. Eventually the request times
out.

> You **must** call `next()` **every time** to move on to the next middleware!

## Recap

When a request comes in to our API, it hits the first middleware function. The
`next` function is called, which in our case goes to the route handler. The
route handler is the **end** of the middleware chain, so it returns control to
the original middleware which then sends it out via Node's HTTP server. This
process repeats for **every request** that comes in to the server.
