# Sending Files

We sometimes need to send **files** back to the client instead of raw JSON data.
Express makes it **very easy** to do just that.

We can put files that we want to make available from our server in the `public`
folder. Update the `messages.controller` file as follows:

```js
const path = require('path'); // `path` allows us to work with filepaths and handle all the ways different OS handle file paths

function getMessages(req, res) {
  res.sendFile(path.join(__dirname, '..', 'public', 'myFile.com')); // sendFile() needs an absolute path
  // __dirname gives the directory in which the current file is located
}

function postMessage(req, res) {
  console.log('Updating messages...');
}
module.exports = {
  getMessages,
  postMessage,
};
```
